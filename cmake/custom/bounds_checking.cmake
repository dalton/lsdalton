if(ENABLE_BOUNDS_CHECK)
  message(STATUS "Bounds checking for Fortran code enabled")
  if(DEFINED CMAKE_Fortran_COMPILER_ID)
    if(CMAKE_Fortran_COMPILER_ID MATCHES GNU)
      list(APPEND LSDALTON_Fortran_FLAGS
        "-fcheck=all"
        )
    endif()
    if(CMAKE_Fortran_COMPILER_ID MATCHES Intel)
      list(APPEND LSDALTON_Fortran_FLAGS
        "-check"
        "-ftrapuv"
        "-traceback"
        "-debug"
        "-fp-stack-check"
        )
    endif()
    if(CMAKE_Fortran_COMPILER_ID MATCHES PGI)
      list(APPEND LSDALTON_Fortran_FLAGS)
    endif()
    if(CMAKE_Fortran_COMPILER_ID MATCHES XL)
      list(APPEND LSDALTON_Fortran_FLAGS "-C")
    endif()
    if(CMAKE_Fortran_COMPILER_ID MATCHES Cray)
      list(APPEND LSDALTON_Fortran_FLAGS "-R bps")
    endif()
    if(CMAKE_Fortran_COMPILER_ID MATCHES Fujitsu)
      list(APPEND LSDALTON_Fortran_FLAGS)
    endif()
  endif()
endif()
