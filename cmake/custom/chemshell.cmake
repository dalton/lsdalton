#.rst:
#
# autocmake.yml configuration::
#
#   docopt:
#     - "--chemshell Compile for Chemshell [default: False]."
#   define: "'-DENABLE_CHEMSHELL={0}'.format(arguments['--chemshell'])"

option(ENABLE_CHEMSHELL "Compile for ChemShell" OFF)

if(ENABLE_CHEMSHELL)
  # Chemshell does not use pcm
  set(ENABLE_PCMSOLVER OFF CACHE BOOL "Enable PCMSolver library" FORCE)
  # Chemshell does not use dec
  set(ENABLE_DEC OFF CACHE BOOL "Enable Divide-Expand-Consolidate module" FORCE)
endif()
