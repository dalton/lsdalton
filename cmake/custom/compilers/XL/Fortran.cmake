list(APPEND LSDALTON_Fortran_FLAGS
  "-qzerosize"
  "-qextname"
  )

list(APPEND LSDALTON_Fortran_FLAGS_DEBUG
  "-g"
  )

list(APPEND LSDALTON_Fortran_FLAGS_RELEASE
  "-O3"
  "-qstrict"
  )
