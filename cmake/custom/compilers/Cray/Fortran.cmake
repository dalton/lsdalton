list(APPEND LSDALTON_Fortran_FLAGS
  "-eZ"
  )

# OpenMP and OpenACC are apparently opt-out, not opt-in, on Cray...
if(NOT ENABLE_OMP)
  list(APPEND LSDALTON_Fortran_FLAGS
    "-h noomp"
    )
endif()

if(NOT ENABLE_ACC)
  list(APPEND LSDALTON_Fortran_FLAGS
    "-h noacc"
    )
endif()

list(APPEND LSDALTON_Fortran_FLAGS_DEBUG
  "-g"
  "-O0"
  )

list(APPEND LSDALTON_Fortran_FLAGS_RELEASE
  )

if(ENABLE_TITANBUILD)
  set_property(
    GLOBAL
    PROPERTY
    COMPILE_DEFINITIONS "VAR_WORKAROUND_CRAY_MEM_ISSUE_LARGE_ASSIGN"
    )
endif()
