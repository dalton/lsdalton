# Copied from
# https://github.com/dev-cafe/cmake-cookbook/blob/master/chapter-07/recipe-03/c-cxx-example/set_compiler_flag.cmake
# Adapted from
# https://github.com/robertodr/ddPCM/blob/expose-C-api/cmake/custom/compilers/SetCompilerFlag.cmake
# which was adapted by Roberto Di Remigio from
# https://github.com/SethMMorton/cmake_fortran_template/blob/master/cmake/Modules/SetCompileFlag.cmake

# Given a list of flags, this stateless function will try each, one at a time,
# and set result to the first flag that works.
# If none of the flags works, result is "".
# If the REQUIRED key is given and no flag is found, a FATAL_ERROR is raised.
#
# Call is:
# set_compiler_flag(result LANG (Fortran|C|CXX) <REQUIRED> FLAGS flag1 flag2 ...)
#
# Example:
# set_compiler_flag(working_compile_flag LANG C REQUIRED FLAGS "-Wall" "-warn all")

include(CheckCCompilerFlag)
include(CheckCXXCompilerFlag)
include(CheckFortranCompilerFlag)

function(set_compiler_flag _result)
  set(options REQUIRED)
  set(oneValueArgs LANG)
  set(multiValueArgs FLAGS)
  cmake_parse_arguments(_flagger
    "${options}"
    "${oneValueArgs}"
    "${multiValueArgs}"
    ${ARGN}
    )

  # Silently check compiler flags
  set(restore_CMAKE_REQUIRED_QUIET ${CMAKE_REQUIRED_QUIET})
  set(CMAKE_REQUIRED_QUIET TRUE)

  get_property(_enabled_langs GLOBAL PROPERTY ENABLED_LANGUAGES)
  # check whether language is among the enabled ones
  if(NOT _flagger_LANG IN_LIST _enabled_langs)
    message(FATAL_ERROR "Language ${_flagger_LANG} has not been enabled for this project!")
  endif()

  set(_flag_found FALSE)
  # loop over all flags, try to find the first which works
  foreach(flag IN ITEMS ${_flagger_FLAGS})
    unset(_flag_works CACHE)
    if(_flagger_LANG STREQUAL "C")
      check_c_compiler_flag("${flag}" _${flag}_works)
    elseif(_flagger_LANG STREQUAL "CXX")
      check_cxx_compiler_flag("${flag}" _${flag}_works)
    elseif(_flagger_LANG STREQUAL "Fortran")
      check_Fortran_compiler_flag("${flag}" _${flag}_works)
    else()
      message(FATAL_ERROR "Unknown language in set_compiler_flag: ${_lang}")
    endif()

    # if the flag works, use it, and exit
    # otherwise try next flag
    if(_${flag}_works)
      set(${_result} "${flag}" PARENT_SCOPE)
      set(_flag_found TRUE)
      break()
    endif()
    unset(_${flag}_works)
  endforeach()

  # raise an error if no flag was found
  if(_flagger_REQUIRED AND NOT _flag_found)
    message(FATAL_ERROR "None of the required flags were supported")
  endif()

  # Restore CMAKE_REQUIRED_QUIET
  set(CMAKE_REQUIRED_QUIET ${restore_CMAKE_REQUIRED_QUIET})

  unset(_enabled_langs)
endfunction()
