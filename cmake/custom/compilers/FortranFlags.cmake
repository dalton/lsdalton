include(${CMAKE_CURRENT_LIST_DIR}/set_compiler_flags.cmake)

# Check whether the compiler understands Fortran 2003 and pointer reshapes
foreach(_name std_2003 ptr_reshape)
  set(WORK_DIR "${CMAKE_CURRENT_BINARY_DIR}/feature_tests/Fortran_tests")
  set(SRC_DIR  "${CMAKE_CURRENT_LIST_DIR}/../feature_tests")
  configure_file(
    "${SRC_DIR}/test-Fortran-${_name}.f90"
    "${WORK_DIR}/test-Fortran-${_name}.f90"
    COPYONLY
    )
  set(SOURCE_FILE "${WORK_DIR}/test-Fortran-${_name}.f90")
  try_compile(has_${_name}
    "${CMAKE_CURRENT_BINARY_DIR}" SOURCES "${SOURCE_FILE}"
    CMAKE_FLAGS
      -DCMAKE_Fortran_COMPILER=${CMAKE_Fortran_COMPILER}
    )
  unset(WORK_DIR)
  unset(SRC_DIR)
  unset(SOURCE_FILE)
endforeach()

if(has_std_2003)
  message(STATUS "Fortran compiler is compliant with the 2003 standard!")
else()
  message(STATUS "Fortran compiler NOT compliant with the 2003 standard. Some functionality will not be available.")
endif()
set(HAS_STD_2003 ${has_std_2003} CACHE BOOL "Fortran compiler compliant with the 2003 standard" FORCE)

if(has_ptr_reshape)
  message(STATUS "Fortran compiler allows pointer reshapes!")
  set_property(
    GLOBAL
    PROPERTY
      COMPILE_DEFINITIONS "VAR_PTR_RESHAPE"
    )
else()
  message(STATUS "Fortran compiler DISALLOWS pointer reshapes. Some functionality will not be available.")
endif()
set(HAS_PTR_RESHAPE ${has_ptr_reshape} CACHE BOOL "The Fortran compiler allows pointer reshapes" FORCE)

set(LSDALTON_Fortran_FLAGS)
if(TARGET OpenMP::OpenMP_Fortran)
  list(APPEND LSDALTON_Fortran_FLAGS ${OpenMP_Fortran_FLAGS})
endif()
set(LSDALTON_Fortran_FLAGS_DEBUG)
set(LSDALTON_Fortran_FLAGS_RELEASE)
set(LSDALTON_Fortran_FLAGS_COVERAGE)

if(CMAKE_Fortran_COMPILER_ID MATCHES GNU)
  include(${CMAKE_CURRENT_LIST_DIR}/GNU/Fortran.cmake)
elseif(CMAKE_Fortran_COMPILER_ID MATCHES Intel)
  include(${CMAKE_CURRENT_LIST_DIR}/Intel/Fortran.cmake)
elseif(CMAKE_Fortran_COMPILER_ID MATCHES PGI)
  include(${CMAKE_CURRENT_LIST_DIR}/PGI/Fortran.cmake)
elseif(CMAKE_Fortran_COMPILER_ID MATCHES XL)
  include(${CMAKE_CURRENT_LIST_DIR}/XL/Fortran.cmake)
elseif(CMAKE_Fortran_COMPILER_ID MATCHES Cray)
  include(${CMAKE_CURRENT_LIST_DIR}/Cray/Fortran.cmake)
elseif(CMAKE_C_COMPILER_ID MATCHES Fujitsu)
  # This is a workaround for the Fujitsu compiler where CMake does not pick up
  # the correct compier ID for fortran, USE WITH CARE
  set(CMAKE_Fortran_COMPILER_ID ${CMAKE_C_COMPILER_ID})
  include(${CMAKE_CURRENT_LIST_DIR}/Fujitsu/Fortran.cmake)
else()
  message(WARNING "Fortran compiler ${CMAKE_Fortran_COMPILER} never tested by the LSDALTON developers")
endif()

include(${CMAKE_CURRENT_LIST_DIR}/../int64.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/../bounds_checking.cmake)

# remove ; from local flag lists (for pretty-printing)
string(REPLACE ";" " " _LSDALTON_Fortran_FLAGS "${LSDALTON_Fortran_FLAGS}")
string(REPLACE ";" " " _LSDALTON_Fortran_FLAGS_${CMAKE_BUILD_TYPE} "${LSDALTON_Fortran_FLAGS_${CMAKE_BUILD_TYPE}}")
message(STATUS "Fortran compiler flags : ${CMAKE_Fortran_FLAGS} ${_LSDALTON_Fortran_FLAGS} ${CMAKE_Fortran_FLAGS_${CMAKE_BUILD_TYPE}} ${_LSDALTON_Fortran_FLAGS_${CMAKE_BUILD_TYPE}}")
