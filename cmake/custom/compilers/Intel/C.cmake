list(APPEND LSDALTON_C_FLAGS
  "-g"
  "-wd981"
  "-wd279"
  "-wd383"
  "-wd1572"
  "-wd1777"
  "-restrict"
  "-DRESTRICT=restrict"
  )

if(DEFINED MKL_FLAG)
  list(APPEND LSDALTON_C_FLAGS
    ${MKL_FLAG}
    )
endif()

list(APPEND LSDALTON_C_FLAGS_DEBUG
  "-O0"
  "-g"
  "-traceback"
  )

list(APPEND LSDALTON_C_FLAGS_RELEASE
  "-O3"
  "-ffast-math"
  "-funroll-loops"
  "-ftree-vectorize"
  "-Wno-unused"
  )
