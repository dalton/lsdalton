option(ENABLE_PYTHON_INTERFACE "Enable Python interface through CFFI" OFF)

# At least 3.6 required, since earlier versions reached end-of-life
if(ENABLE_PYTHON_INTERFACE)
  # Python is required to get the (optional) Python interface working
  find_package(Python 3.6 REQUIRED COMPONENTS Development Interpreter)
else()
  # Python is _always_ needed to run tests with Radovan's runtest
  find_package(Python 3.6 REQUIRED COMPONENTS Interpreter)
endif()

# TODO the PYMOD_INSTALL_* business here _might_ have to be revisited
# Note that CMake defines Python_SITEARCH _almost_ for this purpose
if(NOT DEFINED PYMOD_INSTALL_LIBDIR)
  message(STATUS "Setting (unspecified) option PYMOD_INSTALL_LIBDIR: python")
  set(PYMOD_INSTALL_LIBDIR "python" CACHE STRING "Location within CMAKE_INSTALL_LIBDIR to which Python modules are installed" FORCE)
else()
  message(STATUS "Setting option PYMOD_INSTALL_LIBDIR: ${PYMOD_INSTALL_LIBDIR}")
  set(PYMOD_INSTALL_LIBDIR "${PYMOD_INSTALL_LIBDIR}" CACHE STRING "Location within CMAKE_INSTALL_LIBDIR to which Python modules are installed" FORCE)
endif()
file(TO_NATIVE_PATH "lib/${PYMOD_INSTALL_LIBDIR}/pylsdalton" PYMOD_INSTALL_FULLDIR)

# Get file extension for Python module
execute_process(
  COMMAND
    "${Python_EXECUTABLE}" "-c" "from distutils import sysconfig as s;print(s.get_config_var('EXT_SUFFIX'))"
  RESULT_VARIABLE _PYTHON_SUCCESS
  OUTPUT_VARIABLE Python_MODULE_EXTENSION
  ERROR_VARIABLE _PYTHON_ERROR_VALUE
  OUTPUT_STRIP_TRAILING_WHITESPACE
  )
