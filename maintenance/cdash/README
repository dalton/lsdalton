
Contributing to the nightly testing dashboard
---------------------------------------------

It is very easy to contribute to the LSDALTON nightly testing dashboard:
http://repo.ctcc.no/CDash/index.php?project=LSDALTON

You don't need to install anything extra except cmake.
It is as easy as typing:
$ ./setup [--flags] -D BUILDNAME="my-build-name"
$ cd build
$ make Experimental

Make Experimental compiles the code and runs all tests. The result of "make
Experimental" appears automatically on the Dashboard.

Once "make Experimental" works you can create a script to run "make Nightly".
To get inspired have a look at the cdash example recipes in this directory.

Note that Nightly will run version of the source code corresponding to 00:00:00 CEST,
even if it is executed at 8 am - this is a good thing but might
surprise you at the beginning. Experimental tests the version at the time of
execution.

If you have an iPhone you will find this useful:
http://repo.ctcc.no/CDash/iphone/project.php?project=LSDALTON
