subroutine lsmpi_init(OnMaster)
#ifdef VAR_ENABLE_TENSORS
   use tensor_interface_module,only: tensor_initialize_interface, &
        & tensor_comm_null
#endif
   use memory_handling, only: mem_allocated_global
   use background_buffer_module, only: max_n_pointers, buf_realk
   use lsparameters
#ifdef VAR_MPI
   use precision
   use lstiming
   use lsmpi_Buffer
   use lsmpi_Bcast
   use lsmpi_typeParam
   use lsmpi_param
   use lsmpi_type
   use lsmpi_op
   use lsmpi_module
   use infpar_module
   use ls_env
#ifdef VAR_SCALAPACK  
   use matrix_operations_scalapack
#endif
   implicit none
   logical, intent(inout) :: OnMaster
   integer(kind=ls_mpik)  :: ierr, comm,provided
#ifdef VAR_CHEMSHELL
   integer(kind=ls_mpik)  :: lsdalton_chemsh_comm
   external lsdalton_chemsh_comm
#endif

   nLog      = 0
   nDP       = 0
   nInteger4 = 0
   nInteger8 = 0
   nCha      = 0
   comm      = 0

   if (call_mpi_init) then
#ifdef VAR_CHEMSHELL
     MPI_COMM_LSDALTON = lsdalton_chemsh_comm()
#else
     !This used to be
     call MPI_INIT( ierr )
     
     !Warning this should be done using

     !call MPI_INIT_THREAD( MPI_THREAD_FUNNELED, provided, ierr )
     !IF(MPI_THREAD_FUNNELED.NE.provided)print*,'WARNING MPI provided .NE. required'

     !but that seem to give stalls on MB-Grendel-ifort14-mpi-omp
     
     !The MPI_INIT means that all MPI calls are done outside
     !OpenMP regions by a single thread.
     !If you want to do MPI calls inside OpenMP regions you
     !may need MPI_THREAD_FUNNELED, MPI_THREAD_SERIALIZED or
     !MPI_THREAD_MULTIPLE

     MPI_COMM_LSDALTON        = MPI_COMM_WORLD
#endif
   endif

   !Set default mpi message sizes
   SPLIT_MPI_MSG      = 100000000
   MAX_SIZE_ONE_SIDED =  12500000

   lsmpi_enabled_comm_procs = .false.

   !TEST WHETHER THE MPI VARIABLES AND THE INTERNALLY DECLARED ls_mpik have the
   !same kind
   if(huge(ierr) /= huge(MPI_COMM_WORLD))then
      print *,"WARNING: THE MPI INTRINSIC VARIABLES AND THE ls_mpik ARE OF&
      & DIFFERENT KIND, YOUR JOB IS LIKELY TO FAIL",huge(ierr),huge(MPI_COMM_WORLD)
   endif

   call get_rank_for_comm( MPI_COMM_LSDALTON, infpar%mynum  )
   call get_size_for_comm( MPI_COMM_LSDALTON, infpar%nodtot )
   !default number of nodes to be used with scalapack - can be changed
   infpar%ScalapackGroupSize = infpar%nodtot
   infpar%ScalapackFORCEMEMDIST=.FALSE.
   infpar%ScalapackFORCEMEMDISTnbast=-1
   infpar%ScalapackMPIIO=.FALSE.
#ifdef VAR_SCALAPACK  
   scalapack_mpi_set = .FALSE.
#endif   

   infpar%master = int(0,kind=ls_mpik);

   !CHECK IF WE ARE ON THE MAIN MAIN MAIN MASTER PROCESS (NOT IN LOCAL GROUP,
   !NOT A SPAWNED PROCESS)
   OnMaster = (infpar%mynum==infpar%master)

   ! Set rank, sizes and communcators for local groups
   ! to be identical to those for world group by default.
   call lsmpi_default_mpi_group

   ! Assume that there will be local jobs
   infpar%lg_morejobs=.true.

   !tensor initialization
#ifdef VAR_ENABLE_TENSORS
   call tensor_initialize_interface(infpar%lg_comm, mem_ctr=mem_allocated_global,pdm_slaves_signal=PDMA4SLV)
#endif

#else
   logical, intent(inout) :: OnMaster
   !IF NOT COMPILED WITH MPI SET MASTER = TRUE
   OnMaster = .true.
#ifdef VAR_ENABLE_TENSORS
   call tensor_initialize_interface(tensor_comm_null, mem_ctr=mem_allocated_global )
#endif
#endif


end subroutine lsmpi_init 

subroutine lsmpi_slave(comm)
   use precision
   use memory_handling
#ifdef VAR_MPI
   use lsparameters
   use lstiming
   use infpar_module
   use lsmpi_param
   use lsmpi_op
   use lsmpi_Bcast
   use lsmpi_type
   use lsmpi_test
   use integralinterfaceMod
#ifdef VAR_DEC
   use dec_driver_slave_module
   use lofex_tools
#endif
#ifdef VAR_ENABLE_TENSORS
   use tensor_interface_module
#endif
#ifdef VAR_SCALAPACK  
   use matrix_operations_scalapack
#endif
   implicit none
   !> Communicator from which task is to be received
   integer(kind=ls_mpik) :: comm 
   integer(kind=ls_mpik) :: ierr
   integer :: job
   logical :: stay_in_slaveroutine
   real(realk) bg_memory,MemNotInBackgroundBuffer
   integer(kind=long) :: bytes_to_alloc

   stay_in_slaveroutine = .true.

   do while(stay_in_slaveroutine)

      call time_start_phase(PHASE_IDLE)
      call ls_mpibcast(job,infpar%master,comm)
      call time_start_phase(PHASE_WORK)

      select case(job)
      case(MATRIXTY);
         call lsmpi_set_matrix_type_slave
      case(MATRIXTY2);
         call lsmpi_set_matrix_tmp_type_slave
      case(LSGETINT);
         call lsmpi_getIntegrals_Slave(comm)
      case(LSJENGIN);
         call lsmpi_jengine_Slave(comm)
      case(LSLINK);
         call lsmpi_linK_Slave(comm)
      case(DFTSETFU);
         call lsmpi_setSlaveFunc
      case(DFTADDFU);
         call lsmpi_addSlaveFunc
      case(DFTSETCAM);
         call mpi_dftSetCamParam_slave(comm)
      case(LSMPI_IIDFTKSM);
         call lsmpi_II_DFT_KSM_Slave(comm)
      case(LSMPI_IIDFTABSVALOVERLAP);
         call lsmpi_II_DFT_ABSVALOVERLAP_Slave(comm)
      case(LSMPI_IIDFTKSME);
         call lsmpi_II_DFT_KSME_Slave(comm)
      case(IIDFTGEO);
         call lsmpi_geoderiv_molgrad_Slave(comm)
      case(IIDFTLIN);
         call lsmpi_linrsp_Slave(comm)
      case(IIDFTQRS);
         call lsmpi_quadrsp_Slave(comm)
      case(IIDFTMAG);
         call lsmpi_magderiv_Slave(comm)
      case(IIDFTMAL);
         call lsmpi_magderiv_linrsp_Slave(comm)
      case(IIDFTGKS);
         call lsmpi_geoderiv_F_Slave(comm)
      case(IIDFTGLR);
         call lsmpi_geoderiv_G_Slave(comm)
      case(IISCREENINIT);
         call II_screeninit(comm)
      case(IISCREEN);
         call II_bcast_screen(comm)
      case(IISCREENFREE);
         call II_screenfree(comm)
         ! DEC MP2 integrals and amplitudes
#ifdef VAR_DEC
      case(MP2INAMP);
         call MP2_integrals_and_amplitudes_workhorse_slave
      case(RIMP2INAMP);
         call RIMP2_integrals_and_amplitudes_slave
      case(SOSRIMP2ENERGY);
         call SOSRIMP2_energy_slave
      case(RIMP2INAMPAR);
         call PerformAuxReduction_slave
      case(LSTHCRIMP2INAMP);
         call lsquit('No LSTHCRIMP2_integrals_and_amplitudes_slave',-1)
      case(RIMP2FULL);
         call full_canonical_rimp2_slave
      case(RIMP2F12FULL);         
         call full_canonical_rimp2f12_slave
      case(LSTHCRIMP2FULL);
!         call full_canonical_ls_thc_rimp2_slave
      case(CANONMP2FULL);
         call full_canonical_mp2_slave
      case(DEC_SETTING_TO_SLAVES);
         call set_dec_settings_on_slaves
      case(CCSDDATA);
         call ccsd_data_preparation
      case(MO_RIINTEGRAL_SIMPLE);
         call get_mo_riintegral_par_slave         
      case(MO_INTEGRAL_SIMPLE);
         call get_mo_integral_par_slave
      case(CCSDSLV4E2);
         call calculate_E2_and_permute_slave
!      case(RPAGETRESIDUAL);
!         call rpa_res_slave
      case(RPAGETFOCK);
         call rpa_fock_slave
      case(CCGETGMO);
         call cc_gmo_data_slave
      case(MOCCSDDATA);
         call moccsd_data_slave
      case(CCSDPTSLAVE_INFO);
         call ccsdpt_slave_info
      case(CCSDPTSLAVE_WORK);
         call ccsdpt_slave_work
      case(SET_FORCE_CRASH);
         call ls_mpibcast(force_crash,infpar%master,comm)
      case(SIMPLE_MP2_PAR);
         call get_simple_parallel_mp2_residual_slave
      !case(COMM_CCS_TRANSFO);
      !   call slaves_get_ccs_transfo
      !case(COMM_RICC2_RESIDUAL);
      !   call slaves_get_ricc2_residual
      case(DECDRIVER);
         call main_fragment_driver_slave
      case(CCSD_MULT_PAR)
         call get_ccsd_multipliers_parallel_slave()
      case(LOFEXDRIVER);
         call lofex_driver_mpi()
      case(FULL_CANONICAL_UNREST_MP2)
         call full_canon_unrest_mp2_slave
#endif
      case(GROUPINIT);
         call init_mpi_groups_slave
         ! DEC driver - main loop
      case(DEFAULTGROUPS);
         call lsmpi_default_mpi_group
      case(PDMA4SLV);
#ifdef VAR_ENABLE_TENSORS
         call pdm_tensor_slave
#else
         call lsquit('tensors not enabled',-1)
#endif
#ifdef VAR_SCALAPACK
      case(GRIDINIT);
         IF(scalapack_member)THEN
            call PDM_GRIDINIT_SLAVE
         ENDIF
      case(GRIDEXIT);
         IF(scalapack_member)THEN
            call PDM_GRIDEXIT_SLAVE
         ENDIF
      case(PDMSLAVE);
         IF(scalapack_member)THEN
            !inside the PDM_SLAVE the 
            !scalapack_comm communicator is used
            !only call PDM_SLAVE if this rank is 
            !a member of this communicator
            call PDM_SLAVE()
         ENDIF
#endif
      case(INITSLAVETIME);
         call init_slave_timers_slave(comm)
      case(GETSLAVETIME);
         call get_slave_timers_slave(comm)
      case(LSMPITEST);
         call test_mpi(comm)
      case(LSMPIPRINTINFO);
         call lsmpi_print_mem_info(6,.TRUE.)
      case(SET_GPUMAXMEM);
         call ls_mpibcast(GPUMAXMEM,infpar%master,comm)
      case(SET_SPLIT_MPI_MSG);
         call ls_mpibcast(SPLIT_MPI_MSG,infpar%master,comm)
#ifdef VAR_ENABLE_TENSORS
         call tensor_set_mpi_msg_len(int(SPLIT_MPI_MSG,kind=long))
#endif
      case(SET_MAX_SIZE_ONE_SIDED);
         call ls_mpibcast(MAX_SIZE_ONE_SIDED,infpar%master,comm)
      case(EARLYBACKGROUNDBUFFER);         
         call ls_mpibcast(bg_memory,infpar%master,comm)
         call ls_mpibcast(MemNotInBackgroundBuffer,infpar%master,comm)
         bytes_to_alloc = int(bg_memory*1.0E+9_realk,kind=8)
         call mem_init_background_alloc(bytes_to_alloc,MemNotInBackgroundBuffer)
      case(INIT_BG_BUF);
         call mem_init_background_alloc_slave(comm)
      case(FREE_BG_BUF);
         call mem_free_background_alloc_slave(comm)
      case(DEBUGMPIALLOC);
         infpar%mpimemdebug = .TRUE.
         call ls_mpibcast(infpar%pagesize,infpar%master,comm)
         call ls_mpibcast(infpar%hugepagesize,infpar%master,comm)

         !##########################################
         !########  QUIT THE SLAVEROUTINE ##########
         !##########################################

         ! Quit but there are more local jobs - used for local group handling
      case(QUITMOREJOBS); 
         infpar%lg_morejobs   = .true.
         stay_in_slaveroutine = .false.
         ! Quit there are NO more local jobs - used for local group handling
      case(QUITNOMOREJOBS); 
         infpar%lg_morejobs   = .false.
         stay_in_slaveroutine = .false.
      case(LSMPIQUIT);  
         stay_in_slaveroutine = .false.
!      case()
!         !call free_persistent_array()
!         call lsmpi_finalize(6,.FALSE.)
!         stay_in_slaveroutine = .false.
      case default
         print*,'MPI JOB=',job,' not recognized in lsmpi_slave'
         call lsquit('MPI JOB not recognized in lsmpi_slave',-1)
      end select

   end do
#else
   call lsquit("ERROR(lsmpi_slave): This is a non-mpi build, this routine should not be called",-1)
#endif

end subroutine lsmpi_slave

!> Waiting position for DEC slaves to receive job to be carried out within local group,
!> i.e., the communicator is always infpar%lg_comm.
!> \author Kasper Kristensen 
!> \date April 2012
!!$    subroutine lsmpi_local_slave(job)
!!$      use infpar_module
!!$      use lsmpi_type
!!$      implicit none
!!$      !> Give last job as output, because it is convenient
!!$      !> to distinguish between two different types of quit statements:
!!$      !> job=0, quit local slave - but you may be sent back to local slave later
!!$      !> job=-1, quit local slave - there are no more jobs to be done
!!$      integer :: job
!!$print *, 'entering local slave first'
!!$100   call ls_mpibcast(job,0,infpar%lg_comm)
!!$print *, 'entering local slave - job', job
!!$      select case(job)
!!$      case(MP2INAMP);        ! MP2
!!$         call MP2_integrals_and_amplitudes_workhorse_slave
!!$      case(CCSDDATA);       ! CC2 or CCSD
!!$         call ccsd_data_preparation
!!$      case(0) ! quit local slave, but more jobs (see above)
!!$         goto 200
!!$      case(-1) ! quit local slave, no more jobs (see above)
!!$         goto 200
!!$      end select
!!$
!!$      goto 100
!!$
!!$200   return
!!$
!!$    end subroutine lsmpi_local_slave



