#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > acene_3_pariK.info <<'%EOF%'
   acene_3_pariK
   -------------
   Molecule:         C60/6-31G
   Wave Function:    HF
   Profile:          Exchange Matrix
   CPU Time:         9 min 30 seconds
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > acene_3_pariK.mol <<'%EOF%'
BASIS
cc-pVTZ Aux=cc-pVTZdenfit
ring 4
EXCITATION DIAGNOSTIC
Atomtypes=2 Nosymmetry 
Charge=6.0 Atoms=18
C        0.0000000000            1.3688152776            0.0000000000
C        0.0000000000           -1.3688152776            0.0000000000
C        2.3289797725            2.6510018988            0.0000000000
C       -2.3289797725            2.6510018988            0.0000000000
C        2.3289797725           -2.6510018988            0.0000000000
C       -2.3289797725           -2.6510018988            0.0000000000
C        4.6180629979            1.3685710635            0.0000000000
C       -4.6180629979            1.3685710635            0.0000000000
C        4.6180629979           -1.3685710635            0.0000000000
C       -4.6180629979           -1.3685710635            0.0000000000
C        6.9971809565            2.6559610097            0.0000000000
C       -6.9971809565            2.6559610097            0.0000000000
C        6.9971809565           -2.6559610097            0.0000000000
C       -6.9971809565           -2.6559610097            0.0000000000
C        9.2142648358            1.3487785773            0.0000000000
C       -9.2142648358            1.3487785773            0.0000000000
C        9.2142648358           -1.3487785773            0.0000000000
C       -9.2142648358           -1.3487785773            0.0000000000
Charge=1.0 Atoms=12
H        2.3295052168            4.7016079742            0.0000000000
H       -2.3295052168            4.7016079742            0.0000000000
H        2.3295052168           -4.7016079742            0.0000000000
H       -2.3295052168           -4.7016079742            0.0000000000
H        6.9973998483            4.7051382039            0.0000000000
H       -6.9973998483            4.7051382039            0.0000000000
H        6.9973998483           -4.7051382039            0.0000000000
H       -6.9973998483           -4.7051382039            0.0000000000
H       10.9999455809            2.3504170405            0.0000000000
H      -10.9999455809            2.3504170405            0.0000000000
H       10.9999455809           -2.3504170405            0.0000000000
H      -10.9999455809           -2.3504170405            0.0000000000

%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > acene_3_pariK.dal <<'%EOF%'
**PROFILE
.EXCHANGE
**INTEGRALS
.PARI-K
**WAVE FUNCTIONS
.HF
*DENSOPT
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >acene_3_pariK.check
cat >> acene_3_pariK.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi
CRIT1=`$GREP "Exchange energy, mat_dotproduct\(D,K\)\= * \-XXX\.XXXXXX" $log | wc -l`
TEST[1]=`expr  $CRIT1`
CTRL[1]=1
ERROR[1]="ENERGY NOT CORRECT -"

PASSED=1
for i in 1
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo PROF ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
