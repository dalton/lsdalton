#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > 00012atoms_0210basfunc_gc_XC.info <<'%EOF%'
   00012atoms_0210basfunc_gc_XC
   -------------
   Molecule:         BN benzene like structure/cc-pVTZ
   Wave Function:    B3LYP
   Profile:          Exchange-Correlation Matrix
   CPU Time:         ~20 seconds 
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > 00012atoms_0210basfunc_gc_XC.mol <<'%EOF%'
ATOMBASIS
BN benzene like structure
in B3LYP/6-31G(d,p) optimized geometry
AtomTypes=3 Nosymmetry Angstrom
Charge=5.0 Atoms=3 Bas=cc-pVTZ
B     1.25495700     0.00000000     0.72455000
B     0.00000000     0.00000000    -1.44910000
B    -1.25495700     0.00000000     0.72455000
Charge=7.0 Atoms=3 Bas=cc-pVTZ
N     0.00000000     0.00000000     1.44910000
N     1.25495700     0.00000000    -0.72455000
N    -1.25495700     0.00000000    -0.72455000
Charge=1.0 Atoms=6 Bas=cc-pVDZ
H     0.00000000     0.00000000     2.45970000
H     2.28907800     0.00000000     1.32160000
H     2.13016300     0.00000000    -1.22985000
H     0.00000000     0.00000000    -2.64320000
H    -2.13016300     0.00000000    -1.22985000
H    -2.28907800     0.00000000     1.32160000
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > 00012atoms_0210basfunc_gc_XC.dal <<'%EOF%'
**PROFILE
.XC
**WAVE FUNCTIONS
.DFT
B3LYP
*DENSOPT
.START
H1DIAG
.GCBASIS
*DFT INPUT
.GRID4
.GRID TYPE
BLOCK
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >00012atoms_0210basfunc_gc_XC.check
cat >> 00012atoms_0210basfunc_gc_XC.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi
CRIT1=`$GREP "Exchange Correlation energy = * \-40\.092300488" $log | wc -l`
TEST[1]=`expr  $CRIT1`
CTRL[1]=2
ERROR[1]="ENERGY NOT CORRECT -"

PASSED=1
for i in 1
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo PROF ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
