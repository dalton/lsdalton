#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > PP_pariK.info <<'%EOF%'
   PP_pariK
   -------------
   Molecule:         C60/6-31G
   Wave Function:    HF
   Profile:          Exchange Matrix
   CPU Time:         9 min 30 seconds
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > PP_pariK.mol <<'%EOF%'
BASIS
cc-pVTZ Aux=cc-pVTZdenfit
PP
EXCITATION DIAGNOSTIC
Atomtypes=3 Nosymmetry
Charge=6.0 Atoms=10
C       2.2706222585            4.7730173284            0.0000000000
C      -2.2706222585            4.7730173284            0.0000000000
C       0.0000000000            6.1111121474            0.0000000000
C       2.2816826330            2.1387165841            0.0000000000
C      -2.2816826330            2.1387165841            0.0000000000
C       0.0000000000            0.7923464742            0.0000000000
C       2.1197819099           -3.4258994126            0.0000000000
C      -2.1197819099           -3.4258994126            0.0000000000
C       1.3395610232           -5.9107336961            0.0000000000
C      -1.3395610232           -5.9107336961            0.0000000000
Charge=7.0 Atoms=1
N       0.0000000000           -1.8918071168            0.0000000000
Charge=1.0 Atoms=9
H       4.0627785591            5.7776147665            0.0000000000
H      -4.0627785591            5.7776147665            0.0000000000
H       0.0000000000            8.1643586785            0.0000000000
H       4.0806204132            1.1593701311            0.0000000000
H      -4.0806204132            1.1593701311            0.0000000000
H       4.0065792553           -2.6563688490            0.0000000000
H      -4.0065792553           -2.6563688490            0.0000000000
H       2.5755781273           -7.5400732042            0.0000000000
H      -2.5755781273           -7.5400732042            0.0000000000
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > PP_pariK.dal <<'%EOF%'
**PROFILE
.EXCHANGE
**INTEGRALS
.PARI-K
**WAVE FUNCTIONS
.HF
*DENSOPT
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >PP_pariK.check
cat >> PP_pariK.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi
CRIT1=`$GREP "Exchange energy, mat_dotproduct\(D,K\)\= * \-XXX\.XXXXXX" $log | wc -l`
TEST[1]=`expr  $CRIT1`
CTRL[1]=1
ERROR[1]="ENERGY NOT CORRECT -"

PASSED=1
for i in 1
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo PROF ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
