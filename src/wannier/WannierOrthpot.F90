!> @file WannierOrthpot.F90
!> Module for computing the Wannier orthogonalising potential
module wannier_orthpot

  use precision
  use wannier_types, only: &
       wannier_config, &
       bravais_lattice
  use wlattice_domains, only: ltdom_init, &
       ltdom_free, &
       lattice_domains_type, &
       ltdom_getdomain, &
       ltdom_getlayer
  use lattice_storage!, only: lmatrixp, lts_init, lts_free, lts_get
  use matrix_module, only: &
       matrix
  use wannier_utils, only: &
       wu_indxtocoor
  use wannier_test_module
  use matrix_operations, only: &
       mat_daxpy, &
       mat_free, &
       mat_init, &
       mat_mul, &
       mat_zero, &
       mat_trans, &
       mat_print, &
       mat_assign, &
       mat_identity, &
       mat_section
  use wannier_dens

  implicit none
  private

  public :: w_calc_orthpot, w_calc_projocc
  
contains
  
  !> @brief Compute the orthogonalising potential for a priori Wannier
  !> @param mos MO coefficients in lattice storage
  !> @param blat Bravais lattice
  !> @param orthpot Orthogonalising potential in lattice storage
  !> @param smat Overlap matrix in lattice storage
  !> @param thr Screening threshold
  !> @author Karl Leikanger, Elisa Rebolini
  !> @date April 2016
  subroutine w_calc_orthpot(mos, wconf, orthpot, smat, lupri, luerr)
    type(lmatrixp), intent(in)          :: mos, smat
    type(llmatrixp), intent(inout)      :: orthpot
    type(wannier_config), intent(inout) :: wconf
    integer,intent(in)                  :: lupri,luerr

    real(realk) :: thr
    type(bravais_lattice) :: blat
    type(lattice_domains_type) :: dom_d, dom_ao
    integer :: l1, l2, b, indx_l1, indx_l2, indx_b
    type(matrix), pointer :: sigma_pt1, sigma_pt2, orthpot_pt, smat_pt1, mos_ptr
    type(matrix) :: occMO
    character(len = 1) :: c1
    integer :: norb
    type(lmatrixp) :: sigma

    blat = wconf%blat
    thr = wconf%screening_thr

    if (wconf%scf_shuklaAO) then
       norb = wconf%nocc
       write(lupri,*) 'Orthogonalising potential added to the occupied orbitals only'
       write(luerr,*) 'Orthogonalising potential added to the occupied orbitals only'
    else
       norb = mos%ncol
    endif
    
    call ltdom_init(dom_d, blat%dims, 2*maxval(mos%cutoffs))
    call ltdom_init(dom_ao, blat%dims, maxval(mos%cutoffs))
    call ltdom_getdomain(dom_d, 2*mos%cutoffs, incl_redundant=.true.)
    call ltdom_getdomain(dom_ao, mos%cutoffs, incl_redundant=.true.)

    call lts_init(sigma, blat%dims, 2*mos%cutoffs, mos%nrow, norb, mos%redundant)

    call mat_init(occMO,mos%nrow,norb)
    call mat_zero(occMO)
    
    ! calculate sigma^l1_{\mu p}
    call lts_zero(sigma)
    do l1 = 1, dom_d%nelms; indx_l1 = dom_d%indcs(l1)

       sigma_pt1 => lts_get(sigma, indx_l1)
       if (.not. associated(sigma_pt1)) then
          call lts_mat_init(sigma, indx_l1)
          sigma_pt1 => lts_get(sigma, indx_l1)
          call mat_zero(sigma_pt1)
       endif

       do l2 = 1, dom_ao%nelms; indx_l2 = dom_ao%indcs(l2)
          smat_pt1 => lts_get(smat, abs(indx_l2 - indx_l1))
          if (associated(smat_pt1)) then
             mos_ptr => lts_get(mos, -indx_l2)
             if (associated(mos_ptr)) then
                call mat_section(mos_ptr,1,mos%nrow,1,norb,occMO)
                if (indx_l2 - indx_l1 >= 0) then
                   c1 = 'N'
                else
                   c1 = 'T'
                endif
                call mat_mul(smat_pt1, occMO, c1, 'N', 1.0_realk, 1.0_realk, sigma_pt1)
             endif
          endif
       enddo

    enddo

    ! calculate [P_0]_{l1, l2} = sum_{b /= 0} sigma^{l1-b}*(sigma^{l2-b})^T
    call lts_zero(orthpot)
    do l1 = 1, dom_ao%nelms; indx_l1 = dom_ao%indcs(l1)
       do l2 = 1, dom_ao%nelms; indx_l2 = dom_ao%indcs(l2)

          orthpot_pt => lts_get(orthpot, indx_l1, indx_l2)
          if (.not. associated(orthpot_pt)) then
             call lts_mat_init(orthpot, indx_l1, indx_l2)
             orthpot_pt => lts_get(orthpot, indx_l1, indx_l2)
             call mat_zero(orthpot_pt)
          endif

          do b = 1, dom_ao%nelms; indx_b = dom_ao%indcs(b)
             if (indx_b /= 0) then
                sigma_pt1 => lts_get(sigma, indx_l1 - indx_b)
                if (associated(sigma_pt1)) then
                   sigma_pt2 => lts_get(sigma, indx_l2 - indx_b)
                   if (associated(sigma_pt2)) then
                      call mat_mul(sigma_pt1, sigma_pt2, 'N', 'T', 1.0_realk, 1.0_realk, orthpot_pt)
                   endif
                endif
             endif
          enddo

       enddo
    enddo

    call ltdom_free(dom_d)
    call ltdom_free(dom_ao)
    call lts_free(sigma)
    call mat_free(occMO)

  end subroutine w_calc_orthpot

  !> @brief Computes the projector onto the virtual space
  !>
  !> Q = 1 - DS
  !> Q_{0 nu, L1 mu} = delta_{0 L1} - sum_L2 sum_kappa D_{0 nu, L2 kappa} S{L2 kappa, L1 mu}  
  !> @author Elisa Rebolini
  !> @date September 2016
  
  subroutine w_calc_projocc(projMO,dens,smat,wconf,lupri,luerr)
    
    type(lmatrixp), intent(inout)          :: dens, smat
    type(lmatrixp), intent(inout)       :: projMO
    type(wannier_config), intent(inout) :: wconf
    integer,intent(in)                  :: lupri,luerr

    type(lattice_domains_type) :: dom_d, dom_ao
    integer                    :: l1, l2, indx_l1, indx_l2
    type(matrix), pointer      :: proj_pt, smat_pt, dens_pt
    character(len = 1)         :: c1,c2
    
    call ltdom_init(dom_d, wconf%blat%dims, 2*maxval(projMO%cutoffs))
    call ltdom_init(dom_ao, wconf%blat%dims, maxval(projMO%cutoffs))
    call ltdom_getdomain(dom_d, 2*projMO%cutoffs, incl_redundant=.true.)
    call ltdom_getdomain(dom_ao, projMO%cutoffs, incl_redundant=.true.)

    do l1 = 1, dom_ao%nelms; indx_l1 = dom_ao%indcs(l1)

       proj_pt => lts_get(projMO, indx_l1)
       if (.not. associated(proj_pt)) then
          call lts_mat_init(projMO, indx_l1)
          proj_pt => lts_get(projMO, indx_l1)
          if (indx_l1 == 0) then
             call mat_identity(proj_pt)
          else
             call mat_zero(proj_pt)
          endif
       endif

       do l2 = 1, dom_d%nelms; indx_l2 = dom_d%indcs(l2)
          smat_pt => lts_get(smat, abs(indx_l1 - indx_l2))
          if (associated(smat_pt)) then
             dens_pt => lts_get(dens, abs(indx_l2))
             if (associated(dens_pt)) then
                if (indx_l1 - indx_l2 >= 0) then
                   c1 = 'N'
                else
                   c1 = 'T'
                endif
                if (indx_l2 >= 0) then
                   c2 = 'N'
                else
                   c2 = 'T'
                endif
                call mat_mul(dens_pt, smat_pt, c2, c1, -1.0_realk, 1.0_realk, proj_pt)
             endif
          endif
       enddo

    enddo

    call ltdom_free(dom_d)
    call ltdom_free(dom_ao)

    if (wconf%debug_level >= 0) then
       call wtest_projocc(projMO,dens,smat,wconf,lupri,luerr)
    endif
  end subroutine w_calc_projocc


  

end module wannier_orthpot
