!> @file WannierFock.F90
!>Computes the Wannier Fock matrix elements in AO basis
!> (without the orthogonalising potential)

module wannier_fockAO

  use precision
  use wannier_types, only: &
       wannier_config, &
       bravais_lattice
  use molecule_typetype, only: &
       moleculeinfo
  use molecule_type, only: &
       copy_molecule, &
       free_moleculeinfo
  use typedef!, only: &
       !typedef_setmolecules
  use typedeftype, only: &
       daltoninput, &
       lssetting
  use wlattice_domains, only: &
       ltdom_init, &
       ltdom_free, &
       ltdom_getlayer, &
       lattice_domains_type, &
       ltdom_getdomain, &
       ltdom_getintersect, &
       ltdom_contains
  use lattice_storage, only: &
       lmatrixp, &
       llmatrixp, &
       linteger, &
       lmatrixarrayp, &
       lts_mat_init, &
       lts_init, &
       lts_free, &
       lts_get, &
       lts_axpy, &
       lts_store, &
       lts_load, &
       lts_zero, &
       lts_print, &
       lts_copy
  use matrix_module, only: &
       matrix
  use timings_module
  use memory_handling, only: &
       mem_alloc, mem_dealloc
  use wannier_fmm, only: &
       pbc_mat_redefine_q, &
       pbc_multipl_moment_matorder
  use wannier_mulliken, only: &
       w_calc_mulliken_pot
  use wannier_utils, only: &
       wu_indxtocoor, &
       wu_indxtolatvec, &
       wtranslate_molecule, &
       wcopy_atompositions
  use wannier_test_module
  use matrix_operations, only: &
       mat_daxpy, &
       mat_free, &
       mat_init, &
       mat_mul, &
       mat_zero, &
       mat_dotproduct, &
       mat_abs_max_elm, &
       mat_trans, &
       mat_print, &
       mat_add
  use integralinterfacemod, only: &
       ii_get_coulomb_mat, &
       ii_get_exchange_mat, &
       II_get_nucel_mat
  use wannier_asymptotic_expansion, only: &
	   wcalc_JZAE, wcalc_ZAE
  use configurationType
  use integraloutput_type
  use LSparameters
  use ls_Integral_Interface
    
  implicit none
  private
  public :: w_calc_fockmat, ao_integrals, fmm_tensors

  type ao_integrals
     !> Overlap matrix
     type(lmatrixp) :: smat
     !> Coulombs integral screening: log(sqrt(G_ab))
     type(linteger) :: maxgab
     !> Nuc- nuc repulsion energy
     real(realk) :: enn
     !> Single particle integrals = (nuc - el attr) + el - kin e.
     type(lmatrixp) :: oneptoper
  end type ao_integrals

  type fmm_tensors
     !> FMM multipole moments
     type(lmatrixarrayp) :: sphmom
     !> FMM nuclear moments for the refcell
     real(realk), pointer :: nucmom(:)
     !> T - tensor (See: Bipolar exp. in Helgaker Bible)
     real(realk), pointer :: tlat(:, :)
  end type fmm_tensors
  
contains

  ! todo maxgab necc?. atl not farfieldc. (cutoffs alr set?)

  !> @brief Calculate the non redundant element of the fockmatrix.
  !> @author Karl R. Leikagner
  !> @date 2015
  !> @param refcell Description of the reference cell
  !> @param dens Density matrix on the lattice
  !> @param lsset Information on the integral setttings
  !> @param aoint AO integrals
  !> @param fmmt FMM tensor
  !> @param wconf Wannier configutation
  !> @param timings Timings for the calculation
  !> @param fockmat Fock matrix on the lattice
  !> @param lupri Default print-unit for output
  !> @param luerr Default print-unit for termination
  subroutine w_calc_fockmat( &
       & refcell, dens, lsset, aoint, fmmt, wconf, timings, fockmat, zmat, lupri, luerr)

    implicit none
    
    type(moleculeinfo), intent(in) :: refcell
    type(lmatrixp), intent(in) :: dens 
    type(lssetting), intent(inout) :: lsset
    type(ao_integrals), intent(inout) :: aoint
    type(fmm_tensors), intent(inout) :: fmmt
    type(wannier_config), intent(inout) :: wconf
    type(timings_type), intent(inout) :: timings
    type(lmatrixp),intent(inout) :: zmat
    
    type(lmatrixp) :: fockmat,full_oneptoper
    integer, intent(in) :: lupri, luerr

    call lts_zero(fockmat)

    ! Calculate electron repulsion. Add to fockmat. 
    call timings_start(timings, 'Fock - nearfield J')
    call wcalc_fock_j_nearfield(lsset, aoint%maxgab, wconf, &
         & refcell, dens, fockmat, zmat, lupri, luerr) !ej)
    call timings_stop(timings, 'Fock - nearfield J')

    if (wconf%debug_level > 5) then
       write (luerr,*) ''
       write (luerr,*) '***Fock - nearfield J'
       call lts_print(fockmat,luerr,1)
    endif

    ! Calculate farfield Asymptotic Expansion Coulomb. Add to fockmat.
    if (wconf%asymptotic_expansion) then
       call timings_start(timings, 'Fock - AE ')
       call wcalc_JZAE(fockmat, aoint%smat, dens, refcell, wconf, lupri, luerr)
       call timings_stop(timings, 'Fock - AE ')
    

       if (wconf%debug_level > 5) then
          write (luerr,*) ''
          write (luerr,*) '***Fock - nearfield + AE J + AE Z'
          call lts_print(fockmat,luerr,1)
          write (luerr,*) 'Test hermiticity of the Coulomb contribution'
          call wtest_redundant(fockmat,luerr)
       endif
    endif

    ! Calculate HF exchange. Add to fockmat.
    call timings_start(timings, 'Fock - HFX')
    call wcalc_fock_k(lsset, aoint%maxgab, wconf, &
         & refcell, dens, wconf%nf_cutoff, fockmat, lupri, luerr) !ek)
    call timings_stop(timings, 'Fock - HFX')

    if (wconf%debug_level > 5) then
       write (luerr,*) ''
       if (wconf%asymptotic_expansion) then
          write (luerr,*) '***Fock - J + HFX + AE Z'
       else
          write (luerr,*) '***Fock - nearfield J + HFX'
       endif
       call lts_print(fockmat,luerr,1)
    endif

    ! Add single particle part of the hamilton operator to fockmat
    call lts_axpy(1.0_realk, aoint%oneptoper, fockmat) !eh1= T + nuc_attr)

    if (wconf%debug_level > 5) then
       write (luerr,*) ''
       if (wconf%asymptotic_expansion) then
          write (luerr,*) '***Fock - J + HFX + Z'
       else
          write (luerr,*) '***Fock - NF J + HFX + NF Z'
       endif
       call lts_print(fockmat,luerr,1)
    endif

    if (wconf%debug_level > 5) then
       write (luerr,*) ''
       write (luerr,*) '***One particle operator'
       call lts_print(aoint%oneptoper,luerr,1)
    endif

    if (wconf%debug_level > 5) then
       write (luerr,*) ''
       write (luerr,*) '***Fock - before symmetrization'
       call lts_print(fockmat,luerr,1)
    endif
    
    call symmetrize_fock(fockmat,wconf,lupri,luerr)

    if (wconf%debug_level > 5) then
       write (luerr,*) ''
       write (luerr,*) '***Fock - after symmetrization'
       call lts_print(fockmat,luerr,1)
    endif
    
  end subroutine w_calc_fockmat


  !> @brief Dummy implementation of the HF exchange contribution to the Fock matrix
  !> @date April 2016
  !> @author Elisa Rebolini
  subroutine wcalc_fock_k(lsset, maxgab, wconf, &
       & refcell, dens, nf_cutoff, f_mat, lupri, luerr, e_k)

    integer, intent(in)               :: lupri, luerr, nf_cutoff(3)
    type(lssetting), intent(inout)    :: lsset
    type(lmatrixp), intent(in)        :: dens
    type(wannier_config), intent(in)  :: wconf
    type(linteger), intent(in)        :: maxgab
    type(lmatrixp), intent(inout)     :: f_mat
    type(moleculeinfo), intent(in)    :: refcell
    real(realk), intent(inout), optional :: e_k

    type(lattice_domains_type) :: dom_D, dom_F, dom_N
    integer                    :: nbast, l, m, n, indxL, indxM, indxN, iounit, i,j
    type(matrix)               :: dens0M,matK_0L,matK_tmp
    type(matrix),pointer       :: matF_0L, matK_0L_ptr
    logical                    :: lstat_0M
    type(moleculeinfo)         :: molN,molL,molNM 
    type(lmatrixp)             :: Kmat_debug
    character(len=50)          :: filename
    real(realk),pointer        :: int_0nLNM(:,:,:,:),Dtest(:,:),tmp(:,:)
    real(realk)                :: tmpK, transL(3), transN(3), transNM(3)

    nbast = dens%nrow

    if (wconf%debug_level > 5) then
       call lts_init(Kmat_debug, wconf%blat%dims, f_mat%cutoffs, nbast, nbast, &
            & wconf%no_redundant, .false.)
       call lts_zero(Kmat_debug)
    endif

    !Domain initialisation
    call ltdom_init(dom_D, wconf%blat%dims, maxval(dens%cutoffs))
    call ltdom_init(dom_F, wconf%blat%dims, maxval(f_mat%cutoffs))
    call ltdom_init(dom_N, wconf%blat%dims, maxval(maxgab%cutoffs))

    call ltdom_getdomain(dom_D, dens%cutoffs, incl_redundant=.true.)
    call ltdom_getdomain(dom_N, maxgab%cutoffs, incl_redundant=.true.)
    call ltdom_getdomain(dom_F, f_mat%cutoffs, incl_redundant=wconf%no_redundant)
     
    !Matrix allocation
    call mat_init(dens0M,nbast,nbast)
    call mat_init(matK_0L,nbast,nbast)
    call mat_init(matK_tmp,nbast,nbast)

    !Copies of the reference cell for the integral call
    call copy_molecule(refcell,molN,lupri)
    call copy_molecule(refcell,molL,lupri)
    call copy_molecule(refcell,molNM,lupri)

    !K_{0 mu, L nu}
    loopL: do l = 1, dom_F%nelms; indxL = dom_F%indcs(l)
       call mat_zero(matK_0L)
       call wcopy_atompositions(molL, refcell)
       transL = wu_indxtolatvec(wconf%blat, indxL)
       call wtranslate_molecule(molL, transL)

       loopM: do m = 1, dom_D%nelms; indxM = dom_D%indcs(m)
          call mat_zero(dens0M)
          !Warning: K_mn = sum_kl (mk|nl) D_lk  
          call lts_copy(dens,-indxM,lstat_0M,dens0M)
          if (lstat_0M) then
!!$             if ((wconf%debug_level > 10) .and. (indxM >= 0) .and. (indxM <=9) ) then
!!$                iounit=100
!!$                write (filename,'(A9,I1)') 'wannierK_',indxM
!!$                open(UNIT=iounit,FILE=filename,FORM="FORMATTED",STATUS="OLD", &
!!$                     & ACTION="WRITE",POSITION='APPEND')
!!$             endif

             loopN: do n = 1, dom_N%nelms; indxN = dom_N%indcs(n)
                call mat_zero(matK_tmp)
                call wcopy_atompositions(molN, refcell)
                transN = wu_indxtolatvec(wconf%blat, indxN)
                call wtranslate_molecule(molN, transN)
                call wcopy_atompositions(molNM, refcell)
                transNM =wu_indxtolatvec(wconf%blat, indxN+indxM)
                call wtranslate_molecule(molNM, transNM)

                call typedef_setmolecules( &
                     & lsset, refcell, 1, molN, 2, molL, 3, molNM, 4)

                call II_get_exchange_mat( &
                     & lupri, luerr, lsset, dens0M, 1, .false., matK_tmp)
                
                ! and add them to the K matrix
                call mat_daxpy(1.0_realk, matK_tmp, matK_0L)
                
             enddo loopN
          endif
       enddo loopM

       ! copy matK_0L (K_{0,indx1}) to the fock matrix (F_{0,indx1})
       matF_0L => lts_get(f_mat, indxL)
       if (.not. associated(matF_0L)) then
          call lts_mat_init(f_mat, indxL)
          matF_0L => lts_get(f_mat, indxL)
          call mat_zero(matF_0L)
       endif
       call mat_daxpy(1.0_realk, matK_0L, matF_0L)
           
       if (wconf%debug_level > 5) then
          matK_0L_ptr => lts_get(Kmat_debug, indxL)
          if (.not. associated(matK_0L_ptr)) then
             call lts_mat_init(Kmat_debug, indxL)
             matK_0L_ptr => lts_get(Kmat_debug, indxL)
             call mat_zero(matK_0L_ptr)
          endif
          call mat_daxpy(1.0_realk, matK_0L, matK_0L_ptr)
       endif
    enddo loopL

    if (wconf%debug_level > 5) then
       write (luerr,*) ''
       write (luerr,*) '***Fock - HFX'
       call lts_print(Kmat_debug, luerr,1)
       write(luerr,*) 'Test redundancy of the K matrix'
       call wtest_redundant(Kmat_debug,luerr)
       call lts_free(Kmat_debug)
    endif
    
    !Free domains
    call ltdom_free(dom_D)
    call ltdom_free(dom_F)
    call ltdom_free(dom_N)

    !Free tmp matrices
    call mat_free(dens0M)
    call mat_free(matK_0L)
    call mat_free(matK_tmp)

    !Free molecules
    call free_moleculeinfo(molL)
    call free_moleculeinfo(molN)
    call free_moleculeinfo(molNM)


  end subroutine wcalc_fock_k


  !> @brief Dummy implementation of the Coulomb nearfield contribution to the Fock matrix
  !> @date April 2016
  !> @author Elisa Rebolini
  subroutine wcalc_fock_j_nearfield(lsset, maxgab, wconf, &
       & refcell, dens, f_mat, zmat, lupri, luerr)
    
    integer, intent(in)                 :: lupri, luerr
    type(lssetting), intent(inout)      :: lsset
    type(lmatrixp), intent(in)          :: dens
    type(wannier_config), intent(inout) :: wconf
    type(linteger), intent(in)          :: maxgab
    type(lmatrixp), intent(inout)       :: f_mat, zmat
    type(moleculeinfo), intent(in)      :: refcell

    type(bravais_lattice)      :: blat
    type(lattice_domains_type) :: dom_D, dom_F, dom_N
    integer                    :: nbast, l, m, n, indxL, indxM, indxN, nf_cutoff(3), iounit, i, j
    type(matrix)               :: dens0M, matJ_0L, matJ_tmp, matZ_0L, h1
    type(matrix),pointer       :: matF_0L,matJ_0L_ptr
    logical                    :: lstat_0M,lstat_0L
    type(moleculeinfo)         :: molN, molL, molNM, molM
    real(realk)                :: transN(3), transL(3), transNM(3)
    character(len=50)          :: filename
    type(lmatrixp)             :: Jmat_debug
    character                  :: trans   
    
    nbast = dens%nrow
    blat = wconf%blat
    nf_cutoff = wconf%nf_cutoff
    
    !Domain initialisation
    call ltdom_init(dom_D, blat%dims, maxval(dens%cutoffs))
    call ltdom_init(dom_F, blat%dims, maxval(f_mat%cutoffs))
    call ltdom_init(dom_N, blat%dims, maxval(nf_cutoff))

    call ltdom_getdomain(dom_D, dens%cutoffs, incl_redundant=.true.)
    call ltdom_getdomain(dom_N, nf_cutoff, incl_redundant=.true.)
    call ltdom_getdomain(dom_F, f_mat%cutoffs, incl_redundant=wconf%no_redundant)
           
    !Matrix allocation
    call mat_init(dens0M,nbast,nbast)
    call mat_init(matJ_0L,nbast,nbast)
    call mat_init(matJ_tmp,nbast,nbast)
    
    !Copies of the reference cell for the integral call
    call copy_molecule(refcell,molN,lupri)
    call copy_molecule(refcell,molL,lupri)
    call copy_molecule(refcell,molNM,lupri)
    
    loopM: do m = 1, dom_D%nelms; indxM = dom_D%indcs(m)
       
       call mat_zero(dens0M)
       !Warning: J_mn = sum_kl (mn|kl) D_lk and here D_lk and D_kl are not equivalent
       !as we work in a subblock of the density matrix
       call lts_copy(dens,-indxM,lstat_0M,dens0M)
       if (lstat_0M) then
          
          !J_{0 mu, L nu} for D_0M
          loopL: do l = 1, dom_F%nelms; indxL = dom_F%indcs(l)
             call mat_zero(matJ_0L)
             
             call wcopy_atompositions(molL, refcell)
             transL = wu_indxtolatvec(wconf%blat,(indxL))
             call wtranslate_molecule(molL,transL)
             
             loopN: do n = 1, dom_N%nelms; indxN = dom_N%indcs(n)
                call mat_zero(matJ_tmp)
                call wcopy_atompositions(molN, refcell)
                transN = wu_indxtolatvec(blat, indxN)
                call wtranslate_molecule(molN, transN)
                call wcopy_atompositions(molNM, refcell)
                transNM = wu_indxtolatvec(blat, indxN+indxM)
                call wtranslate_molecule(molNM, transNM)
                
                call typedef_setmolecules( &
                     & lsset, refcell, 1, molL, 2, molN, 3, molNM, 4)

                call II_get_coulomb_mat(lupri, luerr, lsset, dens0M, matJ_tmp, 1)

!!$                if (indxL == 0) then
!!$                   write(luerr,*) 'J_00: M', indxM,'N', indxNh 
!!$                   call mat_print(matJ_tmp,1,nbast,1,nbast,luerr)
!!$                endif
                   
                ! and add them to the J matrix
                call mat_daxpy(1.0_realk, matJ_tmp, matJ_0L)
             enddo loopN

             ! copy matJ_0L for D_0M (J_{0,indx1}) to the fock matrix (F_{0,indx1})
             matF_0L => lts_get(f_mat, indxL)
             if (.not. associated(matF_0L)) then
                call lts_mat_init(f_mat, indxL)
                matF_0L => lts_get(f_mat, indxL)
                call mat_zero(matF_0L)
             endif
             call mat_daxpy(1.0_realk, matJ_0L, matF_0L)
          enddo loopL
         
       endif
    enddo loopM

    if (wconf%debug_level > 15) then
       write (luerr,*) ''
       write (luerr,*) '***Sum of Z and J'

       iounit=100
       open(UNIT=iounit,FILE="debug.wannier",FORM="FORMATTED",STATUS="UNKNOWN", &
            & ACTION="WRITE",POSITION='REWIND')
       
       call mat_init(matZ_0L,nbast,nbast)
       
       loopL2: do l = 1, dom_F%nelms; indxL = dom_F%indcs(l)
          call mat_zero(matZ_0L)
          call mat_zero(matJ_0L)
          call lts_copy(Zmat,indxL,lstat_0L,matZ_0L)
          call lts_copy(f_mat,indxL,lstat_0L,matJ_0L)
          call mat_daxpy(1.0_realk, matJ_0L, matZ_0L)
          write (iounit,*) 'Indx L',indxL
          do i=1,nbast
             write (iounit,*) (matZ_0L%elms((j-1)*nbast +i), j=1,nbast)
          enddo
          !call mat_print(matZ_0L,1,nbast,1,nbast,luerr)
       enddo loopL2
       call lsquit("debug",-1)
       call mat_free(matZ_0L)
    endif
    
    !Free domains
    call ltdom_free(dom_D)
    call ltdom_free(dom_F)
    call ltdom_free(dom_N)

    !Free tmp matrices
    call mat_free(dens0M)
    call mat_free(matJ_0L)
    call mat_free(matJ_tmp)
    
    !Free molecules
    call free_moleculeinfo(molL)
    call free_moleculeinfo(molN)
    call free_moleculeinfo(molNM)
        
  end subroutine wcalc_fock_j_nearfield

  
  subroutine mat_to_2dim(mat,a2dim,nbast)
      type(matrix),intent(in) :: mat
      integer,intent(in)      :: nbast
      real(realk)             :: a2dim(nbast,nbast)

      integer :: i,j,ij

      a2dim = 0E0_realk
      
      do i = 1, nbast
         do j = 1, nbast
            ij = (j-1)*nbast + i
            a2dim(i,j) = mat%elms(ij)
         enddo
      enddo

      !call mat_print(mat,1,nbast,1,nbast,6)
      !do i=1,nbast
      !   write (*,*) a2dim(i,:)
      !enddo
    end subroutine mat_to_2dim
  
  !> @author Karl Leikanger
  !> @date 2015
  !> @brief Calculate e-e repulsion. Add the matrix to f_mat
  subroutine wcalc_fock_j(lsset, maxgab, blat, &
       & refcell, dens, nf_cutoff, f_mat, lupri, luerr, e_j)

    integer, intent(in) :: lupri, luerr, nf_cutoff(3)
    type(lssetting), intent(inout) :: lsset
    type(lmatrixp), intent(in) :: dens
    type(bravais_lattice), intent(in) :: blat
    type(linteger), intent(in) :: maxgab
    type(lmatrixp), intent(inout) :: f_mat
    type(moleculeinfo), intent(in) :: refcell
    real(realk), intent(inout), optional :: e_j

    integer :: i, j, k, indx1, indx2, indx3, indx4, dom1_cutoff, &
         & natoms, nbast 	!cutoffs_new(3)	
    integer, pointer :: gab1, gab2
    real(realk) :: rtmp  !, max_elm_layer
    type(lattice_domains_type) :: dom1, dom2, dom3
    type(matrix) :: mattmp, j_mat_tmp
    type(matrix), pointer :: f_mat_ptr, dens_ptr
    type(matrix), target :: dens_tmp
    type(moleculeinfo) :: mol1, mol3, mol4

    real(realk), parameter :: realthr = 10e-16_realk ! todo should be input
    integer :: intthr
    intthr = int(log10(realthr))

    if (present(e_j)) then
       e_j = 0.0_realk
    end if

    nbast = f_mat%nrow
    natoms = refcell%natoms

    call mat_init(dens_tmp, nbast, nbast)
    call mat_init(mattmp, nbast, nbast)
    call mat_init(j_mat_tmp, nbast, nbast)
    dom1_cutoff = maxval(f_mat%cutoffs)
    call ltdom_init(dom1, blat%dims, dom1_cutoff)
    call ltdom_init(dom2, blat%dims, maxval(dens%cutoffs))
    call ltdom_init(dom3, blat%dims, maxval(nf_cutoff))

    call ltdom_getdomain(dom3, nf_cutoff, incl_redundant=.true.)

    call copy_molecule(refcell, mol1, lupri)
    call copy_molecule(refcell, mol3, lupri)
    call copy_molecule(refcell, mol4, lupri)

    
    ! cycle over all cells 
    call ltdom_getdomain(dom1, f_mat%cutoffs, incl_redundant=.false.)
    do i = 1, dom1%nelms; indx1 = dom1%indcs(i)

       gab1 => lts_get(maxgab, indx1)
       if (.not. associated(gab1)) cycle
       if (gab1 < -18) cycle  ! TODO set thrsh

       call wcopy_atompositions(mol1, refcell)
       call wtranslate_molecule(mol1, wu_indxtolatvec(blat, indx1))  ! -??
       call mat_zero(j_mat_tmp)

       ! cycle over density matrix
       ! TODO think about this !! incl_redundant should be true here ??
       ! TODO but the results corresponds exatcly to Joh's when set to false
       call ltdom_getdomain(dom2, dens%cutoffs, incl_redundant=.true.)  !todo true ?
       do j = 1, dom2%nelms; indx2 = dom2%indcs(j)
          ! do while(ltdom_getnext(dom2, indx2))

          gab2 => lts_get(maxgab, abs(indx2))
          if (.not. associated(gab2)) cycle
          if (gab2 < -18) cycle  ! TODO set thrsh
          dens_ptr => lts_get(dens, abs(indx2))

          ! TODO remove ? is per def ass ?
          if (.not. associated(dens_ptr)) cycle
          if (indx2<0) then
             call mat_trans(dens_ptr, dens_tmp)
             dens_ptr => dens_tmp
          endif

          ! todo Strange rule !!
          call mat_abs_max_elm(dens_ptr, rtmp) 
          if (rtmp > 0.0_realk) then
             if ((int(log10(rtmp)) + gab1 + gab2) < intthr) cycle  ! TODO smart cutoff? set thr
          else
             cycle
             ! todo ?? if (rtmp + gab1 + gab2 < intthr) cycle
             ! strange rule same as it the calculation of J. Ask Joh about this one!!
          endif

          ! cycle over nearfield
          do k = 1, dom3%nelms; indx3 = dom3%indcs(k)

             indx4 = indx2 + indx3

             !think about some cutoff here on inx 4

             call wcopy_atompositions(mol3, refcell)
             call wtranslate_molecule(mol3, wu_indxtolatvec(blat, indx3))

             call wcopy_atompositions(mol4, refcell)
             call wtranslate_molecule(mol4, wu_indxtolatvec(blat, indx4))

             call typedef_setmolecules( &
                  & lsset, refcell, 1, mol1, 2, mol3, 3, mol4, 4)

             call mat_zero(mattmp)
             call II_get_coulomb_mat( &
                  & lupri, luerr, lsset, dens_ptr, mattmp, 1)
             call mat_daxpy(0.5_realk, mattmp, j_mat_tmp)
          enddo
       enddo

       ! same as calling e_j = 0.5 * lts_dotprod(dens, j_mat) outside the loop
       if (present(e_j)  .and. ltdom_contains(dom2, indx1)) then
          dens_ptr => lts_get(dens, indx1)
          if (associated(dens_ptr)) then
             if(indx1 /= 0) then
                e_j = e_j + &
                     & 1.0_realk*mat_dotproduct(j_mat_tmp, lts_get(dens, indx1)) 
             else
                e_j = e_j+ &
                     & 0.5_realk*mat_dotproduct(j_mat_tmp, lts_get(dens, indx1))
             endif
          endif
       endif

       ! copy j_mat_tmp to fockmat
       f_mat_ptr => lts_get(f_mat, indx1)
       if (.not. associated(f_mat_ptr)) then
          call lts_mat_init(f_mat, indx1)
          f_mat_ptr => lts_get(f_mat, indx1)
          call mat_zero(f_mat_ptr)
       endif
       call mat_daxpy(1.0_realk, j_mat_tmp, f_mat_ptr)  ! try copy

    enddo
    
    call ltdom_free(dom1)
    call ltdom_free(dom2)
    call ltdom_free(dom3)
    call mat_free(dens_tmp)
    call mat_free(mattmp)
    call mat_free(j_mat_tmp)
    call free_moleculeinfo(mol1)
    call free_moleculeinfo(mol3)
    call free_moleculeinfo(mol4)
    !free molecules, dom, mat, ...
    
  end subroutine wcalc_fock_j


  !> @brief Add the farfield contribution (e-e + e-n) to the fock matrix.
  !> @param
  !> @param
  !> @param
  !> @param
  !> @param
  !> @author Karl R. Leikanger
  !> @date 2015
  subroutine wcalc_fock_farfield(blat, dens, fmmt, lmax, f_mat, lupri, &
       & e_ff, e_nn)

    integer,intent(in) :: lupri, lmax
    type(fmm_tensors), intent(inout) :: fmmt
    type(lmatrixp), intent(in) :: dens
    type(bravais_lattice), intent(in) :: blat
    type(lmatrixp), intent(inout) :: f_mat
    real(realk), intent(inout), optional :: e_ff, e_nn

    type(matrix) :: farfieldtmp
    real(realk),pointer :: rhojk(:), tlatlm(:),tlatlmnu(:)
    type(lattice_domains_type) :: dom
    type(matrix), pointer :: sphmat_pt(:), dens_pt, fmat_pt
    integer :: gab, nbast, i, j, indx, num_lm

    nbast = f_mat%nrow
    num_lm = (lmax + 1)**2

    call mem_alloc(tlatlm, num_lm)
    call mem_alloc(tlatlmnu, num_lm)
    call mem_alloc(rhojk, num_lm)

    rhojk(:) = 0.0_realk
    call lts_load(fmmt%sphmom)
    call ltdom_init(dom, blat%dims, maxval(fmmt%sphmom%cutoffs))  ! todo inclred?
    call ltdom_getdomain(dom, fmmt%sphmom%cutoffs)

    do i = 1, dom%nelms; indx = dom%indcs(i)

       ! todo necc ? only significalt sphmom initiated!
       !		gab => lts_get(maxgab, indx)  ! fixme 
       !		if (.not. associated(gab)) cycle  ! fixme
       !		if (gab < realthr) cycle  ! fixme

       sphmat_pt => lts_get(fmmt%sphmom, indx)
       if (.not. associated(sphmat_pt)) cycle

       ! todo todo -->
       ! does this need to be done each scf loop?
       ! Why not simply do this before storing the sphermom matrices?
       ! seems to be doing the same regardless of the current density.

       !     write (*, *) wu_indxtocoor(indx) 
       !do j = 1, lmax !(lmax + 1)**2
          ! call mat_print(sphmat_pt(j), 1, sphmat_pt(j)%nrow, 1, sphmat_pt(j)%ncol, 6)
          !	write (*, *) sphmat_pt(j)%elms
       !enddo

       call pbc_multipl_moment_matorder(sphmat_pt, lmax, nbast)
       call pbc_mat_redefine_q(sphmat_pt, lmax, nbast)
       ! <-- todo todo
    enddo
    !call lsquit('fftest', -1)

    do i = 1, dom%nelms; indx = dom%indcs(i)
       sphmat_pt => lts_get(fmmt%sphmom, indx)
       dens_pt => lts_get(dens, indx)
       if (associated(dens_pt) .and. associated(sphmat_pt)) then 
          do j = 1, num_lm
             !todo 
             ! if sphmat_pt(j) is initiated 
             if (.not. associated(sphmat_pt)) cycle
             rhojk(j) = rhojk(j) - &
                  & mat_dotproduct(dens_pt, sphmat_pt(j))
          enddo
       endif
    enddo
    call ltdom_free(dom)

    tlatlm = 0.0_realk  ! todo necc?
    tlatlmnu = 0.0_realk  ! todo necc? 
    do j = 1, num_lm
       !todo dot product inefficient for large matrices
       tlatlm(j) = dot_product(fmmt%tlat(j, 1:num_lm), fmmt%nucmom + rhojk)
       !todo dot product inefficient for large matrices
       !todo necc to do each scf iteration ( not density dependent).
       tlatlmnu(j) = dot_product(fmmt%tlat(j, 1:num_lm), fmmt%nucmom)
    enddo

    call mat_init(farfieldtmp, nbast, nbast)
    call mat_zero(farfieldtmp)  ! todo necc?
    call ltdom_init(dom, blat%dims, maxval(fmmt%sphmom%cutoffs))
    call ltdom_getdomain(dom, fmmt%sphmom%cutoffs, incl_redundant=.false.)
    !call ltdom_getdomain(dom, f_mat%cutoffs, incl_redundant=.false.)
    do i = 1, dom%nelms; indx = dom%indcs(i)

       !		gab => lat_get(maxgab, indx)  ! todo necc ? only significalt sphmom initiated
       !		if (.not. associated(gab)) cycle
       !		if ( .not. (gab >= realthr)) cycle

       sphmat_pt => lts_get(fmmt%sphmom, indx)
       if (.not. associated(sphmat_pt)) cycle
       fmat_pt => lts_get(f_mat, indx)
       if (.not. associated(fmat_pt)) cycle

       call mat_zero(farfieldtmp)
       do j = 1, num_lm
          call mat_daxpy(-tlatlm(j), sphmat_pt(j), farfieldtmp)
       enddo
       call mat_daxpy(1.0_realk, farfieldtmp, fmat_pt)
       !     write (*, *) '----- cell -------', indx 
       !     write (*, *) 'coor', wu_indxtocoor(indx)
       !     call mat_print(fmat_pt, 1, fmat_pt%nrow, 1, fmat_pt%ncol, 6)
    enddo
    call ltdom_free(dom)

    if (present(e_ff)) &
         & e_ff = dot_product(rhojk + fmmt%nucmom, tlatlm) / 2.0_realk
    if (present(e_nn)) &
         & e_nn = dot_product(fmmt%nucmom, tlatlmnu)

    call lts_store(fmmt%sphmom)
    
    call mem_dealloc(tlatlm)
    call mem_dealloc(tlatlmnu)
    call mem_dealloc(rhojk)
    call mat_free(farfieldtmp)

  end subroutine wcalc_fock_farfield

  !> @author Karl Leikanger
  !> @date 2015
  !> @brief Calculate exchange. Add the K matrix to f_mat.
  !> this routine is only calculating the non redundant elements K_{refcell, m}, m>0 
  subroutine wcalc_fock_HFX( lsset, maxgab, blat, &
       & refcell, dens, nf_cutoff, f_mat, lupri, luerr, e_k)

    integer, intent(in) :: lupri, luerr, nf_cutoff(3)
    type(lssetting), intent(inout) :: lsset
    type(lmatrixp), intent(in) :: dens
    type(bravais_lattice), intent(in) :: blat
    type(linteger), intent(in) :: maxgab
    type(lmatrixp), intent(inout) :: f_mat
    type(moleculeinfo), intent(in) :: refcell
    real(realk), intent(inout), optional :: e_k

    integer :: i, j, k, indx1, indx2, indx3, indx4, dom1_cutoff, dom2_cutoff, &
         & layer, layer2, natoms, nbast
    integer, pointer :: gab2, gab3
    real(realk) :: rtmp  !, max_elm_layer
    type(lattice_domains_type) :: dom1, dom2, dom3
    type(matrix), pointer :: f_mat_ptr, dens_ptr
    type(matrix) :: mattmp, j_mat_tmp, dens_tmp
    type(moleculeinfo) :: mol1, mol2, mol3, mol4
    logical :: nonzero_in_layer2

    real(realk) :: realthr
    realthr = 10e-16_realk

    if (present(e_k)) then
       e_k = 0.0_realk
    end if

    nbast = f_mat%nrow
    natoms = refcell%natoms

    call mat_init(mattmp, nbast, nbast)
    call mat_init(dens_tmp, nbast, nbast)
    call mat_init(j_mat_tmp, nbast, nbast)
    dom1_cutoff = maxval(f_mat%cutoffs)
    call ltdom_init(dom1, blat%dims, dom1_cutoff)
    dom2_cutoff = maxval(f_mat%cutoffs)
    call ltdom_init(dom2, blat%dims, dom2_cutoff)
    call ltdom_init(dom3, blat%dims, maxval(dens%cutoffs))
    call ltdom_getdomain(dom3, dens%cutoffs, incl_redundant=.true.)  ! todo incl_redundant=.true. ??

    call copy_molecule(refcell, mol1, lupri)
    call copy_molecule(refcell, mol2, lupri)
    call copy_molecule(refcell, mol3, lupri)
    call copy_molecule(refcell, mol4, lupri)
    
    call ltdom_getdomain(dom1, f_mat%cutoffs, incl_redundant=.false.)
    do i = 1, dom1%nelms; indx1 = dom1%indcs(i)
       ! calculating the matrix K_{0, index1} between the refcell and inxd1
       ! only summing over non-redundant elements indx1

       call wcopy_atompositions(mol1, refcell)
       call wtranslate_molecule(mol1, wu_indxtolatvec(blat, indx1))
       call mat_zero(j_mat_tmp)

       ! cycle over all cells
       ! loop from center cell and outwards until all contributions in a 
       ! layer are 0. TODO This might not be the optimal way to do it!
       layer2 = -1
       nonzero_in_layer2 = .true.
       layers2: do; layer2 = layer2 + 1 
          if (.not. nonzero_in_layer2) exit layers2
          nonzero_in_layer2 = .false.

          ! todo fixme!! shuld be true, but does only give the identical results to Johs
          call ltdom_getlayer(dom2, layer2, incl_redundant=.true.)  
          do j = 1, dom2%nelms; indx2 = dom2%indcs(j)

             gab2 => lts_get(maxgab, indx2) !abs ?? 
             if (.not. associated(gab2)) cycle
             if (gab2 < -18) cycle  ! TODO set thrsh

             call wcopy_atompositions(mol2, refcell)
             call wtranslate_molecule(mol2, wu_indxtolatvec(blat, indx2))

             ! cycle over the densitymatrix
             do k = 1, dom3%nelms; indx3 = dom3%indcs(k)

                indx4 = indx2 + indx3

                dens_ptr => lts_get(dens, abs(indx3))
                call mat_abs_max_elm(dens_ptr, rtmp)

                gab3 => lts_get(maxgab, indx4 - indx1)
                if (.not. associated(gab3)) cycle
                if (gab3 < -18) cycle  

                ! todo look at this criteria. Where does it come from? ask J.
                if (rtmp > 0.0_realk) then
                   if ((int(log10(rtmp)) + gab2 + gab3) < int(log10(realthr))) cycle
                else
                   !cycle	
                   ! todo ?? strange rule but this is what gives me the same results as Joh.
                   if ((rtmp + gab2 + gab3) < int(log10(realthr))) cycle
                endif
                !todo  int(log10(realthr)) -> intthr

                ! todo here it is possible to invent a more efficient cutoff
                ! E.g. Exit when the largest element in mattmp is smaller than some
                ! value in the layer (layers2). Johannes has chosen a fixed cutoff in the layers
                ! (only the nearfield)
                nonzero_in_layer2 = .true.

                !
                ! calculate the actual matrixelements
                !
                !  todo remove mol 2
                !	call wcopy_atompositions(mol3, refcell)
                !	call wtranslate_molecule(mol3, wu_indxtolatvec(blat, indx3))
                call wcopy_atompositions(mol4, refcell)
                call wtranslate_molecule(mol4, wu_indxtolatvec(blat, indx4))

                call typedef_setmolecules( &
                     !& lsset, refcell, 1, mol2, 2, mol1, 3, mol4, 4)
                     & lsset, refcell, 1, mol1, 3, mol2, 2, mol4, 4) 

                call mat_zero(mattmp)
                if (indx3 < 0) then
                   call mat_trans(dens_ptr, dens_tmp)  ! todo probably not optimal for speed
                   call II_get_exchange_mat( &
                        & lupri, luerr, lsset, dens_tmp, 1, .false., mattmp)
                else
                   call II_get_exchange_mat( &
                        & lupri, luerr, lsset, dens_ptr, 1, .false., mattmp)
                endif
                ! and add them to the K matrix
                call mat_daxpy(0.5_realk, mattmp, j_mat_tmp)
             enddo
          enddo

          ! dynamic expansion of domaintype if necc
          if (layer2 == dom2_cutoff) then
             call ltdom_free(dom2)
             dom2_cutoff = dom2_cutoff + 5
             call ltdom_init(dom2, blat%dims, dom2_cutoff)
          endif
       enddo layers2

       ! same as calling e_k = 0.5 * lts_dotprod(dens, k_mat) outside the loop
       if (present(e_k) .and. ltdom_contains(dom2, indx1)) then
          e_k = e_k + &
               & 0.5_realk*mat_dotproduct(j_mat_tmp, lts_get(dens, indx1))
       endif

       ! copy k_mat_tmp (K_{0,indx1}) to the fock matrix (F_{0,indx1})
       f_mat_ptr => lts_get(f_mat, indx1)
       if (.not. associated(f_mat_ptr)) then
          call lts_mat_init(f_mat, indx1)
          f_mat_ptr => lts_get(f_mat, indx1)
          call mat_zero(f_mat_ptr)
       endif
       call mat_daxpy(1.0_realk, j_mat_tmp, f_mat_ptr)

    enddo
    
    call ltdom_free(dom1)
    call ltdom_free(dom2)
    call ltdom_free(dom3)
    call mat_free(mattmp)
    call mat_free(dens_tmp)
    call mat_free(j_mat_tmp)
    call free_moleculeinfo(mol1)
    call free_moleculeinfo(mol2)
    call free_moleculeinfo(mol3)
    call free_moleculeinfo(mol4)
  end subroutine wcalc_fock_HFX

  subroutine symmetrize_fock(fockmat,wconf,lupri,luerr)

    type(lmatrixp),intent(inout)     :: fockmat
    type(wannier_config), intent(in) :: wconf
    integer, intent(in)              :: lupri,luerr

    type(lattice_domains_type) :: dom
    type(matrix),pointer       :: tmp1, tmp2
    type(matrix)               :: tmpt, tmpl
    integer                    :: l, indxl, nbas

    nbas = fockmat%nrow
    
    call ltdom_init(dom, wconf%blat%dims, maxval(fockmat%cutoffs))
    call ltdom_getdomain(dom, fockmat%cutoffs, incl_redundant=.false.)

    call mat_init(tmpt,nbas,nbas)
    call mat_init(tmpl,nbas,nbas)
       
    loopl: do l = 1, dom%nelms
       indxl = dom%indcs(l)
       if (indxl .le. 0) cycle
       tmp1 => lts_get(fockmat,indxl)
       !call mat_print(tmp1,1,nbas,1,nbas,6)
       tmp2 => lts_get(fockmat,-indxl)
       !call mat_print(tmp2,1,nbas,1,nbas,6)
       call mat_trans(tmp2,tmpt)
       !call mat_print(tmpt,1,nbas,1,nbas,6)
       call mat_add(0.5_realk,tmp1,0.5_realk,tmpt,tmpl)
       
       tmp1%elms(:) = tmpl%elms(:)
       call mat_trans(tmp1,tmp2)
    enddo loopl

    call mat_free(tmpt)
    call mat_free(tmpl)
    
    call ltdom_free(dom)
    
  end subroutine symmetrize_fock

  
end module wannier_fockAO
