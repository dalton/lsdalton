add_library(wannier
  OBJECT
    ${CMAKE_CURRENT_LIST_DIR}/timings_module.F90
    ${CMAKE_CURRENT_LIST_DIR}/WannierMain.F90
    ${CMAKE_CURRENT_LIST_DIR}/WannierFMM.F90
    ${CMAKE_CURRENT_LIST_DIR}/WannierAOint.F90
    ${CMAKE_CURRENT_LIST_DIR}/WlatticeDomains.F90
    ${CMAKE_CURRENT_LIST_DIR}/lattice_storage.F90
    ${CMAKE_CURRENT_LIST_DIR}/wannierTestModule.F90
    ${CMAKE_CURRENT_LIST_DIR}/WannierOrthpot.F90
    ${CMAKE_CURRENT_LIST_DIR}/WannierFock.F90
    ${CMAKE_CURRENT_LIST_DIR}/WannierMOFock.F90
    ${CMAKE_CURRENT_LIST_DIR}/WannierComplexUtils.F90
    ${CMAKE_CURRENT_LIST_DIR}/WannierStartGuess.F90
    ${CMAKE_CURRENT_LIST_DIR}/WannierBandstructure.F90
    ${CMAKE_CURRENT_LIST_DIR}/WannierLocalisation.F90
    ${CMAKE_CURRENT_LIST_DIR}/wannierHFenergy.F90
    ${CMAKE_CURRENT_LIST_DIR}/WannierSCF.F90
    ${CMAKE_CURRENT_LIST_DIR}/wannierDensity.F90
    ${CMAKE_CURRENT_LIST_DIR}/WannierShukla.F90
    ${CMAKE_CURRENT_LIST_DIR}/WannierMulliken.F90
    ${CMAKE_CURRENT_LIST_DIR}/WannierAsymptoticExpansion.F90
  )

target_compile_definitions(wannier
  PRIVATE
    $<$<BOOL:${ENABLE_DEBUGPBC}>:DEBUGPBC>
  )

target_compile_options(wannier
  PRIVATE
    "${LSDALTON_Fortran_FLAGS};${LSDALTON_Fortran_FLAGS_${CMAKE_BUILD_TYPE}}"
  )

target_link_libraries(wannier
  PUBLIC
    lsutil
    LSint
    linears
  )

target_link_libraries(lsdalton
  PUBLIC
    wannier
  )
