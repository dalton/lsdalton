READ BEFORE IMPLEMENTING NEW DEC CODE.

1. How to implement a new model or correction to existing model.

2. How to handle dependencies with deccc/ folder.



********************************************************************************
*                           1. NEW DEC MODEL/CORRECTION                        *
********************************************************************************

If you want to implement energies for a new model/correction in DEC, then please do the following:

(i)
Define whether you want to introduce a new MODEL (e.g. CC3)    OR
a new CORRECTION (e.g. F12) which can be added to various existing models/corrections.

(ii)
The global integers MODEL_* in dec_typedef.F90 define the various models.
If you include a new model, then add it here. 
If you include a new correction (e.g. F12), then there is nothing to add here.

(iii) 
Since we have different partitioning schemes for each CC model, 
it is necessary to have another set of global
integers which define the fragment model (e.g. occupied partitioning scheme for CCSD).
These are defined as the global integers FRAGMODEL_* in dec_typedef.F90.
If you include a new model OR correction, then add it here.
At the same time, you need to increase the global integer ndecenergies in lsutil/dec_typedef.F90 accordingly.
E.g. if your new model/correction requires N additional energies to be stored, 
increase ndecenergies by N.

(iv)
New model:
* Define your model in dec_set_model_names and find_model_number_from_input.
* Add model to input keyword(s) in config_dec_input. 
(For example, see what is done for CCSD and do something similar).

New correction: 
* Insert logical describing whether correction should be done 
  in type DECsettings in lsutil/dec_typedef.f90.  
* Set defaults in dec_set_default_config and add input keyword(s) in config_dec_input.
(For example, see what is done for F12 and do something similar).

(v)
For new model (e.g. RPA): Grep for "MODIFY FOR NEW MODEL" in all files in the DEC code. These are the main places you need to change to include your new model. It is hopefully clear from the context and the comments in the code what needs to be done to include the new model.

For new correction (e.g. F12): Grep for "MODIFY FOR NEW CORRECTION" in all files in the DEC code. These are the main places you need to change to include your new model. It is hopefully clear from the context and the comments in the code what needs to be to include the new correction".


(vi) 
Workarounds:
if you have to introduce workarounds on specific systems introduce them in dec_workarounds.F90 with a specific precompiler flag for your issue at hand. Rationale: Usually workarounds are compiler and system dependent and a clean version of the code should be maintained by default 

Note: I have almost certainly forgotten something in the description above, so please feel free
to update and improve the description!





********************************************************************************
*                               2. DEC DEPENDENCIES                            *
********************************************************************************


This is an overview of DEC dependencies.
The list below should ALWAYS be kept up to date!

Rules:

1. Files at level X is allowed to use a subroutine/function in a file on level Y if and only if X>Y!
2. You are of course allowed to introduce new files, shuffle things around etc.
   If you do so, make sure that each files has a place in the hierarchy and that this readme-file is updated!
3. If modifying dependencies, make sure that all DEC depencies are put under the 
   "! DEC DEPENDENCIES (within deccc directory)" comment within each file.
4. Use the "public" and "private" keywords as much as possible for the high-level files (see e.g. dec_main.f90). This significantly speeds up compilation and reduces memory consumption during compilation.


Anyone not obeying these simple rules will get executed by the Jonas Kramer warrior at dawn...


Dependency list:
****************

Level 12
dec_main

Level 11
snoop_main

Level 10
full_driver, dec_driver, decmpiSlave, full_rimp2f12

Level 9
dec_driver_slave

Level 8
fragment_energy

Level 7
cc_driver, cc_debug_routines

Level 6
rpa, snoop_tools, cc_response_solver

Level 5
pno_ccsd, ccsd, dec_atom, ccsdpt, mp2_gradient, f12_integrals, 
rif12_integrals, cc_response_tools

Level 4
full_mp3, ricc2

Level 3
full_mp2, ccsd_left_trans, ricc2_noddy,lofex_tools, f12ri_energycontraction

Level 2 
fullmolecule, mp2, cc_integrals, crop_tools, rimp2, ri_util

Level 1
ccorbital, ccarray2_simple, ccarray3_simple, ccarray4_simple,  decmpi, dec_utils

Level 0 
dec_settings, full_driver_f12contractions, CABS, cc_tools, f12ri_util, dec_print

array3_memory, array4_memory, f12_routines, dec_workarounds, dec_tools, dec_ccsdf12_routines


------------------------
Kasper Kristensen, 2013
