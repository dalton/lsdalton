add_library(ddynam
  OBJECT
   ${CMAKE_CURRENT_LIST_DIR}/LSinput.F90
   ${CMAKE_CURRENT_LIST_DIR}/Fock_mat_dyn.F90
   ${CMAKE_CURRENT_LIST_DIR}/Modules.F90
   ${CMAKE_CURRENT_LIST_DIR}/Temperature.F90
   ${CMAKE_CURRENT_LIST_DIR}/TimeRev_prop.F90
   ${CMAKE_CURRENT_LIST_DIR}/dyn_utilities.F90
  )

target_compile_options(ddynam
  PRIVATE
    "${LSDALTON_Fortran_FLAGS};${LSDALTON_Fortran_FLAGS_${CMAKE_BUILD_TYPE}}"
  )

target_link_libraries(ddynam
  PUBLIC
    lsutil
  )

target_link_libraries(lsdalton
  PUBLIC
    ddynam
  )
