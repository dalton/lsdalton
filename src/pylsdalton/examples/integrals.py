import os
import sys
import numpy as np
from cffi import FFI
sys.path.append(os.getenv('LSDALTON_PY_MODULE_PATH'))
import pylsdalton # isort: skip

dal = '''**GENERAL
.TIME
.PSFUN
**WAVE FUNCTIONS
.DFT
B3LYP
*END OF INPUT
'''

mol = '''BASIS
STO-3G Aux=3-21G


Atomtypes=2    Charge=0   Nosymmetry Angstrom
Charge=3.00   Atoms=1
Li    0.0000000    0.0000000   -0.0613836 
Charge=1.00   Atoms=1
H     0.7756707    0.0000000    0.4871008 
'''

with open("LSDALTON.INP", "w") as dal_file:
    dal_file.write(dal)

with open("MOLECULE.INP", "w") as mol_file:
    mol_file.write(mol)

pylsdalton.create()

natoms = pylsdalton.number_atoms()
nbasis = pylsdalton.number_basis()
naux = pylsdalton.number_aux()
nelectron = pylsdalton.number_electrons()
nocc = nelectron/int(2)

print("---dimensions: Atoms = ", natoms, " nBasis = ", nbasis, " nAux = ", naux, " nElectrons =", nelectron)
print("---number of occupied orbitals", nocc)

#LSDalton calls to 1-electron matrices
overlap = pylsdalton.overlap_matrix()
kinetic = pylsdalton.kinetic_energy_matrix()
nucel = pylsdalton.nuclear_electron_attraction_matrix()
oneel = np.add(kinetic, nucel)

#H1 start guess
orbital_energies, CMO = pylsdalton.diagonalize(oneel, overlap)
occ_mos = CMO[0:int(nocc),:]

density = np.einsum("ki,kj->ij", occ_mos, occ_mos)

density2 = np.zeros(shape=(2,nbasis,nbasis), dtype=np.float64)
density2[0,:,:] = density
density2[1,:,:] = density

print("---core-nucleous oribtal energies", orbital_energies)
print("---number of occupied electrons", nocc)
print("---homo energy", orbital_energies[int(nocc)-1])
print("---lumo energy", orbital_energies[int(nocc)])
print("---MOs")
print(CMO)
print("---occ MOs")
print(occ_mos)

print("---H1DIAG density")
print(density)
trace = 2.0*np.trace(np.matmul(density, np.transpose(overlap)))
print("--trace(DS) electrons", trace)


#LSDalton calls to 2-electron matrices
coulomb = pylsdalton.coulomb_matrix(density)
exchange = pylsdalton.exchange_matrix(density2)
exchange_correlation, XC_energy = pylsdalton.exchange_correlation_matrix(density, testElectrons = False)

fock = oneel + np.multiply(2.0, coulomb) + exchange + exchange_correlation

print("---XC energy")
print(XC_energy)
print("---overlap")
print(overlap)
print("---kinetic")
print(kinetic)
print("---nucel")
print(nucel)
print("---oneel")
print(oneel)
print("---coulomb")
print(coulomb)
print("---exchange")
print(exchange)
print("---exchange-correlation")
print(exchange_correlation)
print("---fock")
print(fock)
#

#Full fock matrix in one LSDalton call
ffock = pylsdalton.fock_matrix(density, testElectrons = False)
print("---ffock")
print(ffock)

print("---diff")
print(ffock - fock)

# 4-center electron-repulsion integrals (ERIs)
eri = pylsdalton.eri('RRRRC')

# Form Coulomb matrix from ERIs 
Jeri = np.einsum('abcde,cd', eri, density)
Jeri.shape = (nbasis,nbasis)

print("---Jeri")
print(Jeri)

print("---Jeri - J")
print(Jeri - coulomb)

#--- df-J construction

# - Integrals
#       (ab|alpha)
ab_alpha = pylsdalton.eri('DERRC')
ab_alpha.shape = (nbasis, nbasis, naux)

#       (alpha|beta)
alpha_beta = pylsdalton.eri('DEDEC')
alpha_beta.shape = (naux, naux)


# - contracted integrals
#        g_alpha = D_ab (ab|alpha)
galpha = np.einsum('abA,ab', ab_alpha, density, optimize=True)


# - linear solver for the fitting coefficients
#       (beta|alpha)^{-1}
ab_inv = np.linalg.inv(alpha_beta)
#       c_alpha = (alpha|beta)^{-1} g_beta
calpha = np.einsum('AB,B', ab_inv, galpha)

# - df-J matrix construction
#       J_ab = (ab|alpha) C_alpha
dfJ = np.einsum('A,abA', calpha, ab_alpha)

print("---dfJ")
print(dfJ)


# Prints for unit testing
#print("---ab_alpha")
#print(ab_alpha.tolist())
#print("---alpha_beta")
#print(alpha_beta.tolist())
#print("---eri")
#print(eri.tolist())

#alpha_beta = pylsdalton.eri('DEDEH')

pylsdalton.destroy()

