!> @file
!> Module defining perturbations and corresponding subroutines,
!> for the time being they should be the same
!> as those in external/openrsp/src/ao_dens/rsp_pert_table.F90
!> \author Bin Gao and Magnus Ringholm
!> \date 2015-02-20
module ls_openrsp_perturbations

    use precision, only: INTK, realk
    use OpenRSP_f, only: QcPertInt

    implicit none

    ! number of all supported perturbations, MUST be greater than the
    ! identifiers of all perturbations
    integer(kind=INTK), public, parameter :: OPENRSP_NUM_PERT = 6_INTK

    ! null perturbation (for use in e.g. XC routines)
    ! MaR: For now leave the null perturbation out of other arrays
    ! connected to perturbation label management as it should not see
    ! use in routines needing that information
    integer(kind=QcPertInt), public, parameter :: OPENRSP_NULL_PERT = 0
    ! identifier of geometric derivatives
    integer(kind=QcPertInt), public, parameter :: OPENRSP_GEO_PERT = 1
    ! identifier of electric dipole perturbations
    integer(kind=QcPertInt), public, parameter :: OPENRSP_EL_PERT = 2
    ! identifier of electric dipole gradient perturbations
    integer(kind=QcPertInt), public, parameter :: OPENRSP_ELGR_PERT = 3
    ! identifier of magnetic derivatives without LAOs
    integer(kind=QcPertInt), public, parameter :: OPENRSP_MAG0_PERT = 4
    ! identifier of magnetic derivatives with LAOs
    integer(kind=QcPertInt), public, parameter :: OPENRSP_MAG_PERT = 5

    ! labels of all perturbations
    integer(kind=QcPertInt), public, parameter ::                    &
        OPENRSP_PERT_LABELS(OPENRSP_NULL_PERT:OPENRSP_NUM_PERT-1) =  &
        (/OPENRSP_NULL_PERT,                                         &
          OPENRSP_GEO_PERT,                                          &
          OPENRSP_EL_PERT,                                           &
          OPENRSP_ELGR_PERT,                                         &
          OPENRSP_MAG0_PERT,                                         &
          OPENRSP_MAG_PERT/)
    ! allowed maximal orders of all perturbations
    integer(kind=INTK), public, parameter ::                            &
        OPENRSP_PERT_MAX_ORDERS(OPENRSP_NULL_PERT:OPENRSP_NUM_PERT-1) = &
        (/1_INTK,  &
          10_INTK, &
          10_INTK, &  !for exchange-correlation, we need higher order for EL
          1_INTK,  &
          2_INTK,  &
          2_INTK/)
    ! static perturbation or not
    logical, public, parameter ::                                   &
        OPENRSP_PERT_STATIC(OPENRSP_NULL_PERT:OPENRSP_NUM_PERT-1) = &
        (/.true.,                                                   &
          .true.,                                                   &
          .false.,                                                  &
          .false.,                                                  &
          .false.,                                                  &
          .false./)


    ! Perturbation ID datatype for use in XC contribution routines
    type pid_tuple
       integer :: npert
       integer, allocatable, dimension(:) :: pid
    end type

    ! Index block datatype for use in conjunction with XC contribution routines
    type triangulated_index_block
       integer, allocatable, dimension(:,:) :: t_ind
    end type


    public :: ls_openrsp_get_pert_label
    public :: ls_openrsp_get_pert_num_comps
    public :: ls_openrsp_get_size_props
    public :: ls_openrsp_get_pert_concatenation
    public :: ls_openrsp_get_pert_orders

    public :: get_num_blks
    public :: get_blk_info
    public :: get_triangular_sizes
    public :: make_triangulated_indices
    public :: get_triang_blks_offset

    contains

    !> \brief Returns LSDalton's identifier of a perturbation given by user-specified label
    !> \author Bin Gao
    !> \date 2015-02-20
    !> \param user_label User-specified label (characters) of a perturbation
    !> \param lupri Default print unit
    !> \return pert_label LSDalton's identifier (integer) of the perturbation
    subroutine ls_openrsp_get_pert_label(user_label, pert_label, lupri)
        character(*), intent(in) :: user_label
        integer(kind=QcPertInt), intent(out) :: pert_label
        integer(kind=INTK), intent(in) :: lupri
        select case (trim(user_label))
        case ('GEO')
            pert_label = OPENRSP_GEO_PERT
        case ('EL')
            pert_label = OPENRSP_EL_PERT
        case default
            write(lupri,100) ' Label "'//trim(user_label)// &
                             '" not recognized or implemented'
            call lsquit('ls_openrsp_get_pert_label>> illegal perturbation label', &
                        lupri)
        end select
100     format(A)
        return
    end subroutine ls_openrsp_get_pert_label

    !> \brief Gets the numbers of components of all LSDalton's defined perturbations
    !> \author Bin Gao
    !> \date 2015-02-20
    !> \param num_atoms Number of atoms
    !> \param lupri Default print unit
    !> \return pert_num_comps Number of components of all LSDalton's defined perturbations
    subroutine ls_openrsp_get_pert_num_comps(num_atoms, pert_num_comps, lupri)
        integer(kind=INTK), intent(in) :: num_atoms
        integer(kind=INTK), intent(inout) :: pert_num_comps(:)
        integer(kind=INTK), intent(in) :: lupri
        integer(kind=INTK) ipert, iorder, icomp
        if (size(pert_num_comps)<sum(OPENRSP_PERT_MAX_ORDERS)) then
            call lsquit('ls_openrsp_get_pert_num_comps>> size of pert_num_comps not enough', &
                        lupri)
        end if
        icomp = 0
        do ipert = OPENRSP_NULL_PERT, OPENRSP_NUM_PERT-1
            select case (OPENRSP_PERT_LABELS(ipert))
            case (OPENRSP_NULL_PERT)
                do iorder = 1, OPENRSP_PERT_MAX_ORDERS(ipert)
                    icomp = icomp+1
                    pert_num_comps(icomp) = 1
                end do
            case (OPENRSP_GEO_PERT)
                icomp = icomp+1
                pert_num_comps(icomp) = 3*num_atoms
                do iorder = 2, OPENRSP_PERT_MAX_ORDERS(ipert)
                    icomp = icomp+1
!FIXME: non-redundant geometric derivatives
                    pert_num_comps(icomp) = pert_num_comps(icomp-1) &
                                          * (3*num_atoms+iorder-1)/iorder
                end do
            case (OPENRSP_EL_PERT)
                icomp = icomp+1
                pert_num_comps(icomp) = 3
                do iorder = 2, OPENRSP_PERT_MAX_ORDERS(ipert)
                    icomp = icomp+1
!FIXME: number of components for higher order (>=2) electric dipole perturbations?
                    pert_num_comps(icomp) = 3
                end do
            case (OPENRSP_ELGR_PERT)
                icomp = icomp+1
                pert_num_comps(icomp) = 6
                do iorder = 2, OPENRSP_PERT_MAX_ORDERS(ipert)
                    icomp = icomp+1
!FIXME: number of components for higher order (>=2) electric quadruple perturbations?
                    pert_num_comps(icomp) = 6
                end do
            case default
                do iorder = 1, OPENRSP_PERT_MAX_ORDERS(ipert)
                    icomp = icomp+1
                    pert_num_comps(icomp) = (iorder+1)*(iorder+2)/2
                end do
            end select
        end do
        return
    end subroutine ls_openrsp_get_pert_num_comps

    !> \brief Gets the size of given properties (perturbations and corresponding frequencies)
    !> \author Bin Gao and Magnus Ringholm
    !> \date 2016-10-20
    !> \param num_props Number of properties
    !> \param len_tuple Length of perturbation tuple for each property
    !> \param pert_tuple Ordered list of perturbation labels (perturbation
    !>                   tuple) for each property
    !> \param num_freq_configs Number of different frequency configurations for
    !>                         each property
    !> \param pert_freqs Complex frequencies of each perturbation label over
    !>                   all frequency configurations
    !> \param num_atoms Number of atoms
    !> \param lupri Default print unit
    !> \return size_props Size of given properties
    subroutine ls_openrsp_get_size_props(num_props,        &
                                         len_tuple,        &
                                         pert_tuple,       &
                                         num_freq_configs, &
                                         pert_freqs,       &
                                         num_atoms,        &
                                         size_props,       &
                                         lupri)
        integer(kind=INTK), intent(in) :: num_props
        integer(kind=INTK), intent(in) :: len_tuple(num_props)
        integer(kind=QcPertInt), intent(in) :: pert_tuple(sum(len_tuple))
        integer(kind=INTK), intent(in) :: num_freq_configs(num_props)
        real(kind=realk), intent(in) :: pert_freqs(2*dot_product(len_tuple, num_freq_configs))
        integer(kind=INTK), intent(in) :: num_atoms
        integer(kind=INTK), intent(out) :: size_props
        integer(kind=INTK), intent(in) :: lupri
        !local variables
        integer(kind=INTK) iprop           !incremental recorder over properties
        integer(kind=INTK) ipert, jpert    !incremental recorders over perturbations
        integer(kind=INTK) size_each_prop  !size of each property
        integer(kind=INTK) num_geo_pert    !number of geometric perturbations of each property
        integer, allocatable, dimension(:) :: pert_freq_category
        integer, allocatable, dimension(:,:) :: curr_perts_int, blk_info
        integer :: p_inc, p_inc_p
        logical :: srch_fin
        integer :: nblks
        integer :: i, j, k, m, p
        
        
        size_props = 0
        p_inc = 0
        iprop = 0
    
        do p = 1, num_props
    
           allocate(pert_freq_category(len_tuple(p) *  num_freq_configs(p)))
           p_inc_p = 0
    
           do i = 1, num_freq_configs(p)
    
              m = 1
          
              do j = 1, len_tuple(p)
       
                 srch_fin = .FALSE.
       
                 do k = 1, j - 1
          
                    if (.NOT.(srch_fin)) then
          
                       if (pert_freqs(2 * (p_inc + p_inc_p + k) - 1) == &
                           pert_freqs(2 * (p_inc + p_inc_p + j) - 1)) then
             
                          pert_freq_category(p_inc_p + j) = &
                          pert_freq_category(p_inc_p + k)
                   
                          srch_fin = .TRUE.
                   
                       end if
                   
                    end if
             
                 end do
          
                 if (.NOT.(srch_fin)) then

                    pert_freq_category(p_inc_p + j) = m
                    m = m + 1
          
                 end if
       
              end do
          
              p_inc_p = p_inc_p + len_tuple(p)
    
           end do

           p_inc_p = 0
       
           allocate(curr_perts_int(len_tuple(p), 3))
 
           do i = 1, len_tuple(p)
          
              ! FIXME: MAKE SEPARATE ROUTINE FOR NUMBER OF COMPONENTS
              ! FOR THIS PURPOSE ALSO MAKE FUNCTION TO RETURN NUMBER OF ATOMS
          
              curr_perts_int(i, 1) = pert_tuple(sum(len_tuple(1:p)) - len_tuple(p) + i)
          
              select case (curr_perts_int(i, 1))
          
                 case (OPENRSP_GEO_PERT)
           
                    curr_perts_int(i, 3) = 3 * num_atoms
                
                 case (OPENRSP_EL_PERT)
           
                    curr_perts_int(i, 3) = 3
                
                 case (OPENRSP_ELGR_PERT)
           
                    curr_perts_int(i, 3) = 6
                
                 case (OPENRSP_MAG_PERT)
           
                    curr_perts_int(i, 3) = 3
                
                 case (OPENRSP_MAG0_PERT)
           
                    curr_perts_int(i, 3) = 3
           
                 case default
           
                    ! Error case: Unrecognized perturbation category
           
              end select
          
           end do

           do i = 1, num_freq_configs(p)
       
              iprop = iprop + 1
       
              do j = 1, len_tuple(p)
          
                 curr_perts_int(j, 2) = pert_freq_category(p_inc_p + j)
          
              end do
 
              ! Find property size
        
              nblks = get_num_blks(len_tuple(p), curr_perts_int)
              
              allocate(blk_info(nblks, 3))
           
              blk_info = get_blk_info(nblks, len_tuple(p), curr_perts_int)
           
              size_props = size_props                                                &
                         + product(get_triangular_sizes(nblks, blk_info(1:nblks, 2), &
                                   blk_info(1:nblks, 3)))
                                                   
              deallocate(blk_info)
          
              p_inc_p = p_inc_p + len_tuple(p)
       
           end do
       
           deallocate(curr_perts_int)
       
           deallocate(pert_freq_category)
       
           p_inc = p_inc + len_tuple(p) * num_freq_configs(p)
    
        end do
       
        return
100     format('ls_openrsp_get_size_props>> ',A,3I8)
    end subroutine ls_openrsp_get_size_props


    !> \brief Callback subroutine for getting the ranks of components of
    !>        sub-perturbation tuples (with the same perturbation label) for
    !>        given components of the corresponding concatenated perturbation
    !>        tuple
    !> \author Bin Gao
    !> \date 2015-02-20
    !> \param pert_label Perturbation label
    !> \param first_cat_comp Rank of the first component of the concatenated
    !>                       perturbation tuple
    !> \param num_cat_comps Number of components of the concatenated perturbation
    !>                      tuple
    !> \param num_sub_tuples Number of sub-perturbation tuples to construct the
    !>                       concatenated perturbation tuple
    !> \param len_sub_tuples Length of each sub-perturbation tuple
    !> \param user_ctx Contains user-defined callback subroutine context,
    !>                 encoded as type C_PTR
    !> \return rank_sub_comps Ranks of components of sub-perturbation tuples for
    !>                        the corresponding component of the concatenated
    !>                        perturbation tuple
    subroutine ls_openrsp_get_pert_concatenation(pert_label,     &
                                                 first_cat_comp, &
                                                 num_cat_comps,  &
                                                 num_sub_tuples, &
                                                 len_sub_tuples, &
                                                 user_ctx,       &
                                                 rank_sub_comps)
        use, intrinsic :: iso_c_binding
        integer(kind=QcPertInt), intent(in) :: pert_label
        integer(kind=INTK), intent(in) :: first_cat_comp
        integer(kind=INTK), intent(in) :: num_cat_comps
        integer(kind=INTK), intent(in) :: num_sub_tuples
        integer(kind=INTK), intent(in) :: len_sub_tuples(num_sub_tuples)
        type(C_PTR), intent(in) :: user_ctx
        integer(kind=INTK), intent(out) :: rank_sub_comps(num_cat_comps*num_sub_tuples)
        return
    end subroutine ls_openrsp_get_pert_concatenation

    !> \brief Gets orders of all LSDalton's defined perturbations from given
    !>        perturbations of callback subroutines
    !> \author Bin Gao
    !> \date 2016-10-20
    !> \param num_pert Number of perturbations
    !> \param pert_labels Given perturbation labels
    !> \param pert_orders Given perturbation orders
    !> \param lupri Default print unit
    !> \return ls_pert_orders Orders of all LSDalton's defined perturbations
    subroutine ls_openrsp_get_pert_orders(num_pert,       &
                                          pert_labels,    &
                                          pert_orders,    &
                                          ls_pert_orders, &
                                          lupri)
        integer(kind=INTK), intent(in) :: num_pert
        integer(kind=QcPertInt), intent(in) :: pert_labels(num_pert)
        integer(kind=INTK), intent(in) :: pert_orders(num_pert)
        integer(kind=INTK), intent(out) :: ls_pert_orders(OPENRSP_NUM_PERT)
        integer(kind=INTK), intent(in) :: lupri
        integer(kind=INTK) ipert
        ls_pert_orders = 0
        do ipert = 1, num_pert
            select case (pert_labels(ipert))
            case (OPENRSP_GEO_PERT)
                ls_pert_orders(OPENRSP_GEO_PERT) = ls_pert_orders(OPENRSP_GEO_PERT) &
                                                 + pert_orders(ipert)
            case (OPENRSP_EL_PERT)
                ls_pert_orders(OPENRSP_EL_PERT) = ls_pert_orders(OPENRSP_EL_PERT) &
                                                + pert_orders(ipert)
            case (OPENRSP_ELGR_PERT)
                ls_pert_orders(OPENRSP_ELGR_PERT) = ls_pert_orders(OPENRSP_ELGR_PERT) &
                                                  + pert_orders(ipert)
            case (OPENRSP_MAG0_PERT)
                ls_pert_orders(OPENRSP_MAG0_PERT) = ls_pert_orders(OPENRSP_MAG0_PERT) &
                                                  + pert_orders(ipert)
            case (OPENRSP_MAG_PERT)
                ls_pert_orders(OPENRSP_MAG_PERT) = ls_pert_orders(OPENRSP_MAG_PERT) &
                                                 + pert_orders(ipert)
            case default
                write(lupri,100) ipert, pert_labels(ipert)
                call lsquit('ls_openrsp_get_pert_orders>> unknown perturbation', &
                            lupri)
            end select
        end do
        return
100     format("ls_openrsp_get_pert_orders>> ",2I8)
    end subroutine ls_openrsp_get_pert_orders


        ! Indexing/addressing routines
    
    
!> \brief Get number of unique perturbations ("blocks") in tuple 'fields'
!> \author M. Ringholm
!> \date 2016
!> \param nfields Number of perturbations in tuple
!> \param fields The perturbation tuples
!> \return get_num_blks Number of blocks
  function get_num_blks(nfields, fields)

    implicit none

    integer :: i, curr_blk_start, get_num_blks, nfields
    integer, dimension(nfields, 3) :: fields

    curr_blk_start = 1
    get_num_blks = 1

    do i = 1, nfields

       if (.NOT.( (fields(curr_blk_start,1) == fields(i,1)) .AND. &
                     (fields(curr_blk_start,2) == fields(i,2)) ) ) then

          curr_blk_start = i
          get_num_blks = get_num_blks + 1

       end if

    end do

  end function
    
    
!> \brief Get various block information for perturbation tuple 'fields'
!> \author M. Ringholm
!> \date 2016
!> \param nblks Number of blocks in perturbation tuple
!> \param nfields Number of perturbations in tuple
!> \param fields The perturbation tuples
!> \return get_blk_info various block information
  function get_blk_info(nblks, nfields, fields)

    implicit none

    integer :: nblks, nfields, i, this_blk, curr_blk_start
    integer, dimension(nblks,3) :: get_blk_info   
    integer, dimension(nfields, 3) :: fields

    if (nfields > 0) then

       this_blk = 1
       curr_blk_start = 1
       get_blk_info(1, 1) = 1
       get_blk_info(1, 3) = fields(1,3)

       do i = 1, nfields


          if (.NOT.( (fields(curr_blk_start,1) == fields(i,1)) .AND. &
                     (fields(curr_blk_start,2) == fields(i,2)) ) ) then
           
             curr_blk_start = i
             this_blk = this_blk + 1
             get_blk_info(this_blk, 1) = i
             get_blk_info(this_blk - 1, 2) = i - get_blk_info(this_blk - 1, 1)
             get_blk_info(this_blk, 3) = fields(i, 3)

          end if

       end do

       if (nblks > 1) then

          get_blk_info(nblks, 2) = nfields - get_blk_info(nblks, 1) + 1

       elseif (nblks == 1) then

          get_blk_info(1, 2) = nfields

       end if

    else

       get_blk_info(1, 1) = 1
       get_blk_info(1, 2) = 1
       get_blk_info(1, 3) = 1

    end if

  end function
    

!> \brief Make nonredundant indices for tuple of perturbations
!> \author M. Ringholm
!> \date 2016
!> \param nblks Number of blocks in perturbation tuple
!> \param blk_info Various block info
!> \param triangulated_size Total number of indices
!> \return indices Indices to be returned
  subroutine make_triangulated_indices(nblks, blk_info, triangulated_size, indices)

    implicit none

    integer :: nblks, i, triangulated_size, j
    integer, dimension(nblks) :: triang_sizes
    integer, dimension(nblks, 3) :: blk_info
    integer, dimension(triangulated_size, sum(blk_info(:,2))) :: indices
    type(triangulated_index_block), allocatable, dimension(:) :: blks

    allocate(blks(nblks))

    do i = 1, nblks

       triang_sizes(i) = get_one_triangular_size(blk_info(i, 2), blk_info(i,3))

       allocate(blks(i)%t_ind(triang_sizes(i), blk_info(i, 2)))

       call make_one_triang_index_blk(blk_info(i, 2), blk_info(i, 3), 1, 1, 1, &
                                      triang_sizes(i), blks(i)%t_ind)

    end do

    call index_blks_direct_product(nblks, triang_sizes, blks, indices, &
                                   sum(blk_info(:,2)), 1, 1, 1)

    do i = 1, nblks

       deallocate(blks(i)%t_ind)

    end do

    deallocate(blks)

  end subroutine


  
!> \brief Make direct product of one set of indices (corresponding to one block) to another 
!> \author M. Ringholm
!> \date 2016
!> \param nblks Number of blocks in perturbation tuple
!> \param blk_sizes Block sizes
!> \param blks The blocks
!> \param nways Total tensor rank
!> \param current_way Current rank under consideration
!> \param lvl Recursion level parameter
!> \param offset Offset parameter
!> \return indices Indices to be returned
  recursive subroutine index_blks_direct_product(nblks, blk_sizes, blks, indices, &
                       nways, current_way, lvl, offset)

    implicit none

    integer :: nblks, current_way, lvl, i, j, offset, increment, nways, new_offset
    integer, dimension(nblks) :: blk_sizes
    type(triangulated_index_block), dimension(nblks) :: blks
    integer, dimension(product(blk_sizes), nways) :: indices

    if (lvl < nblks) then

       increment = product(blk_sizes(lvl:nblks))/blk_sizes(lvl)

       do i = 0, blk_sizes(lvl) - 1

          do j = 0, increment - 1

             indices(offset + i * increment + j, &
                     current_way:current_way + size(blks(lvl)%t_ind, 2) - 1) = &
                     blks(lvl)%t_ind(i + 1, :)

          end do

          new_offset = offset + i * increment
          call index_blks_direct_product(nblks, blk_sizes, blks, indices, &
               nways, current_way + size(blks(lvl)%t_ind, 2), lvl + 1, new_offset)

       end do

    elseif (lvl == nblks) then

       do i = 0, blk_sizes(lvl) - 1

          indices(offset + i, current_way:current_way + size(blks(lvl)%t_ind, 2) - 1) = &
          blks(lvl)%t_ind(i + 1, :)

       end do

    end if

  end subroutine


!> \brief Make one set of indices for one block of perturbations
!> \author M. Ringholm
!> \date 2016
!> \param blk_size Block size
!> \param pdim Perturbation dimensions
!> \param st_ind Starting index
!> \param lvl Recursion level parameter
!> \param offset Offset parameter
!> \param triang_size Nonredundant size of index block
!> \return index_blk Index block to be returned
  recursive subroutine make_one_triang_index_blk(blk_size, pdim, st_ind, lvl, offset, &
                                                   triang_size, index_blk)

    implicit none

    integer :: blk_size, pdim, st_ind, lvl, i, j, offset, &
               triang_size, new_offset, increment
    integer, dimension(triang_size, blk_size) :: index_blk

    if (lvl < blk_size) then

       new_offset = offset

       do i = 0, pdim - st_ind

          increment = get_one_triangular_size(blk_size - lvl, pdim - st_ind - i + 1)
          index_blk(new_offset:new_offset + increment - 1, lvl) = & 
          (i + st_ind) * (/ (j/j, j = 1, increment)/)

          call make_one_triang_index_blk(blk_size, pdim, st_ind + i, lvl + 1, &
                                         new_offset, triang_size, index_blk)

          new_offset = new_offset + increment

       end do

    elseif (lvl == blk_size) then

       do i = 0, pdim - st_ind

          index_blk(offset + i, lvl) = i + st_ind

       end do

    end if

  end subroutine

  
!> \brief Get size of nonredundant representation of elements for one perturbation tuple
!> \author M. Ringholm
!> \date 2016
!> \param nblks Number of blks
!> \param blk_nfield Perturbation order for each block
!> \param pdims Perturbation type dimension
!> \return blk_sizes Block sizes
  function get_triangular_sizes(nblks, blk_nfield, pdims) result(blk_sizes)

    implicit none

    integer :: i, nblks
    integer, dimension(nblks) :: blk_nfield, pdims, blk_sizes

    do i = 1, nblks

       blk_sizes(i) = get_one_triangular_size(blk_nfield(i), pdims(i))

    end do 

  end function

  
!> \brief Get size of nonredundant representation of elements for one perturbation tuple
!> \author M. Ringholm
!> \date 2016
!> \param blk_size Block size
!> \param pdim Perturbation dimension
!> \return get_one_triangular_size Size result 
  function get_one_triangular_size(blk_size, pdim)

    implicit none

    integer :: get_one_triangular_size, blk_size, pdim

    get_one_triangular_size = fact_terminate_lower(pdim + blk_size - 1, pdim) / &
                              fact_terminate_lower(blk_size,  1)

  end function

  
!> \brief Get offset (non-redundant) for indices 'ind', in tuple of perturbations
!> \author M. Ringholm
!> \date 2016
!> \param nblks Number of blocks
!> \param nfield Number of fields
!> \param blk_info Block information
!> \param blk_sizes Block sizes
!> \param ind Indices
!> \return offset Offset result
  function get_triang_blks_offset(nblks, nfield, blk_info, blk_sizes, ind) &
           result(offset)

    implicit none

    integer :: nblks, nfield, i, offset
    integer, dimension(nblks) :: blk_sizes
    integer, dimension(nblks, 3) :: blk_info
    integer, dimension(nfield) :: ind

    offset = 0

    do i = 1, nblks - 1

       offset = offset + (get_triang_offset(blk_info(i,2), &
       ind(blk_info(i,1): blk_info(i,1) + blk_info(i,2) - 1), blk_info(i,3)) - 1)* &
                            product(blk_sizes(i:nblks))/blk_sizes(i)

    end do

    offset = offset + get_triang_offset(blk_info(nblks,2), &
             ind(blk_info(nblks,1): blk_info(nblks,1) + blk_info(nblks,2) - 1), &
             blk_info(nblks,3))

  end function

!> \brief Get offset for one block taken from one perturbation tuple
!> \author M. Ringholm
!> \date 2016
!> \param nfield Number of fields
!> \param ind Indices
!> \param pdim Perturbation dimension
!> \return offset Offset result
  function get_triang_offset(nfield, ind, pdim) result(offset)

    implicit none

    integer :: i, offset, nfield, pdim
    integer, dimension(nfield) :: ind

    offset = 1

    if (nfield > 1) then

       offset = offset + get_one_way_triang_offset(nfield - 1, ind(1) - 1, pdim)

       do i = 2, nfield - 1

          offset = offset + get_one_way_triang_offset(nfield - i, ind(i) - ind(i - 1), &
                            pdim - ind(i - 1) + 1)

       end do

       offset = offset + ind(nfield) - ind(nfield - 1)

    else 

       offset = ind(1)

    end if

  end function


!> \brief Get offset corresponding to one way of one block taken from one perturbation tuple
!> \author M. Ringholm
!> \date 2016
!> \param remaining Remaining ranks
!> \param ind Indices
!> \param pdim Perturbation dimension
!> \return offset Offset result  
  function get_one_way_triang_offset(remaining, ind, pdim) result(offset)

    implicit none

    integer :: remaining, ind, pdim, offset, i

    offset = 0

    do i = 0, ind - 1

       offset = offset + fact_terminate_lower(pdim - i + remaining - 1, pdim - i) / &
                         fact_terminate_lower(remaining, 1)

    end do

  end function
  
  
  
  
  
!> \brief Calculate the factorial quotient (highest)!/(lowest)!
!> \author M. Ringholm
!> \date 2016
!> \param highest Factorial numerator parameter
!> \param lowest Factorial denominator parameter
!> \return ftl Factorial quotient result
  recursive function fact_terminate_lower(highest, lowest) result(ftl)

    implicit none

    integer :: highest, lowest
    integer(8) :: ftl

    if (highest == lowest) then

       ftl = highest   

    else

       ftl = highest * fact_terminate_lower(highest - 1, lowest)

    end if
    
  end function
    
    ! End indexing/addressing routines
    

end module ls_openrsp_perturbations
