#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > cartes_bfgs_min.info <<'%EOF%'
   dft_lda_molhes 
   -------------
   Molecule:         Water
   Wave Function:    DFT / LDA / 6-31G / Ahlrichs-Coulomb-Fit
   Test Purpose:     Test geometry optimizer in Cartesian coordinates,
   BFGS update of initially unit Hessian
                     
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > cartes_bfgs_min.mol <<'%EOF%'
BASIS
6-31G Aux=Ahlrichs-Coulomb-Fit
H2O
First order minimization in Cartesian coordinates
Atomtypes=2 Generators=0
Charge=8.0 Atoms=1
O     0.0000000000       -0.2249058930        0.0000000000             
Charge=1.0 Atoms=2
H     1.4523500000        0.8996230000        0.0000000000             
H    -1.4523500000        0.8996230000        0.0000000000             
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > cartes_bfgs_min.dal <<'%EOF%'
**OPTIMI
.CARTES
.INITEV
1.D0
.BAKER
**WAVE FUNCTIONS
.DFT
 LDA
*DFT INPUT
.GRID TYPE
 BECKEORIG LMG
.RADINT
1.0D-11
.ANGINT
31
*DENSOPT
.START
H1DIAG
.RESTART
**INFO
.DEBUG_MPI_MEM
*END OF INPUT
%EOF%
#######################################################################

 

#######################################################################

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL > cartes_bfgs_min.check
cat >> cartes_bfgs_min.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Energy
CRIT1=`$GREP "Final * DFT energy\: * \-75\.81990079" $log | wc -l`
TEST[1]=`expr   $CRIT1`
CTRL[1]=1
ERROR[1]="DFT ENERGY NOT CORRECT -"

# Memory test
CRIT2=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[2]=`expr  $CRIT2`
CTRL[2]=1
ERROR[2]="Memory leak -"

# Number of energies
CRIT3=`$GREP "Final * DFT energy" $log | wc -l` 
TEST[3]=`expr   $CRIT3`
CTRL[3]=7
ERROR[3]="Wrong number of geometry steps"

# MPI Memory test
CRIT1=`$GREP "[0-9][0-9] byte  \- Should be zero \- otherwise a leakage is present" $log | wc -l`
TEST[4]=`expr  $CRIT1`
CTRL[4]=0
ERROR[4]="MPI Memory leak -"

PASSED=1
for i in 1 2 3 4
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM
  exit 1
fi

%EOF%
#######################################################################
