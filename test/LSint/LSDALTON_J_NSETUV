#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > LSDALTON_J_NSETUV.info <<'%EOF%'
   dft_lda_molhes 
   -------------
   Molecule:         C4H2
   Wave Function:    HF/ Turbomole-SVP
   Test Purpose:     Test .NSETUV keyword
                     
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > LSDALTON_J_NSETUV.mol <<'%EOF%'
BASIS
Turbomole-SVP
C4H2 molecule 
testing .NSETUV
Atomtypes=2  Nosymmetry Angstrom
Charge=1 Atoms=2
H1  0.000000    0.000000   -2.962513
H2  0.000000    0.000000    2.962198
Charge=6 Atoms=4
C1  0.000000    0.000000   -1.896750
C2  0.000000    0.000000   -0.684323
C3  0.000000    0.000000    0.684363
C4  0.000000    0.000000    1.896763
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > LSDALTON_J_NSETUV.dal <<'%EOF%'
**INTEGRALS
.NO CS
.NO PS
.NOLINK
.NSETUV
**WAVE FUNCTIONS
.HF
*DENSOPT
**INFO
.DEBUG_MPI_MEM
*END OF INPUT
%EOF%
#######################################################################

 

#######################################################################

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >LSDALTON_J_NSETUV.check
cat >>LSDALTON_J_NSETUV.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Energy
CRIT1=`$GREP "Final * HF energy\: * \-152\.3818574" $log | wc -l`
TEST[1]=`expr   $CRIT1`
CTRL[1]=1
ERROR[1]="HF ENERGY NOT CORRECT -"

# Memory test
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[2]=`expr  $CRIT1`
CTRL[2]=1
ERROR[2]="Memory leak -"

# MPI Memory test
CRIT1=`$GREP "[0-9][0-9] byte  \- Should be zero \- otherwise a leakage is present" $log | wc -l`
TEST[3]=`expr  $CRIT1`
CTRL[3]=0
ERROR[3]="MPI Memory leak -"

PASSED=1
for i in 1 2 3
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM
  exit 1
fi

%EOF%
#######################################################################
