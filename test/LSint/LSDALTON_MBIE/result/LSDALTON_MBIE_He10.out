     THIS IS A DEBUG BUILD
  
     ******************************************************************    
     **********  LSDalton - An electronic structure program  **********    
     ******************************************************************    
  
  
    This is output from LSDalton 1.0
  
  
     IF RESULTS OBTAINED WITH THIS CODE ARE PUBLISHED,
     THE FOLLOWING PAPER SHOULD BE CITED:
     
     K. Aidas, C. Angeli, K. L. Bak, V. Bakken, R. Bast,
     L. Boman, O. Christiansen, R. Cimiraglia, S. Coriani,
     P. Dahle, E. K. Dalskov, U. Ekstroem, T. Enevoldsen,
     J. J. Eriksen, P. Ettenhuber, B. Fernandez,
     L. Ferrighi, H. Fliegl, L. Frediani, K. Hald,
     A. Halkier, C. Haettig, H. Heiberg,
     T. Helgaker, A. C. Hennum, H. Hettema,
     E. Hjertenaes, S. Hoest, I.-M. Hoeyvik,
     M. F. Iozzi, B. Jansik, H. J. Aa. Jensen,
     D. Jonsson, P. Joergensen, J. Kauczor,
     S. Kirpekar, T. Kjaergaard, W. Klopper,
     S. Knecht, R. Kobayashi, H. Koch, J. Kongsted,
     A. Krapp, K. Kristensen, A. Ligabue,
     O. B. Lutnaes, J. I. Melo, K. V. Mikkelsen, R. H. Myhre,
     C. Neiss, C. B. Nielsen, P. Norman,
     J. Olsen, J. M. H. Olsen, A. Osted,
     M. J. Packer, F. Pawlowski, T. B. Pedersen,
     P. F. Provasi, S. Reine, Z. Rinkevicius,
     T. A. Ruden, K. Ruud, V. Rybkin,
     P. Salek, C. C. M. Samson, A. Sanchez de Meras,
     T. Saue, S. P. A. Sauer, B. Schimmelpfennig,
     K. Sneskov, A. H. Steindal, K. O. Sylvester-Hvid,
     P. R. Taylor, A. M. Teale, E. I. Tellgren,
     D. P. Tew, A. J. Thorvaldsen, L. Thoegersen,
     O. Vahtras, M. A. Watson, D. J. D. Wilson,
     M. Ziolkowski, and H. AAgren,
     "The Dalton quantum chemistry program system",
     WIREs Comput. Mol. Sci. (doi: 10.1002/wcms.1172)
  
  
                                               
    LSDalton authors in alphabetical order (main contribution(s) in parenthesis)
    ----------------------------------------------------------------------------
    Vebjoern Bakken,        University of Oslo,        Norway   (Geometry optimizer)
    Radovan Bast,           UiT The Arctic University of Norway (CMake, Testing)
    Pablo Baudin,           Aarhus University,         Denmark  (DEC,CCSD)
    Sonia Coriani,          University of Trieste,     Italy    (Response)
    Patrick Ettenhuber,     Aarhus University,         Denmark  (CCSD)
    Janus Juul Eriksen,     Aarhus University,         Denmark  (CCSD(T), DEC)
    Trygve Helgaker,        University of Oslo,        Norway   (Supervision)
    Stinne Hoest,           Aarhus University,         Denmark  (SCF optimization)
    Ida-Marie Hoeyvik,      Aarhus University,         Denmark  (Orbital localization, SCF opt)
    Robert Izsak,           University of Oslo,        Norway   (ADMM)
    Branislav Jansik,       Aarhus University,         Denmark  (Trilevel, orbital localization)
    Poul Joergensen,        Aarhus University,         Denmark  (Supervision)
    Joanna Kauczor,         Aarhus University,         Denmark  (Response solver)
    Thomas Kjaergaard,      Aarhus University,         Denmark  (RSP, INT, DEC, SCF, Readin, MPI, MAT)
    Andreas Krapp,          University of Oslo,        Norway   (FMM, dispersion-corrected DFT)
    Kasper Kristensen,      Aarhus University,         Denmark  (Response, DEC)
    Patrick Merlot,         University of Oslo,        Norway   (ADMM)
    Cecilie Nygaard,        Aarhus University,         Denmark  (SOEO)
    Jeppe Olsen,            Aarhus University,         Denmark  (Supervision)
    Simen Reine,            University of Oslo,        Norway   (Integrals, geometry optimizer)
    Vladimir Rybkin,        University of Oslo,        Norway   (Geometry optimizer, dynamics)
    Pawel Salek,            KTH Stockholm,             Sweden   (FMM, DFT functionals)
    Andrew M. Teale,        University of Nottingham   England  (E-coefficients)
    Erik Tellgren,          University of Oslo,        Norway   (Density fitting, E-coefficients)
    Andreas J. Thorvaldsen, University of Tromsoe,     Norway   (Response)
    Lea Thoegersen,         Aarhus University,         Denmark  (SCF optimization)
    Mark Watson,            University of Oslo,        Norway   (FMM)
    Marcin Ziolkowski,      Aarhus University,         Denmark  (DEC)
  
  
     NOTE:
      
     This is an experimental code for the evaluation of molecular
     energies and properties using various electronic structure models.
     The authors accept no responsibility for the performance of the code or
     for the correctness of the results.
      
     The code (in whole or part) is provided under a licence and
     is not to be reproduced for further distribution without
     the written permission of the authors or their representatives.
      
     See the home page "http://daltonprogram.org"
     for further information.
  
  
     Who compiled             | tkjaer
     Host                     | localhost.localdomain
     System                   | Linux-3.16.6-200.fc20.x86_64
     CMake generator          | Unix Makefiles
     Processor                | x86_64
     64-bit integers          | OFF
     MPI                      | OFF
     Fortran compiler         | /opt/intel/composer_xe_2013_sp1.3.174/bin/intel64/
                              | ifort
     Fortran compiler version | ifort (IFORT) 14.0.3 20140422
     C compiler               | /opt/intel/composer_xe_2013_sp1.3.174/bin/intel64/
                              | icc
     C compiler version       | icc (ICC) 14.0.3 20140422
     C++ compiler             | /opt/intel/composer_xe_2013_sp1.3.174/bin/intel64/
                              | icpc
     C++ compiler version     | icpc (ICC) 14.0.3 20140422
     BLAS                     | /opt/intel/composer_xe_2013_sp1.3.174/mkl/lib/inte
                              | l64/libmkl_intel_lp64.so;/opt/intel/composer_xe_20
                              | 13_sp1.3.174/mkl/lib/intel64/libmkl_sequential.so;
                              | /opt/intel/composer_xe_2013_sp1.3.174/mkl/lib/inte
                              | l64/libmkl_core.so;/usr/lib64/libpthread.so;/usr/l
                              | ib64/libm.so
     LAPACK                   | /opt/intel/composer_xe_2013_sp1.3.174/mkl/lib/inte
                              | l64/libmkl_lapack95_lp64.a;/opt/intel/composer_xe_
                              | 2013_sp1.3.174/mkl/lib/intel64/libmkl_intel_lp64.s
                              | o
     Static linking           | OFF
     Last Git revision        | 783589279943cd9c0e7f8071004bc5fb5402dace
     Git branch               | ThomasK/developQQR
     Configuration time       | 2016-02-18 13:17:21.614994
  

         Start simulation
     Date and time (Linux)  : Thu Feb 18 14:15:19 2016
     Host name              : localhost.localdomain                   
                      
    ---------------------------------------------------
             PRINTING THE MOLECULE.INP FILE 
    ---------------------------------------------------
                      
    BASIS                                   
    cc-pVTZ                                 
    10 He molecules placed on a string      
    STRUCTURE IS NOT OPTIMIZED. cc-pVDZ basi
    Atomtypes=1 Nosymmetry                                      
    Charge=2. Atoms=10                                          
    H   0.0    0.0    0.0                                       
    H   0.0   10.0    0.0                                       
    H   0.0   20.0    0.0                                       
    H   0.0   30.0    0.0                                       
    H   0.0   40.0    0.0                                       
    H   0.0   50.0    0.0                                       
    H   0.0   70.0    0.0                                       
    H   0.0   90.0    0.0                                       
    H   0.0  190.0    0.0                                       
    H   0.0  200.0    0.0                                       
                      
    ---------------------------------------------------
             PRINTING THE LSDALTON.INP FILE 
    ---------------------------------------------------
                      
    **GENERAL
    .FORCEGCBASIS
    **INTEGRALS
    .MBIE
    .THRESH
    1.d-5
    **WAVE FUNCTIONS
    .HF
    *DENSOPT
    .CONVTHR
    1.d-1
    **INFO
    .DEBUG_MPI_MEM
    *END OF INPUT
 
WARNING:  No bonds - no atom pairs are within normal bonding distances
WARNING:  maybe coordinates were in Bohr, but program were told they were in Angstrom ?
 
                      
    Atoms and basis sets
      Total number of atoms        :     10
      THE  REGULAR   is on R =   1
    ---------------------------------------------------------------------
      atom label  charge basisset                prim     cont   basis
    ---------------------------------------------------------------------
          1 H      2.000 cc-pVTZ                   17       14 [6s2p1d|3s2p1d]                              
          2 H      2.000 cc-pVTZ                   17       14 [6s2p1d|3s2p1d]                              
          3 H      2.000 cc-pVTZ                   17       14 [6s2p1d|3s2p1d]                              
          4 H      2.000 cc-pVTZ                   17       14 [6s2p1d|3s2p1d]                              
          5 H      2.000 cc-pVTZ                   17       14 [6s2p1d|3s2p1d]                              
          6 H      2.000 cc-pVTZ                   17       14 [6s2p1d|3s2p1d]                              
          7 H      2.000 cc-pVTZ                   17       14 [6s2p1d|3s2p1d]                              
          8 H      2.000 cc-pVTZ                   17       14 [6s2p1d|3s2p1d]                              
          9 H      2.000 cc-pVTZ                   17       14 [6s2p1d|3s2p1d]                              
         10 H      2.000 cc-pVTZ                   17       14 [6s2p1d|3s2p1d]                              
    ---------------------------------------------------------------------
    total         20                              170      140
    ---------------------------------------------------------------------
                      
                      
    Basic Molecule/Basis information
    --------------------------------------------------------------------
      Molecular Charge                   :    0.0000
      Regular basisfunctions             :      140
      Primitive Regular basisfunctions   :      170
    --------------------------------------------------------------------
                      
    Configuration:
    ==============
    This is a Single core calculation. (no OpenMP)
    This is a serial calculation (no MPI)

    You have requested Augmented Roothaan-Hall optimization
    => explicit averaging is turned off!
 
    Expand trust radius if ratio is larger than:            0.75
    Contract trust radius if ratio is smaller than:         0.25
    On expansion, trust radius is expanded by a factor      1.20
    On contraction, trust radius is contracted by a factor  0.70
 
    Maximum size of subspace in ARH linear equations:       2
 
    Density subspace min. method    : None                    
    Density optimization            : Augmented RH optimization          
 
    Maximum size of Fock/density queue in averaging:   10
 
    Convergence threshold for gradient        :   0.10E+00
    We have detected a Dunnings Basis but the 
    FORCEGCBASIS keyword is in effect.
     
    We perform the calculation in the Grand Canonical basis
    (see PCCP 2009, 11, 5805-5813)
    To use the standard input basis use .NOGCBASIS
 
    Since the input basis set is a segmented contracted basis we
    perform the integral evaluation in the more efficient
    standard input basis and then transform to the Grand 
    Canonical basis, which is general contracted.
    You can force the integral evaluation in Grand 
    Canonical basis by using the keyword
    .NOGCINTEGRALTRANSFORM
     
    The Overall Screening threshold is set to              :  1.0000E-05
    The Screening threshold used for Coulomb               :  1.0000E-07
    The Screening threshold used for Exchange              :  1.0000E-05
    The Screening threshold used for One-electron operators:  1.0000E-12
    The SCF Convergence Criteria is applied to the gradnorm in OAO basis
 
    End of configuration!
 
    >>>  CPU Time used in *INPUT is   0.00 seconds
    >>> wall Time used in *INPUT is   0.00 seconds
 
    Matrix type: mtype_dense
 
    A set of atomic calculations are performed in order to construct
    the Grand Canonical Basis set (see PCCP 11, 5805-5813 (2009))
    as well as JCTC 5, 1027 (2009)
    This is done in order to use the TRILEVEL starting guess and 
    perform orbital localization
    This is Level 1 of the TRILEVEL starting guess and is performed per default.
    The use of the Grand Canonical Basis can be deactivated using .NOGCBASIS
    under the **GENERAL section. This is NOT recommended if you do TRILEVEL 
    or orbital localization.
 
 
    Level 1 atomic calculation on cc-pVTZ Charge   2
    ================================================
  
    >>>  CPU Time used in OVERLAP is   0.00 seconds
    >>> wall Time used in OVERLAP is   0.00 seconds
    >>>  CPU Time used in NucElec is   0.00 seconds
    >>> wall Time used in NucElec is   0.00 seconds
    >>>  CPU Time used in Kinetic is   0.00 seconds
    >>> wall Time used in Kinetic is   0.00 seconds
    >>>  CPU Time used in reg-Jengine is   0.01 seconds
    >>> wall Time used in reg-Jengine is   0.01 seconds
    >>>  CPU Time used in st-Kbuild is   0.02 seconds
    >>> wall Time used in st-Kbuild is   0.02 seconds
    ******************************************************************************** ###
     it            E(SCF)          dE(SCF)    exit        alpha RHshift  AO gradient ###
    ******************************************************************************** ###
      1     -2.7520963228    0.00000000000    0.00      0.00000    0.00    1.158E+00 ###
    >>>  CPU Time used in reg-Jengine is   0.01 seconds
    >>> wall Time used in reg-Jengine is   0.01 seconds
    >>>  CPU Time used in st-Kbuild is   0.02 seconds
    >>> wall Time used in st-Kbuild is   0.02 seconds
      2     -2.8597756607   -0.10767933784    0.00      0.00000    0.00    1.408E-01 ###
    >>>  CPU Time used in reg-Jengine is   0.01 seconds
    >>> wall Time used in reg-Jengine is   0.01 seconds
    >>>  CPU Time used in st-Kbuild is   0.02 seconds
    >>> wall Time used in st-Kbuild is   0.02 seconds
      3     -2.8611457084   -0.00137004771   -1.00      0.00000    0.00    7.963E-03 ###
    >>>  CPU Time used in reg-Jengine is   0.01 seconds
    >>> wall Time used in reg-Jengine is   0.01 seconds
    >>>  CPU Time used in st-Kbuild is   0.02 seconds
    >>> wall Time used in st-Kbuild is   0.02 seconds
      4     -2.8611533422   -0.00000763384   -1.00      0.00000    0.00    1.907E-04 ###
 
    Matrix type: mtype_dense
    >>>  CPU Time used in *ATOM is   0.13 seconds
    >>> wall Time used in *ATOM is   0.13 seconds
    >>>  CPU Time used in MBIE IntDriver is   0.01 seconds
    >>> wall Time used in MBIE IntDriver is   0.01 seconds
    >>>  CPU Time used in CS12_INPUT-Mole is   0.00 seconds
    >>> wall Time used in CS12_INPUT-Mole is   0.00 seconds
    >>>  CPU Time used in II_precalc_ScreenMat is   0.14 seconds
    >>> wall Time used in II_precalc_ScreenMat is   0.14 seconds
    >>>  CPU Time used in OVERLAP is   0.02 seconds
    >>> wall Time used in OVERLAP is   0.02 seconds
    >>>  CPU Time used in *S is   0.02 seconds
    >>> wall Time used in *S is   0.02 seconds
    >>>  CPU Time used in NucElec is   0.04 seconds
    >>> wall Time used in NucElec is   0.04 seconds
    >>>  CPU Time used in Kinetic is   0.02 seconds
    >>> wall Time used in Kinetic is   0.02 seconds
    >>>  CPU Time used in *H1 is   0.06 seconds
    >>> wall Time used in *H1 is   0.06 seconds
 
    First density: Atoms in molecule guess
 
    >>>  CPU Time used in reg-Jengine is   0.14 seconds
    >>> wall Time used in reg-Jengine is   0.14 seconds
    >>>  CPU Time used in LINK-Kbuild is   0.06 seconds
    >>> wall Time used in LINK-Kbuild is   0.06 seconds
    Iteration 0 energy:      -28.611533422253
 
    >>>  CPU Time used in *START is   0.20 seconds
    >>> wall Time used in *START is   0.20 seconds
    Preparing to do S^1/2 decomposition...
    >>>  CPU Time used in LWDIAG is   0.00 seconds
    >>> wall Time used in LWDIAG is   0.00 seconds
  
    Relative convergence threshold for solver:  1.00000000E-02
    SCF Convergence criteria for gradient norm:  1.00000000E-01
    >>>  CPU Time used in INIT SCF is   0.00 seconds
    >>> wall Time used in INIT SCF is   0.00 seconds
    >>>  CPU Time used in reg-Jengine is   0.13 seconds
    >>> wall Time used in reg-Jengine is   0.13 seconds
    >>>  CPU Time used in LINK-Kbuild is   0.09 seconds
    >>> wall Time used in LINK-Kbuild is   0.09 seconds
    >>>  CPU Time used in FCK_FO is   0.23 seconds
    >>> wall Time used in FCK_FO is   0.23 seconds
    >>>  CPU Time used in G_GRAD is   0.00 seconds
    >>> wall Time used in G_GRAD is   0.00 seconds
    ******************************************************************************** ###
     it            E(SCF)          dE(SCF)    exit        alpha RHshift OAO gradient ###
    ******************************************************************************** ###
      1    -28.6115334561    0.00000000000    0.00      0.00000    0.00    2.247E-05 ###
    SCF converged in      1 iterations
    >>>  CPU Time used in SCF iterations is   0.23 seconds
    >>> wall Time used in SCF iterations is   0.23 seconds
 
    Total no. of matmuls in SCF optimization:         43
 
    Number of occupied orbitals:      10
    Number of virtual orbitals:      130
 
    Number of occupied orbital energies to be found:       1
    Number of virtual orbital energies to be found:        1
 
 
    Calculation of HOMO-LUMO gap
    ============================
 
    Calculation of occupied orbital energies converged in     4 iterations!
 
    Calculation of virtual orbital energies converged in     2 iterations!
 
     E(LUMO):                         1.500516 au
    -E(HOMO):                        -0.917617 au
    -------------------------------------------------
     HOMO-LUMO Gap (iteratively):     2.418133 au
 
    >>>  CPU Time used in HL GAP is   0.63 seconds
    >>> wall Time used in HL GAP is   0.63 seconds
 
    ********************************************************
     it       dE(HF)          exit   RHshift    RHinfo 
    ********************************************************
      1    0.00000000000    0.0000    0.0000    0.0000000
 
    ======================================================================
                       LINSCF ITERATIONS:
      It.nr.               Energy                 OAO Gradient norm
    ======================================================================
        1           -28.61153345610387077613      0.224704302292907D-04
 
          SCF converged !!!! 
             >>> Final SCF results from LSDALTON <<<
 
 
          Final HF energy:                       -28.611533456104
          Nuclear repulsion:                       5.541376318900
          Electronic energy:                     -34.152909775003
 
    Total no. of matmuls used:                        59
    Total no. of Fock/KS matrix evaluations:           2
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
                      Memory statistics          
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
      Allocated memory (TOTAL):         0 byte Should be zero, otherwise a leakage is present
 
      Max allocated memory, TOTAL                          7.276 MB
      Max allocated memory, type(matrix)                   2.823 MB
      Max allocated memory, real(realk)                    7.239 MB
      Max allocated memory, integer                       99.756 kB
      Max allocated memory, logical                        6.444 kB
      Max allocated memory, character                      3.680 kB
      Max allocated memory, AOBATCH                      106.496 kB
      Max allocated memory, ODBATCH                       20.768 kB
      Max allocated memory, LSAOTENSOR                    84.800 kB
      Max allocated memory, SLSAOTENSOR                   19.456 kB
      Max allocated memory, ATOMTYPEITEM                 112.528 kB
      Max allocated memory, ATOMITEM                       5.632 kB
      Max allocated memory, LSMATRIX                       3.600 kB
      Max allocated memory, OverlapT                     464.128 kB
      Max allocated memory, linkshell                      3.648 kB
      Max allocated memory, integrand                    580.608 kB
      Max allocated memory, integralitem                   2.304 MB
      Max allocated memory, IntWork                      171.656 kB
      Max allocated memory, Overlap                        6.343 MB
      Max allocated memory, ODitem                        15.104 kB
      Max allocated memory, LStensor                     465.194 kB
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
 
    Allocated MPI memory a cross all slaves:          0 byte  - Should be zero - otherwise a leakage is present
    This is a non MPI calculation so naturally no memory is allocated on slaves!
      ===============================================================
      Overall Timings for the Matrix Operations and Integral routines
               in Level 1 The Atomic Calculation                     
      Matrix Operation   # calls          CPU Time         Wall Time
      ===============================================================
   L1 mat_trans                9            0.0000            0.0000
   L1 mat_assign              12            0.0000            0.0000
   L1 mat_copy                 4            0.0000            0.0000
   L1 mat_mul                 12            0.0000            0.0000
   L1 mat_add                 13            0.0000            0.0000
   L1 mat_daxpy               19            0.0000            0.0000
   L1 mat_dotproduct          11            0.0000            0.0000
   L1 mat_sqnorm2             23            0.0000            0.0000
   L1 mat_add_block           30            0.0000            0.0000
   L1 mat_zero                11            0.0000            0.0000
   L1  mat_inv                 1            0.0020            0.0020
   L1 mat_write_to_disk        1            0.0010            0.0010
      ==============================================================
   L1 TOTAL MAT                             0.0010            0.0010
      ==============================================================
   L1     II_get_overlap       1            0.0020            0.0020
   L1     II_get_kinetic       1            0.0010            0.0010
   L1   II_get_nucel_mat       1            0.0030            0.0020
   L1 II_GET_EXCHANGE_MA       4            0.0840            0.0840
   L1 II_get_coulomb_mat       4            0.0380            0.0390
      ==============================================================
   L1 TOTAL INTEGRAL                        0.1300            0.1300
      ==============================================================
      ===============================================================
      Overall Timings for the Matrix Operations and Integral routines
               in Level 3 The Full molecular Calculation             
      Matrix Operation   # calls          CPU Time         Wall Time
      ===============================================================
   L3 mat_to_full              4            0.0000            0.0000
   L3 mat_trans                8            0.0040            0.0060
   L3 mat_assign               5            0.0000            0.0000
   L3 mat_mul                 46            0.0140            0.0130
   L3 mat_add                  8            0.0000            0.0010
   L3 mat_daxpy                8            0.0010            0.0000
   L3 mat_dotproduct           4            0.0000            0.0000
   L3 mat_sqnorm2              9            0.0010            0.0000
   L3 mat_diag_f               1            0.0030            0.0030
   L3 mat_scal                 2            0.0000            0.0000
   L3 mat_zero                 7            0.0000            0.0020
   L3 mat_density_from_o       1            0.0000            0.0000
   L3 mat_scal_dia_vec         2            0.0000            0.0000
   L3 mat_dsyev                1            0.0020            0.0010
   L3 mat_identity             2            0.0000            0.0020
   L3 mat_write_to_disk        3            0.0030            0.0030
      ==============================================================
   L3 TOTAL MAT                             0.0260            0.0280
      ==============================================================
   L3     II_get_overlap       1            0.0200            0.0190
   L3     II_get_kinetic       1            0.0240            0.0240
   L3   II_get_nucel_mat       1            0.0350            0.0360
   L3 II_precalc_ScreenM       1            0.1400            0.1410
   L3      II_get_nucpot       1            0.0000            0.0010
   L3 II_GET_EXCHANGE_MA       2            0.1540            0.1550
   L3 II_get_coulomb_mat       2            0.2690            0.2690
   L3           homolumo       1            0.6310            0.6330
      ==============================================================
   L3 TOTAL INTEGRAL                        1.2760            1.2820
      ==============================================================
    >>>  CPU Time used in LSDALTON is   1.43 seconds
    >>> wall Time used in LSDALTON is   1.44 seconds

    End simulation
     Date and time (Linux)  : Thu Feb 18 14:15:21 2016
     Host name              : localhost.localdomain                   
