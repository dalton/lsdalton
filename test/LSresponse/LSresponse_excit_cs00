#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > LSresponse_excit_cs00.info <<'%EOF%'
   LSresponse_excit_cs00 
   -------------
   Molecule:         He
   Wave Function:    DFT (B3LYP) / Huckel
   Test Purpose:     Test MCD: Aterm, Bterm and Damped MCD. 
                     
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > LSresponse_excit_cs00.mol <<'%EOF%'
BASIS                                   
aug-cc-pVDZ                             
==============-segmentet                
=============                           
Atomtypes=3 Charge=0 Nosymmetry Angstrom                    
Charge=2.0 Atoms=1                                          
He   0.0000   0.0000   0.0000   
Charge=1.0 Atoms=1   pointcharge                                     
He   2.0000   0.0000   0.0000   
Charge=1.0 Atoms=1   pointcharge                                     
He   0.0000   0.0000   1.8000   
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > LSresponse_excit_cs00.dal <<'%EOF%'
**GENERAL
.PSFUN
**WAVE FUNCTIONS
.DFT
B3LYP
*DFT INPUT
.CS00
.CS00 ZND1
0.2332d0
.CS00 ZND2
0.0116d0
.ULTRAF
*DENSOPT
.RH
.DIIS
.CONVTHR
1.d-9
.START
ATOMS
**RESPONSE
*LINRSP
.NEXCIT
3
*SOLVER
.CONVTHR
1.0D-9
*END OF INPUT
%EOF%
#######################################################################

 

#######################################################################

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >LSresponse_excit_cs00.check
cat >>LSresponse_excit_cs00.check <<'%EOF%'

log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# ENERGY                                         
CRIT1=`$GREP "This is a DFT calculation of type: B3LYP" $log | wc -l`  
CRIT2=`$GREP "Final DFT energy\: *\-3\.218272123" $log | wc -l`       
TEST[1]=`expr   $CRIT1 + $CRIT2`                        
CTRL[1]=2                                                           
ERROR[1]="DFT ENERGY NOT CORRECT -"  

# A term
CRIT1=`$GREP "1 * 0\.35983" $log | wc -l` 
CRIT2=`$GREP "2 * 0\.39218" $log | wc -l` 
CRIT3=`$GREP "3 * 0\.42605" $log | wc -l` 
TEST[2]=`expr   $CRIT1 + $CRIT2 + $CRIT3`     
CTRL[2]=3   
ERROR[2]="Excitation energies not correct -"     

# Memory test
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[3]=`expr  $CRIT1`
CTRL[3]=1
ERROR[3]="Memory leak -"

PASSED=1
for i in 1 2 3
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM
  exit 1
fi

%EOF%
#######################################################################
