#!/usr/bin/env bash
################################################################################
#                                                                              #
# TEST-script for LSDALTON                                                     #
#                                                                              #
################################################################################
#
# location of default lsdalton script
LSDALTON_script="$PWD/../../../gfortran_debug/lsdalton"

# radovan: this is to figure out the location of this script
# http://stackoverflow.com/questions/59895/can-a-bash-script-tell-what-directory-its-stored-in
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
    SCRIPT_DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
    SOURCE="$(readlink "$SOURCE")"
    # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
    [[ $SOURCE != /* ]] && SOURCE="$SCRIPT_DIR/$SOURCE"
done
SCRIPT_DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

#########################################################################
# function usage(): print usage information
#########################################################################
usage() {
 cat <<%EOF%
Usage: $0 [-h|-help|--help] [-keep] [-reftest] [-benchmark]
            [-param "option list"] [-lsdalton script] [-log logfile] testcase1 [testcase2] ...

       -h | -help | --help  : show this help description
       -keep                : keep *.mol, *.dal, *.check and *.log files
       -geotest             : standalone geometry optimization run
       -reftest             : test reference file(s); do not run any calculations
       -benchmark           : print CPU time for each testcase as part of the summary
       -param "option list" : pass "option list" to lsdalton-script
       -gridgen             : use gridgen
       -lsdalton script     : use script instead of the default
                              lsdalton-script "$LSDALTON_script"
       -log logfile         : write output into logfile instead of the "TESTLOG"
%EOF%
exit 1
}

#########################################################################
# function myprint(string): print to stdout and $listing
#########################################################################
myprint(){
 echo "$1";
 echo "$1" >> $listing;
 echo "$1" >> $SCRIPT_DIR/$listing;
}

#########################################################################
#########################################################################
# start with real work:
#########################################################################
#########################################################################

#########################################################################
# set defaults and evaluate parameters
#########################################################################
TESTparams="$0 $*"
help=""
geotest="false"
gridgentest="false"
keep="false"
paramlist=""
reftest=""
benchmark=""
listing="TESTLOG"
emptystring="                                "
answer_yes=

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

while echo $1 | egrep -q '^-'; do
   case $1 in
     "-h" | "-help" | "--help" ) help="true" ; break;;
     "-lsdalton" )  shift; LSDALTON_script=$1; shift;;
     "-log" ) shift; listing=$1; shift;;
     "-geotest" ) geotest="true"; shift;;
     "-gridgen" ) gridgentest="true"; shift;;
     "-keep" ) keep="true"; shift;;
     -param* ) shift; paramlist="$1"; shift;;
     "-reftest" ) reftest="true"; shift;;
     "-y") answer_yes=true; shift;;
     "-benchmark" ) benchmark="true"; shift;;
     * ) echo; echo "***** SORRY ***** option $1 is not known"; echo; usage;;
   esac
done
if [ $help ]; then
  usage
fi
if [ ! -x $LSDALTON_script ]; then
   echo ' '
   echo '----> ERROR, the "lsdalton" job script '
   echo    '"'$LSDALTON_script'"'
   echo '  does not exist or is not executable.'
   echo 'The person who installed lsdalton has probably selected'
   echo '  another place for the "lsdalton" job script.'
   echo 'Please modify the LSDALTON_script variable in this script (i.e. '$0')'
   echo '  to point to that place.    Alternatively, you may'
   echo '  specify where the lsdalton script is with the -lsdalton option'
   echo ' '
#  usage
   exit 1
fi


#########################################################################
# set list of test cases:
#########################################################################
testcase=$*
if   [ "$testcase" = "" ]; then
    echo '    ***********************************'
    echo '    * ERROR: No testcase(s) specified *'
    echo '    ***********************************'
    usage
fi


#########################################################################
# create a unique directory for test output /hjaaj
# nonportable characters in the directory name removed. /pawsa
#########################################################################
testdir="`date +'%Y-%m-%dT%H_%M'`-testjob-pid-$$"
while [ -e $testdir ]; do
    testdir=${testdir}x
done
mkdir -p testruns/$testdir
cd testruns/$testdir || exit 1

#########################################################################
# check files for test listing:
#########################################################################
if [  -s $listing -a "$answer_yes" = '' ]; then
   echo "$listing already exists... should it be deleted first? (y/n)"
   read answer
   if [ "$answer" = "yes"  -o  "$answer" = "y" -o "$answer" = "Y" ]; then
       rm -f $listing
   fi
fi
if [  -s $SCRIPT_DIR/$listing ]; then
   rm -f $SCRIPT_DIR/$listing
fi

myprint "#####################################################################"
myprint "                        LSDALTON test suite"
myprint "#####################################################################"
myprint "invoked with          : $TESTparams"
myprint "date and time         : `date`"
myprint "lsdalton script       : $LSDALTON_script"
myprint "parameter list passed : $paramlist"
myprint "test job directory    : testruns/$testdir"
myprint "test listing          : $listing"
myprint "test cases            : $testcase"

#########################################################################
# loop over test cases:
#########################################################################
passedall="ALL TESTS ENDED PROPERLY!"
problems=""
exitcode=0
numresult=0
num_reftests_not_found=0
for fitem in ${testcase}
do
  trouble=0
  myprint "###########################################################"
  myprint "`date`"
  myprint "start now with test $fitem:"
  myprint "-----------------------------------------------------------"
  sh $SCRIPT_DIR/$fitem || exit 1
  item=`basename $fitem`
  if [ -f $item.info ]; then
    cat $item.info | tee -a $listing | tee -a $SCRIPT_DIR/$listing
  fi
# Check if this is a multistep job
  numsteps=1
  if [ -r ./$item'__'1.dal ]; then
      multi=1
      numsteps=`ls ./$item\_\_[0-9].dal | wc -l`
      myprint "   This is a multi-step job consisting of $numsteps steps"
      rm -f ./$item'__'[0-9]'_'$item.log
      chmod +x ./$item'__'[0-9].check
  else
      multi=0
      rm -f ./$item.log
      chmod +x ./$item.check
  fi
# Multi-step tests loop over the different jobsteps, regular tests just
# go through this once
  step=1
  while [ "$step" -le "$numsteps" ]
  do
      if [ "$multi" -eq 1 ]; then
          myprint "-----------------------------------------------------------"
          myprint "  Start $item step $step of $numsteps :"
          myprint "-----------------------------------------------------------"
          molfile=$item
	  cp $item'__'$step'.dal' $item'.dal'
          dalfile=$item
          logfile=./$item'__'$step'_'$item.log
          extlogfile=./$item'__'$step'_'$item.ext.log
          gridlogfile=./$item'__'$step'_'$item.grid.log
          checkfile=./$item'__'$step.check
          reffile=../ref/$item'__'$step.*ref
          moldenfile=./$item'__'$step'_'$item.molden
      else
          molfile=$item
          dalfile=$item
          logfile=./$item.log
          extlogfile=./$item.ext.log
          gridlogfile=./$item.grid.log
          checkfile=./$item.check
          reffile=../ref/$item.*ref
          moldenfile=./$item.molden
      fi
# If it's a reftest, no calculation is performed
      if [ "$reftest" = "true" ]; then
          myprint ""
          myprint "evaluate reference output file $reffile:"
          myprint "-----------------------------------------------------------"
          compressed="false"
          if [ ! -r $reffile ]; then
              if [ -r $reffile.gz ]; then
                  compressed="true"
                  gunzip -f $reffile.gz
              fi
          fi
              checkout=`$checkfile $reffile | tee -a $listing` | tee -a $SCRIPT_DIR/$listing
          if [ "$compressed" = "true" ]; then
              gzip -f --best $reffile
          fi
      else
	  if [ "$geotest" = "true" ]; then
	      $LSDALTON_script -geo1 $paramlist -ext log $dalfile $molfile | \
		  grep -v '\*\*\**' | \
		  grep -v 'OUTPUT FROM' | grep -v 'Version' | grep -v 'PID' | \
		  grep -v '^$' | tee -a $listing | tee -a $SCRIPT_DIR/$listing
	      # do it a second time this time with standlone version
	      $LSDALTON_script -geo2 $paramlist -ext log $dalfile $molfile | \
		  grep -v '\*\*\**' | \
		  grep -v 'OUTPUT FROM' | grep -v 'Version' | grep -v 'PID' | \
		  grep -v '^$' | tee -a $listing | tee -a $SCRIPT_DIR/$listing
	  else
              if [ "$step" -eq "$numsteps" ]; then
		  $LSDALTON_script $paramlist -ext log $dalfile $molfile | \
		      grep -v '\*\*\**' | \
		      grep -v 'OUTPUT FROM' | grep -v 'Version' | grep -v 'PID' | \
		      grep -v '^$' | tee -a $listing | tee -a $SCRIPT_DIR/$listing
              else
		  $LSDALTON_script -D $paramlist -ext log $dalfile $molfile | \
		      grep -v '\*\*\**' | \
		      grep -v 'OUTPUT FROM' | grep -v 'Version' | grep -v 'PID' | \
		      grep -v '^$' | tee -a $listing | tee -a $SCRIPT_DIR/$listing
              fi
	  fi
          if [ "$multi" -eq 1 ]; then
	      cp $molfile'.log' $logfile
          fi
          myprint ""
          myprint "evaluate output file $logfile:"
          myprint "-----------------------------------------------------------"
	  if [ "$gridgentest" = "true" ]; then
	      checkout=`$checkfile $gridlogfile | tee -a $listing | tee -a $SCRIPT_DIR/$listing`
	  else
	      checkout=`$checkfile $logfile | tee -a $listing | tee -a $SCRIPT_DIR/$listing`
	  fi
          if [ "$benchmark" = "true" ]; then
              if [ `$GREP "CPU Time used in LSDALTON  *is" $logfile | wc -l` = 1 ]; then
                  CPU_usage=`$GREP "CPU Time used in LSDALTON  *is" $logfile | sed s/">* *CPU Time used in LSDALTON *is"/""/`
              else
                  CPU_usage="N/A"
              fi
          fi
      fi
      if [ "$benchmark" = "true" ]; then
          numresult=`expr $numresult \+ 1`
          if [ $step -eq 1 ]; then
              testlist[$numresult]="$item"
          else
              testlist[$numresult]="...step$step"
          fi
          timelist[$numresult]="$CPU_usage"
      fi
      echo $checkout
      passed=`echo $checkout | grep "TEST ENDED PROPERLY"`
      if [ -z "$passed" ]; then
        trouble=`expr $trouble \+ 1`
        passedall="THERE IS A PROBLEM IN TEST CASE(S)"
        if [ "$trouble" -eq 1 ]; then
            problems="$problems $item"
        fi
        exitcode=1
      elif [ "$keep" = "false" ]; then

        rm -f $dalfile.dal $dalfile.BAS $checkfile $logfile $extlogfile $item.info $dalfile.tar.gz
#     else everything OK and -keep defined
      fi
    step=`expr $step \+ 1`
  done
  if [ "$trouble" -eq 0 ]; then
      if [ "$keep" = "false" ]; then
          rm -f $molfile.mol
      fi
  fi
done

#########################################################################
# final result:
#########################################################################

myprint ""
myprint "#####################################################################"
myprint "                              Summary"
myprint "#####################################################################"
myprint ""

if [ "$benchmark" = "true" ]; then
    if [ "$numresult" -gt 0  ]; then
      ind=1
      while [ "$ind" -le "$numresult" ]; do
        awk "END{printf \"%32s %s\\n\", \"${testlist[$ind]}\",\"${timelist[$ind]}\"}"</dev/null
        awk "END{printf \"%32s %s\\n\", \"${testlist[$ind]}\",\"${timelist[$ind]}\"}"</dev/null >> $listing
        awk "END{printf \"%32s %s\\n\", \"${testlist[$ind]}\",\"${timelist[$ind]}\"}"</dev/null >> $SCRIPT_DIR/$listing
        ind=`expr $ind \+ 1`
      done
      myprint ""
    fi
fi

if [ "$reftest" = "true" ]; then
   myprint " >>>>> THIS IS A TEST OF THE REFERENCE OUTPUTS >>>>>"
   myprint ""
fi

n=0
for problem in ${problems}
do
  n=`expr $n \+ 1`
done
if [ "$n" -eq 0 ]; then
    myprint "$passedall"
else
    myprint "$passedall $n TEST CASE(S)"
    for problem in ${problems}
    do
      myprint "$problem"
    done
fi

myprint "date and time         : `date`"

exit $exitcode
