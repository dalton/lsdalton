  
     ******************************************************************    
     **********  LSDalton - An electronic structure program  **********    
     ******************************************************************    
  
  
    This is output from LSDalton 1.0
  
  
     IF RESULTS OBTAINED WITH THIS CODE ARE PUBLISHED,
     THE FOLLOWING PAPER SHOULD BE CITED:
     
     K. Aidas, C. Angeli, K. L. Bak, V. Bakken, R. Bast,
     L. Boman, O. Christiansen, R. Cimiraglia, S. Coriani,
     P. Dahle, E. K. Dalskov, U. Ekstroem, T. Enevoldsen,
     J. J. Eriksen, P. Ettenhuber, B. Fernandez,
     L. Ferrighi, H. Fliegl, L. Frediani, K. Hald,
     A. Halkier, C. Haettig, H. Heiberg,
     T. Helgaker, A. C. Hennum, H. Hettema,
     E. Hjertenaes, S. Hoest, I.-M. Hoeyvik,
     M. F. Iozzi, B. Jansik, H. J. Aa. Jensen,
     D. Jonsson, P. Joergensen, J. Kauczor,
     S. Kirpekar, T. Kjaergaard, W. Klopper,
     S. Knecht, R. Kobayashi, H. Koch, J. Kongsted,
     A. Krapp, K. Kristensen, A. Ligabue,
     O. B. Lutnaes, J. I. Melo, K. V. Mikkelsen, R. H. Myhre,
     C. Neiss, C. B. Nielsen, P. Norman,
     J. Olsen, J. M. H. Olsen, A. Osted,
     M. J. Packer, F. Pawlowski, T. B. Pedersen,
     P. F. Provasi, S. Reine, Z. Rinkevicius,
     T. A. Ruden, K. Ruud, V. Rybkin,
     P. Salek, C. C. M. Samson, A. Sanchez de Meras,
     T. Saue, S. P. A. Sauer, B. Schimmelpfennig,
     K. Sneskov, A. H. Steindal, K. O. Sylvester-Hvid,
     P. R. Taylor, A. M. Teale, E. I. Tellgren,
     D. P. Tew, A. J. Thorvaldsen, L. Thoegersen,
     O. Vahtras, M. A. Watson, D. J. D. Wilson,
     M. Ziolkowski, and H. AAgren,
     "The Dalton quantum chemistry program system",
     WIREs Comput. Mol. Sci. (doi: 10.1002/wcms.1172)
  
  
                                               
    LSDalton authors in alphabetical order (main contribution(s) in parenthesis)
    ----------------------------------------------------------------------------
    Vebjoern Bakken,        University of Oslo,        Norway   (Geometry optimizer)
    Radovan Bast,           UiT The Arctic University of Norway (CMake, Testing)
    Pablo Baudin,           Aarhus University,         Denmark  (DEC,CCSD)
    Sonia Coriani,          University of Trieste,     Italy    (Response)
    Patrick Ettenhuber,     Aarhus University,         Denmark  (CCSD)
    Janus Juul Eriksen,     Aarhus University,         Denmark  (CCSD(T), DEC)
    Trygve Helgaker,        University of Oslo,        Norway   (Supervision)
    Stinne Hoest,           Aarhus University,         Denmark  (SCF optimization)
    Ida-Marie Hoeyvik,      Aarhus University,         Denmark  (Orbital localization, SCF opt)
    Robert Izsak,           University of Oslo,        Norway   (ADMM)
    Branislav Jansik,       Aarhus University,         Denmark  (Trilevel, orbital localization)
    Poul Joergensen,        Aarhus University,         Denmark  (Supervision)
    Joanna Kauczor,         Aarhus University,         Denmark  (Response solver)
    Thomas Kjaergaard,      Aarhus University,         Denmark  (RSP, INT, DEC, SCF, Readin, MPI, MAT)
    Andreas Krapp,          University of Oslo,        Norway   (FMM, dispersion-corrected DFT)
    Kasper Kristensen,      Aarhus University,         Denmark  (Response, DEC)
    Patrick Merlot,         University of Oslo,        Norway   (ADMM)
    Cecilie Nygaard,        Aarhus University,         Denmark  (SOEO)
    Jeppe Olsen,            Aarhus University,         Denmark  (Supervision)
    Simen Reine,            University of Oslo,        Norway   (Integrals, geometry optimizer)
    Vladimir Rybkin,        University of Oslo,        Norway   (Geometry optimizer, dynamics)
    Pawel Salek,            KTH Stockholm,             Sweden   (FMM, DFT functionals)
    Andrew M. Teale,        University of Nottingham   England  (E-coefficients)
    Erik Tellgren,          University of Oslo,        Norway   (Density fitting, E-coefficients)
    Andreas J. Thorvaldsen, University of Tromsoe,     Norway   (Response)
    Lea Thoegersen,         Aarhus University,         Denmark  (SCF optimization)
    Mark Watson,            University of Oslo,        Norway   (FMM)
    Marcin Ziolkowski,      Aarhus University,         Denmark  (DEC)
  
  
     NOTE:
      
     This is an experimental code for the evaluation of molecular
     energies and properties using various electronic structure models.
     The authors accept no responsibility for the performance of the code or
     for the correctness of the results.
      
     The code (in whole or part) is provided under a licence and
     is not to be reproduced for further distribution without
     the written permission of the authors or their representatives.
      
     See the home page "http://daltonprogram.org"
     for further information.
  
  
     Who compiled             | pbaudin
     Host                     | fe2
     System                   | Linux-2.6.32-504.16.2.el6.x86_64
     CMake generator          | Unix Makefiles
     Processor                | x86_64
     64-bit integers          | OFF
     MPI                      | OFF
     Fortran compiler         | /com/pgi/linux86-64/2015/bin/pgf90
     Fortran compiler version | pgf90 15.5-0 64-bit target on x86-64 Linux -tp san
                              | dybridge
     C compiler               | /com/pgi/linux86-64/2015/bin/pgcc
     C compiler version       | pgcc 15.5-0 64-bit target on x86-64 Linux -tp sand
                              | ybridge
     C++ compiler             | /com/pgi/linux86-64/2015/bin/pgc++
     C++ compiler version     | unknown
     Static linking           | OFF
     Last Git revision        | 9759e9efbe9a0859e73a36a55d2a64803dfc34c6
     Git branch               | master
     Configuration time       | 2016-11-24 16:08:39.594707
  

         Start simulation
     Date and time (Linux)  : Thu Nov 24 16:43:04 2016
     Host name              : fe2                                     
                      
    ---------------------------------------------------
             PRINTING THE MOLECULE.INP FILE 
    ---------------------------------------------------
                      
    BASIS                                  
    3-21G Aux=cc-pVDZ-RI                   
                                           
                                                                                                                           
                      
    ---------------------------------------------------
             PRINTING THE LSDALTON.INP FILE 
    ---------------------------------------------------
                      
    **WAVE FUNCTIONS
    .HF
    *DENSOPT
    .RH
    .DIIS
    .START
    ATOMS
    .LCM
    .CONVTHR
    1.0d-10
    .MAXIT
    1000
    **CC
    .RICC2
    .MEMORY
    1.0
    *CCRESPONSE
    .NEXCIT
    3
    .USE_LEFT_TRANSFO
    *END OF INPUT
 
 
 WARNING This basis format violate the old dalton basis format
 Which consist of 1 Exponent F16.9 and up to 6 contraction
 coefficients on the first line followed by a up to 7 contraction coefficients
 on the following lines until the full number of contraction coefficients are given
 We will try to this basis set, but this code is not very well testet. TK
 WARNING This basis format violate the old dalton basis format
 Which consist of 1 Exponent F16.9 and up to 6 contraction
 coefficients on the first line followed by a up to 7 contraction coefficients
 on the following lines until the full number of contraction coefficients are given
 We will try to this basis set, but this code is not very well testet. TK
 WARNING This basis format violate the old dalton basis format
 Which consist of 1 Exponent F16.9 and up to 6 contraction
 coefficients on the first line followed by a up to 7 contraction coefficients
 on the following lines until the full number of contraction coefficients are given
 We will try to this basis set, but this code is not very well testet. TK
 WARNING This basis format violate the old dalton basis format
 Which consist of 1 Exponent F16.9 and up to 6 contraction
 coefficients on the first line followed by a up to 7 contraction coefficients
 on the following lines until the full number of contraction coefficients are given
 We will try to this basis set, but this code is not very well testet. TK
 WARNING This basis format violate the old dalton basis format
 Which consist of 1 Exponent F16.9 and up to 6 contraction
 coefficients on the first line followed by a up to 7 contraction coefficients
 on the following lines until the full number of contraction coefficients are given
 We will try to this basis set, but this code is not very well testet. TK
 WARNING This basis format violate the old dalton basis format
 Which consist of 1 Exponent F16.9 and up to 6 contraction
 coefficients on the first line followed by a up to 7 contraction coefficients
 on the following lines until the full number of contraction coefficients are given
 We will try to this basis set, but this code is not very well testet. TK
 WARNING This basis format violate the old dalton basis format
 Which consist of 1 Exponent F16.9 and up to 6 contraction
 coefficients on the first line followed by a up to 7 contraction coefficients
 on the following lines until the full number of contraction coefficients are given
 We will try to this basis set, but this code is not very well testet. TK
                      
    Atoms and basis sets
      Total number of atoms        :      2
      THE  REGULAR   is on R =   1
    ---------------------------------------------------------------------
      atom label  charge basisset                prim     cont   basis
    ---------------------------------------------------------------------
          1 Li     3.000 3-21G                     15        9 [6s3p|3s2p]                                  
          2 H      1.000 3-21G                      3        2 [3s|2s]                                      
    ---------------------------------------------------------------------
    total          4                               18       11
    ---------------------------------------------------------------------
                      
                      
    Atoms and basis sets
      Total number of atoms        :      2
      THE  AUXILIARY is on R =   2
    ---------------------------------------------------------------------
      atom label  charge basisset                prim     cont   basis
    ---------------------------------------------------------------------
          1 Li     3.000 cc-pVDZ-RI                56       56 [7s5p4d2f|7s5p4d2f]                          
          2 H      1.000 cc-pVDZ-RI                14       14 [3s2p1d|3s2p1d]                              
    ---------------------------------------------------------------------
    total          4                               70       70
    ---------------------------------------------------------------------
                      
                      
    Basic Molecule/Basis information
    --------------------------------------------------------------------
      Molecular Charge                   :    0.0000
      Regular basisfunctions             :       11
      Auxiliary basisfunctions           :       70
      Primitive Regular basisfunctions   :       18
      Primitive Auxiliary basisfunctions :       70
    --------------------------------------------------------------------
                      
    Configuration:
    ==============
    This is an OpenMP calculation using   3 threads.
    This is a serial calculation (no MPI)
 
    Density subspace min. method    : DIIS                    
    Density optimization            : Diagonalization                    
 
    Maximum size of Fock/density queue in averaging:   10
 
    Convergence threshold for gradient        :   0.10E-09
     
    We perform the calculation in the Grand Canonical basis
    (see PCCP 2009, 11, 5805-5813)
    To use the standard input basis use .NOGCBASIS
 
    Due to the presence of the keyword (default for correlation)
    .NOGCINTEGRALTRANSFORM
    We transform the input basis to the Grand Canonical
    basis and perform integral evaluation using this basis
     
    The Overall Screening threshold is set to              :  1.0000E-08
    The Screening threshold used for Coulomb               :  1.0000E-10
    The Screening threshold used for Exchange              :  1.0000E-08
    The Screening threshold used for One-electron operators:  1.0000E-15
    The SCF Convergence Criteria is applied to the gradnorm in AO basis
 
    End of configuration!
 
 
    Matrix type: mtype_dense
 
    A set of atomic calculations are performed in order to construct
    the Grand Canonical Basis set (see PCCP 11, 5805-5813 (2009))
    as well as JCTC 5, 1027 (2009)
    This is done in order to use the TRILEVEL starting guess and 
    perform orbital localization
    This is Level 1 of the TRILEVEL starting guess and is performed per default.
    The use of the Grand Canonical Basis can be deactivated using .NOGCBASIS
    under the **GENERAL section. This is NOT recommended if you do TRILEVEL 
    or orbital localization.
 
 
    Level 1 atomic calculation on 3-21G Charge   3
    ================================================
  
    *********************************************************************************** ###
     it            E(SCF)          dE(SCF)    exit        alpha RHshift       RHinfo  AO gradient ###
    *********************************************************************************** ###
      1     -7.2442996115    0.00000000000    0.00      0.00000    0.00    0.0000000    3.588E-01 ###
      2     -7.3244913082   -0.08019169671    0.00      0.00000    0.00    0.0000000    4.467E-02 ###
      3     -7.3268139108   -0.00232260254   -1.00      0.00000    0.00    0.0000000    4.315E-03 ###
      4     -7.3268361221   -0.00002221135   -1.00      0.00000    0.00    0.0000000    9.137E-05 ###
 
    Level 1 atomic calculation on 3-21G Charge   1
    ================================================
  
    *********************************************************************************** ###
     it            E(SCF)          dE(SCF)    exit        alpha RHshift       RHinfo  AO gradient ###
    *********************************************************************************** ###
      1     -0.3362031110    0.00000000000    0.00      0.00000    0.00    0.0000000    1.985E-01 ###
      2     -0.3429646205   -0.00676150947    0.00      0.00000    0.00    0.0000000    1.316E-02 ###
      3     -0.3429945468   -0.00002992637   -1.00      0.00000    0.00    0.0000000    3.649E-05 ###
 
    Matrix type: mtype_dense
 
    First density: Atoms in molecule guess
 
    Iteration 0 energy:       -7.789520710191
 
    Preparing to do S^1/2 decomposition...
  
    Relative convergence threshold for solver:  1.00000000E-02
    SCF Convergence criteria for gradient norm:  1.00000000E-10
    *********************************************************************************** ###
     it            E(SCF)          dE(SCF)    exit        alpha RHshift       RHinfo  AO gradient ###
    *********************************************************************************** ###
      1     -7.7388202583    0.00000000000    0.00      0.00000    0.00    0.0000000    3.201E-01 ###
      2     -7.7443819794   -0.00556172108    0.00      0.00000    0.00    0.0000000    5.951E-02 ###
      3     -7.7449836873   -0.00060170792   -1.00      0.00000    0.00    0.0000000    3.171E-02 ###
      4     -7.7452032410   -0.00021955367   -1.00      0.00000    0.00    0.0000000    2.539E-03 ###
      5     -7.7452048089   -0.00000156790   -1.00      0.00000    0.00    0.0000000    4.005E-04 ###
      6     -7.7452048223   -0.00000001338   -1.00      0.00000    0.00    0.0000000    6.804E-05 ###
      7     -7.7452048226   -0.00000000029   -1.00      0.00000    0.00    0.0000000    4.011E-06 ###
      8     -7.7452048226   -0.00000000000   -1.00      0.00000    0.00    0.0000000    3.423E-07 ###
      9     -7.7452048226   -0.00000000000   -1.00      0.00000    0.00    0.0000000    3.941E-08 ###
     10     -7.7452048226    0.00000000000   -1.00      0.00000    0.00    0.0000000    4.864E-09 ###
     11     -7.7452048226   -0.00000000000   -1.00      0.00000    0.00    0.0000000    8.958E-11 ###
    SCF converged in     11 iterations
    >>>  CPU Time used in SCF iterations is   0.39 seconds
    >>> wall Time used in SCF iterations is   0.39 seconds
 
    Total no. of matmuls in SCF optimization:         69
 
    Number of occupied orbitals:       2
    Number of virtual orbitals:        9
 
    Number of occupied orbital energies to be found:       1
    Number of virtual orbital energies to be found:        1
 
 
    Calculation of HOMO-LUMO gap
    ============================
 
    Calculation of occupied orbital energies converged in     2 iterations!
 
    Calculation of virtual orbital energies converged in     5 iterations!
 
     E(LUMO):                         0.012182 au
    -E(HOMO):                        -0.316002 au
    -------------------------------------------------
     HOMO-LUMO Gap (iteratively):     0.328184 au
 
 
    ********************************************************
     it       dE(HF)          exit   RHshift    RHinfo 
    ********************************************************
      1    0.00000000000    0.0000    0.0000    0.0000000
      2   -0.00556172108    0.0000    0.0000    0.0000000
      3   -0.00060170792   -1.0000    0.0000    0.0000000
      4   -0.00021955367   -1.0000    0.0000    0.0000000
      5   -0.00000156790   -1.0000    0.0000    0.0000000
      6   -0.00000001338   -1.0000    0.0000    0.0000000
      7   -0.00000000029   -1.0000    0.0000    0.0000000
      8   -0.00000000000   -1.0000    0.0000    0.0000000
      9   -0.00000000000   -1.0000    0.0000    0.0000000
     10    0.00000000000   -1.0000    0.0000    0.0000000
     11   -0.00000000000   -1.0000    0.0000    0.0000000
 
    ======================================================================
                       LINSCF ITERATIONS:
      It.nr.               Energy                 AO Gradient norm
    ======================================================================
        1            -7.73882025832409681243      0.320063865555185D+00
        2            -7.74438197940122385887      0.595084625167553D-01
        3            -7.74498368731978636959      0.317084283853157D-01
        4            -7.74520324098666002044      0.253944598363349D-02
        5            -7.74520480888771878369      0.400453123567241D-03
        6            -7.74520482226911255452      0.680378799963947D-04
        7            -7.74520482256311915137      0.401061096621145D-05
        8            -7.74520482256417430733      0.342270113690162D-06
        9            -7.74520482256418851819      0.394089903714531D-07
       10            -7.74520482256418674183      0.486399297479163D-08
       11            -7.74520482256419207090      0.895789033224623D-10
 
          SCF converged !!!! 
             >>> Final SCF results from LSDALTON <<<
 
 
          Final HF energy:                        -7.745204822564
          Nuclear repulsion:                       1.804013210114
          Electronic energy:                      -9.549218032678
 
      %LOC%   
      %LOC%   ********************************************
      %LOC%   *       LEVEL 3 ORBITAL LOCALIZATION       *
      %LOC%   ********************************************
      %LOC%   
      %LOC%  
      %LOC% *******  LOCALIZE     1 CORE ORBITALS ******
      %LOC%  
      %LOC% Too few orbitals to localize...
      %LOC%  
      %LOC% *******  LOCALIZE     1 VALENCE ORBITALS ******
      %LOC%  
      %LOC% Too few orbitals to localize...
      %LOC%  
      %LOC% *******  LOCALIZE     9 VIRTUAL ORBITALS ******
      %LOC%  
      %LOC%   1 sigma_2 =  6.26 mu =  0.0E+00 grd =  4.1E+02 it =  1 TR =  0.50 step = 0.00
      %LOC%   2 sigma_2 =  6.06 mu = -1.4E+04 grd =  4.3E+02 it =  6 TR =  0.50 step = 1.00
      %LOC%   3 sigma_2 =  5.86 mu = -8.0E+03 grd =  1.6E+02 it =  6 TR =  0.50 step = 1.00
      %LOC%   4 sigma_2 =  5.85 mu = -4.2E+03 grd =  2.7E+02 it = 10 TR =  0.50 step = 1.50
      %LOC%   5 sigma_2 =  5.67 mu = -9.6E+02 grd =  3.9E+01 it =  8 TR =  0.50 step = 1.00
      %LOC%   6 sigma_2 =  5.66 mu = -4.6E+03 grd =  1.7E+02 it = 21 TR =  0.50 step = 1.00
      %LOC%   7 sigma_2 =  5.56 mu = -4.6E+03 grd =  1.6E+02 it = 12 TR =  0.50 step = 1.00
      %LOC%   8 sigma_2 =  5.16 mu = -3.6E+03 grd =  1.0E+02 it = 10 TR =  0.50 step = 1.00
      %LOC%   9 sigma_2 =  5.09 mu = -1.8E+03 grd =  6.4E+01 it = 10 TR =  0.50 step = 1.00
      %LOC%  10 sigma_2 =  5.01 mu = -9.2E+02 grd =  2.8E+01 it =  9 TR =  0.50 step = 1.00
    14.47026098999816     
      %LOC%  11 sigma_2 =  4.98 mu = -2.6E+02 grd =  7.2E+00 it = 12 TR =  0.38 step = 0.50
      %LOC%  12 sigma_2 =  4.97 mu = -4.7E+01 grd =  1.0E+00 it = 20 TR =  0.14 step = 0.19
      %LOC%  13 sigma_2 =  4.97 mu = -6.2E+01 grd =  2.7E-01 it = 22 TR =  0.05 step = 0.07
      %LOC%  14 sigma_2 =  4.98 mu = -2.5E+01 grd =  7.6E-01 it = 24 TR =  0.08 step = 0.16
      %LOC%  15 sigma_2 =  4.99 mu = -2.6E+01 grd =  9.4E-01 it = 24 TR =  0.12 step = 0.24
      %LOC%  16 sigma_2 =  4.98 mu = -4.4E+01 grd =  1.5E+00 it = 23 TR =  0.09 step = 0.18
      %LOC%  17 sigma_2 =  4.99 mu = -6.0E+01 grd =  2.8E-01 it = 23 TR =  0.07 step = 0.09
      %LOC%  18 sigma_2 =  4.99 mu = -2.6E+01 grd =  9.9E-01 it = 22 TR =  0.10 step = 0.20
      %LOC%  19 sigma_2 =  4.99 mu = -1.2E+00 grd =  1.5E-01 it = 22 TR =  0.15 step = 0.20
      %LOC%  20 sigma_2 =  4.99 mu =  0.0E+00 grd =  6.6E-03 it = 22 TR =  0.23 step = 0.04
    1088.743724528408     
      %LOC% temporary orbitals written to localized_orbitals.restart
      %LOC%  21 sigma_2 =  4.99 mu =  0.0E+00 grd =  9.0E-06 it = 23 TR =  0.34 step = 0.00
      %LOC% 
      %LOC%  ********* Orbital localization converged ************
      %LOC%  *                                                   *
      %LOC%  * The gradient norm for the orbital localization    *
      %LOC%  * function is below the threshold, and we exit      *
      %LOC%  * the localization procedure.                       *
      %LOC%  *                                                   *
      %LOC%  *  Minimal Hessiag diagonal : -0.00
      %LOC%  *****************************************************
      %LOC% 
 
      %LOC%  %%%%%%%%%%%%%%% CORE LOCALITY  %%%%%%%%%%%%%%%
      %LOC%   Max. sigma_2 and orb.number:    0.68    1
      %LOC%   Max. sigma_4 and orb.number:    0.99    1
      %LOC%   Min. sigma_2 and orb.number:    0.68    1
      %LOC%   Min. sigma_4 and orb.number:    0.99    1
 
 
      %LOC%  %%%%%%%%%%%%%% VALENCE LOCALITY  %%%%%%%%%%%%%%
      %LOC%   Max. sigma_2 and orb.number:    2.18    2
      %LOC%   Max. sigma_4 and orb.number:    2.89    2
      %LOC%   Min. sigma_2 and orb.number:    2.18    2
      %LOC%   Min. sigma_4 and orb.number:    2.89    2
 
 
      %LOC%  %%%%%%%%%%%%%% VIRTUAL LOCALITY  %%%%%%%%%%%%%%
      %LOC%   Max. sigma_2 and orb.number:    4.99    5
      %LOC%   Max. sigma_4 and orb.number:    6.17    8
      %LOC%   Min. sigma_2 and orb.number:    4.64   11
      %LOC%   Min. sigma_4 and orb.number:    5.71    4
 
    >>>  CPU Time used in Orbital Localization is   4.07 seconds
    >>> wall Time used in Orbital Localization is   4.07 seconds
      %LOC%
      %LOC% Localized orbitals written to lcm_orbitals.u
      %LOC%
 

    -- Full molecular info --

    FULL: Overall charge of molecule    :      0

    FULL: Number of electrons           :      4
    FULL: Number of atoms               :      2
    FULL: Number of basis func.         :     11
    FULL: Number of aux. basis func.    :     70
    FULL: Number of core orbitals       :      1
    FULL: Number of valence orbitals    :      1
    FULL: Number of occ. orbitals       :      2
    FULL: Number of occ. alpha orbitals :      0
    FULL: Number of occ. beta  orbitals :      0
    FULL: Number of virt. orbitals      :      9
    FULL: Local memory use type full    :  0.24E-05
    FULL: Distribute matrices           :  F
    FULL: Using frozen-core approx.     :  F
 
    Memory set in input to be:    1.000     GB
 
 
    =============================================================================
         -- Full molecular Coupled-Cluster calculation -- 
    =============================================================================
 

 ================================================ 
              Full molecular driver               
 ================================================ 


--------------------------
  Coupled-cluster energy  
--------------------------

Wave function    = RICC2   
MaxIter          =  100
Num. b.f.        =   11
Num. occ. orb.   =    2
Num. unocc. orb. =    9
Convergence      =  0.1E-08
Debug mode       =    F
Print level      =    2
Use CROP         =    T
CROP subspace    =    3
Preconditioner   =    T
Precond. B       =    T
Singles          =    T
 
 ### Starting CC iterations
 ### ----------------------
 ###  Iteration     Residual norm          CC energy
 ###      1         0.935486793E-02       -0.147469170E-01
 ###      2         0.257793404E-02       -0.148981943E-01
 ###      3         0.756066507E-03       -0.149729447E-01
 ###      4         0.174746225E-03       -0.149939562E-01
 ###      5         0.109863090E-04       -0.149964178E-01
 ###      6         0.980879033E-06       -0.149964469E-01
 ###      7         0.119214668E-06       -0.149964455E-01
 ###      8         0.236173514E-07       -0.149964451E-01
 ###      9         0.870010920E-08       -0.149964448E-01
 ###     10         0.127640824E-08       -0.149964445E-01
 ###     11         0.174808800E-09       -0.149964445E-01
 CCSOL: MAIN LOOP      :  7.09    s
 

-------------------------------
  Coupled-cluster job summary  
-------------------------------

Hooray! CC equation is solved!
CCSOL: Total cpu time    =   1.60     s
CCSOL: Total wall time   =   1.60     s
Singles amplitudes norm  =  0.2246812E-01
Total amplitudes norm    =  0.2246812E-01
Corr. energy             =     -0.0149964445
Number of CC iterations  =   11


 -------------------------------
  Non-linear CC Davidson solver 
 -------------------------------

 Self-consistency threshold =  0.100E-03
 Solve for left eigenvectors
 Input excitation energies:
      0.32818409
      0.37033916
      0.37033916

 ### Starting Davidson Macro-iterations
 ### ----------------------------------
 ###
 ### State no.  Macro-it   # Micro-it  delta(freq.)     freq. (a.u.)
 ### ---------------------------------------------------------------
 ###     1          1            1     0.189997E+00      0.13818694
 ###     2          1            1     0.191296E+00      0.17904284
 ###     3          1            1     0.191296E+00      0.17904284
 ###     1          2            3     0.549664E-02      0.13269029
 ###     2          2            3     0.910940E-03      0.17813190
 ###     3          2            3     0.910940E-03      0.17813190
 ###     1          3            3     0.988642E-05      0.13270018
 ###     2          3            3     0.224870E-03      0.17790703
 ###     3          3            3     0.224870E-03      0.17790703
 ###     1          4            2     0.261297E-05      0.13270279
 ###     2          4            2     0.139917E-04      0.17792102
 ###     3          4            2     0.139917E-04      0.17792102


      ************************************************************
      *               RICC2    excitation energies               *
      ************************************************************

            Exci.    Hartree           eV            cm-1         
              1      0.1327028       3.611029       29124.90    
              2      0.1779210       4.841480       39049.15    
              3      0.1779210       4.841480       39049.15    
 
 
 
 RICC2 correlation energy :    -0.1499644452E-01
 
 
 
    ******************************************************************************
    *                      Full RICC2    calculation is done !                   *
    ******************************************************************************
 
 
 
 
 
    ******************************************************************************
    *                             CC ENERGY SUMMARY                              *
    ******************************************************************************
 
     E: Hartree-Fock energy                            :       -7.7452048226
     E: Correlation energy                             :       -0.0149964445
     E: Total RI-CC2 energy                            :       -7.7602012671
 
 
 
    CC Memory summary
    -----------------
     Allocated memory for array4   :   0.000     GB
     Memory in use for array4      :   0.000     GB
     Max memory in use for array4  :   0.000     GB
    ------------------
 

    ------------------------------------------------------
    Total CPU  time used in CC           :         6.00416     s
    Total Wall time used in CC           :         6.00400     s
    ------------------------------------------------------

 
    Job finished   : Date: 24/11/2016   Time: 16:43:16
 
 

    =============================================================================
                              -- end of CC program --
    =============================================================================

 
 
    Total no. of matmuls used:                      5098
    Total no. of Fock/KS matrix evaluations:          12
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
                      Memory statistics          
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
      Allocated memory (TOTAL):         0 byte Should be zero, otherwise a leakage is present
 
      Max allocated memory, TOTAL                         11.584 MB
      Max allocated memory, type(matrix)                 107.704 kB
      Max allocated memory, real(realk)                   11.565 MB
      Max allocated memory, integer                      112.152 kB
      Max allocated memory, logical                        1.092 kB
      Max allocated memory, character                      2.640 kB
      Max allocated memory, AOBATCH                       62.464 kB
      Max allocated memory, BATCHTOORB                     0.304 kB
      Max allocated memory, ARRAY                         19.392 kB
      Max allocated memory, ODBATCH                        2.640 kB
      Max allocated memory, LSAOTENSOR                     6.272 kB
      Max allocated memory, SLSAOTENSOR                    8.496 kB
      Max allocated memory, ATOMTYPEITEM                 661.584 kB
      Max allocated memory, ATOMITEM                       1.584 kB
      Max allocated memory, LSMATRIX                      31.312 kB
      Max allocated memory, OverlapT                     412.416 kB
      Max allocated memory, linkshell                      0.252 kB
      Max allocated memory, integrand                    774.144 kB
      Max allocated memory, integralitem                  11.466 MB
      Max allocated memory, IntWork                      179.064 kB
      Max allocated memory, Overlap                        4.653 MB
      Max allocated memory, ODitem                         1.920 kB
      Max allocated memory, LStensor                      43.945 kB
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
 
    Allocated MPI memory a cross all slaves:          0 byte  - Should be zero - otherwise a leakage is present
    This is a non MPI calculation so naturally no memory is allocated on slaves!
    >>>  CPU Time used in LSDALTON is  11.50 seconds
    >>> wall Time used in LSDALTON is  11.50 seconds

    End simulation
     Date and time (Linux)  : Thu Nov 24 16:43:16 2016
     Host name              : fe2                                     
