#!/usr/bin/env python

import os
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), "..", ".."))  # isort: skip

from runtest import version_info, get_filter, cli, run
from runtest_config import configure


assert version_info.major == 2

options = cli()

f_hf = [
    get_filter(string="Nuclear repulsion:", rel_tolerance=1.0e-9),
    get_filter(string="Electronic energy:", rel_tolerance=1.0e-9),
    get_filter(string="PCM polarization energy:", abs_tolerance=1.0e-7),
    get_filter(string="Final HF energy:", rel_tolerance=1.0e-9),
    get_filter(
        from_string="FIRST HYPERPOLARIZABILITY TENSOR RESULTS",
        to_string="ZZZ",
        skip_below=1.0e-8,
        rel_tolerance=1.0e-8,
    ),
    get_filter(string="BetaPerpendicular", rel_tolerance=1.0e-5),
    get_filter(string="BetaParallel", rel_tolerance=1.0e-5),
]

# First Hyperpolar HF
ierr = run(
    options,
    configure,
    input_files=["beta_hf.dal", "CH2O.mol", "pcmsolver.pcm"],
    filters={"out": f_hf},
)

f_hfTPA = [
    get_filter(string="Nuclear repulsion:", rel_tolerance=1.0e-9),
    get_filter(string="Electronic energy:", rel_tolerance=1.0e-9),
    get_filter(string="PCM polarization energy:", abs_tolerance=1.0e-7),
    get_filter(string="Final HF energy:", rel_tolerance=1.0e-9),
    get_filter(
        from_string="TWO-PHOTON ABSORPTION RESULTS",
        num_lines=52,
        ignore_sign=True,
        abs_tolerance=1.0e-4,
    ),
]
# Two Photon Absorption HF
ierr += run(
    options,
    configure,
    input_files=["tpa_hf.dal", "CH2O.mol", "pcmsolver.pcm"],
    filters={"out": f_hfTPA},
)

f_lda = [
    get_filter(string="Nuclear repulsion:", rel_tolerance=1.0e-9),
    get_filter(string="Electronic energy:", rel_tolerance=1.0e-9),
    get_filter(string="PCM polarization energy:", abs_tolerance=1.0e-7),
    get_filter(string="Final DFT energy:", rel_tolerance=1.0e-9),
    get_filter(
        from_string="FIRST HYPERPOLARIZABILITY TENSOR RESULTS",
        to_string="ZZZ",
        skip_below=1.0e-8,
        rel_tolerance=1.0e-8,
    ),
    get_filter(string="BetaPerpendicular", rel_tolerance=1.0e-5),
    get_filter(string="BetaParallel", rel_tolerance=1.0e-5),
]

# First Hyperpolar LDA
ierr += run(
    options,
    configure,
    input_files=["beta_lda.dal", "CH2O.mol", "pcmsolver.pcm"],
    filters={"out": f_lda},
)


f_ldaTPA = [
    get_filter(string="Nuclear repulsion:", rel_tolerance=1.0e-9),
    get_filter(string="Electronic energy:", rel_tolerance=1.0e-9),
    get_filter(string="PCM polarization energy:", abs_tolerance=1.0e-7),
    get_filter(string="Final DFT energy:", rel_tolerance=1.0e-9),
    get_filter(
        from_string="TWO-PHOTON ABSORPTION RESULTS",
        num_lines=52,
        ignore_sign=True,
        abs_tolerance=1.0e-4,
    ),
]
# Two Photon Absorption LDA
ierr += run(
    options,
    configure,
    input_files=["tpa_lda.dal", "CH2O.mol", "pcmsolver.pcm"],
    filters={"out": f_ldaTPA},
)

sys.exit(ierr)
