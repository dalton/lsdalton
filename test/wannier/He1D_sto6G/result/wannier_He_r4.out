     THIS IS A DEBUG BUILD
  
     ******************************************************************    
     **********  LSDalton - An electronic structure program  **********    
     ******************************************************************    
  
  
    This is output from LSDalton 1.0
  
  
     IF RESULTS OBTAINED WITH THIS CODE ARE PUBLISHED,
     THE FOLLOWING PAPER SHOULD BE CITED:
     
     K. Aidas, C. Angeli, K. L. Bak, V. Bakken, R. Bast,
     L. Boman, O. Christiansen, R. Cimiraglia, S. Coriani,
     P. Dahle, E. K. Dalskov, U. Ekstroem, T. Enevoldsen,
     J. J. Eriksen, P. Ettenhuber, B. Fernandez,
     L. Ferrighi, H. Fliegl, L. Frediani, K. Hald,
     A. Halkier, C. Haettig, H. Heiberg,
     T. Helgaker, A. C. Hennum, H. Hettema,
     E. Hjertenaes, S. Hoest, I.-M. Hoeyvik,
     M. F. Iozzi, B. Jansik, H. J. Aa. Jensen,
     D. Jonsson, P. Joergensen, J. Kauczor,
     S. Kirpekar, T. Kjaergaard, W. Klopper,
     S. Knecht, R. Kobayashi, H. Koch, J. Kongsted,
     A. Krapp, K. Kristensen, A. Ligabue,
     O. B. Lutnaes, J. I. Melo, K. V. Mikkelsen, R. H. Myhre,
     C. Neiss, C. B. Nielsen, P. Norman,
     J. Olsen, J. M. H. Olsen, A. Osted,
     M. J. Packer, F. Pawlowski, T. B. Pedersen,
     P. F. Provasi, S. Reine, Z. Rinkevicius,
     T. A. Ruden, K. Ruud, V. Rybkin,
     P. Salek, C. C. M. Samson, A. Sanchez de Meras,
     T. Saue, S. P. A. Sauer, B. Schimmelpfennig,
     K. Sneskov, A. H. Steindal, K. O. Sylvester-Hvid,
     P. R. Taylor, A. M. Teale, E. I. Tellgren,
     D. P. Tew, A. J. Thorvaldsen, L. Thoegersen,
     O. Vahtras, M. A. Watson, D. J. D. Wilson,
     M. Ziolkowski, and H. AAgren,
     "The Dalton quantum chemistry program system",
     WIREs Comput. Mol. Sci. (doi: 10.1002/wcms.1172)
  
  
                                               
    LSDalton authors in alphabetical order (main contribution(s) in parenthesis)
    ----------------------------------------------------------------------------
    Vebjoern Bakken,        University of Oslo,        Norway   (Geometry optimizer)
    Radovan Bast,           UiT The Arctic University of Norway (CMake, Testing)
    Pablo Baudin,           Aarhus University,         Denmark  (DEC,CCSD)
    Sonia Coriani,          University of Trieste,     Italy    (Response)
    Patrick Ettenhuber,     Aarhus University,         Denmark  (CCSD)
    Janus Juul Eriksen,     Aarhus University,         Denmark  (CCSD(T), DEC)
    Trygve Helgaker,        University of Oslo,        Norway   (Supervision)
    Stinne Hoest,           Aarhus University,         Denmark  (SCF optimization)
    Ida-Marie Hoeyvik,      Aarhus University,         Denmark  (Orbital localization, SCF opt)
    Robert Izsak,           University of Oslo,        Norway   (ADMM)
    Branislav Jansik,       Aarhus University,         Denmark  (Trilevel, orbital localization)
    Poul Joergensen,        Aarhus University,         Denmark  (Supervision)
    Joanna Kauczor,         Aarhus University,         Denmark  (Response solver)
    Thomas Kjaergaard,      Aarhus University,         Denmark  (RSP, INT, DEC, SCF, Readin, MPI, MAT)
    Andreas Krapp,          University of Oslo,        Norway   (FMM, dispersion-corrected DFT)
    Kasper Kristensen,      Aarhus University,         Denmark  (Response, DEC)
    Patrick Merlot,         University of Oslo,        Norway   (ADMM)
    Cecilie Nygaard,        Aarhus University,         Denmark  (SOEO)
    Jeppe Olsen,            Aarhus University,         Denmark  (Supervision)
    Simen Reine,            University of Oslo,        Norway   (Integrals, geometry optimizer)
    Vladimir Rybkin,        University of Oslo,        Norway   (Geometry optimizer, dynamics)
    Pawel Salek,            KTH Stockholm,             Sweden   (FMM, DFT functionals)
    Andrew M. Teale,        University of Nottingham   England  (E-coefficients)
    Erik Tellgren,          University of Oslo,        Norway   (Density fitting, E-coefficients)
    Andreas J. Thorvaldsen, University of Tromsoe,     Norway   (Response)
    Lea Thoegersen,         Aarhus University,         Denmark  (SCF optimization)
    Mark Watson,            University of Oslo,        Norway   (FMM)
    Marcin Ziolkowski,      Aarhus University,         Denmark  (DEC)
  
  
     NOTE:
      
     This is an experimental code for the evaluation of molecular
     energies and properties using various electronic structure models.
     The authors accept no responsibility for the performance of the code or
     for the correctness of the results.
      
     The code (in whole or part) is provided under a licence and
     is not to be reproduced for further distribution without
     the written permission of the authors or their representatives.
      
     See the home page "http://daltonprogram.org"
     for further information.
  
  
     Who compiled             | erebolini
     Host                     | 1x-193-157-181-175.uio.no
     System                   | Darwin-16.1.0
     CMake generator          | Unix Makefiles
     Processor                | x86_64
     64-bit integers          | OFF
     MPI                      | OFF
     Fortran compiler         | /usr/local/bin/gfortran
     Fortran compiler version | GNU Fortran (Homebrew gcc 6.2.0) 6.2.0
     C compiler               | /Applications/Xcode.app/Contents/Developer/usr/bin
                              | /gcc
     C compiler version       | Apple LLVM version 8.0.0 (clang-800.0.42.1)
     C++ compiler             | /Applications/Xcode.app/Contents/Developer/usr/bin
                              | /g++
     C++ compiler version     | Apple LLVM version 8.0.0 (clang-800.0.42.1)
     BLAS                     | /usr/lib/libblas.dylib
     LAPACK                   | /usr/lib/liblapack.dylib
     Static linking           | OFF
     Last Git revision        | e45304a971abc764d318f908dcc2254d987a0a9c
     Git branch               | elisa/wannier_dggev
     Configuration time       | 2016-11-29 09:17:42.566995
  
 

         Start simulation
                      
    ---------------------------------------------------
             PRINTING THE MOLECULE.INP FILE 
    ---------------------------------------------------
                      
    BASIS                                   
    STO-6G                                  
                                            
                                            
    Atomtypes=1 Angstrom Nosymmetry periodic                                                                                
    Charge=2. Atoms=1                                                                                                       
    He 0.0 0.0 0.0                                                                                                          
                      
    ---------------------------------------------------
             PRINTING THE LSDALTON.INP FILE 
    ---------------------------------------------------
                      
    **GENERAL
    .NOGCBASIS
    **INTEGRALS
    .THRESH
    1.0D-18
    .NOLINK
    .NO PS
    .NO CS
    **WAVE FUNCTIONS
    .HF
    *DENSOPT
    .START
    H1DIAG
    **WANNIER
    .NF_CUTOFF
    10 5 5
    .AO_CUTOFF
    7
    .SCREENING_THRESHOLD
    1.0e-16
    .AO_SHUKLA
    *END OF INPUT
 
 
                      
    Atoms and basis sets
      Total number of atoms        :      1
      THE  REGULAR   is on R =   1
    ---------------------------------------------------------------------
      atom label  charge basisset                prim     cont   basis
    ---------------------------------------------------------------------
          1 He     2.000 STO-6G                     6        1 [6s|1s]                                      
    ---------------------------------------------------------------------
    total          2                                6        1
    ---------------------------------------------------------------------
                      
                      
    Basic Molecule/Basis information
    --------------------------------------------------------------------
      Molecular Charge                   :    0.0000
      Regular basisfunctions             :        1
      Primitive Regular basisfunctions   :        6
    --------------------------------------------------------------------
                      
    Configuration:
    ==============
    This is a Single core calculation. (no OpenMP)
    This is a serial calculation (no MPI)

    You have requested Augmented Roothaan-Hall optimization
    => explicit averaging is turned off!

    Expand trust radius if ratio is larger than:            0.75
    Contract trust radius if ratio is smaller than:         0.25
    On expansion, trust radius is expanded by a factor      1.20
    On contraction, trust radius is contracted by a factor  0.70

    H1DIAG does not work well with only few saved microvectors for ARH...
    Resetting max. size of subspace in ARH linear equations to:       5

    Density subspace min. method    : None                    
    Density optimization            : Augmented RH optimization          

    Maximum size of Fock/density queue in averaging:   10

    Convergence threshold for gradient        :   0.10E-03
    We perform the calculation in the standard input basis
     
    The Overall Screening threshold is set to              :  1.0000E-18
    The Screening threshold used for Coulomb               :  1.0000E-20
    The Screening threshold used for Exchange              :  1.0000E-18
    The Screening threshold used for One-electron operators:  1.0000E-25
    The SCF Convergence Criteria is applied to the gradnorm in OAO basis

    End of configuration!


    Optimize first density for H1 operator

    Preparing to do cholesky decomposition...
  
    Relative convergence threshold for solver:  1.00000000E-02
    SCF Convergence criteria for gradient norm:  9.99999975E-05
 Calculating max(log(sqrt G0l)) ...
 >>>> Time consumption: maxgab                        ,  Walltime:     0.01,  CPUtime:     0.01.
  ----- max(logsqrt(Gab)) - matrix calculated ----- 
 Cutoff threshold for layers =         -16
 Cutoff in layers (Lx, Ly, Lz) =           2           0           0
 Calculating S - matrix...
 >>>> Time consumption: S mat                         ,  Walltime:     0.00,  CPUtime:     0.00.
  ----- S - matrix calculated ----- 
 Smallest element cutoff threshold for layers =   9.9999999999999998E-017
 Cutoff in layers (Lx, Ly, Lz) =           2           0           0
 Calculating T - matrix...
 >>>> Time consumption: T mat                         ,  Walltime:     0.00,  CPUtime:     0.00.
  ----- T - matrix calculated ----- 
 Smallest element cutoff threshold for layers =   9.9999999999999998E-017
 Cutoff in layers (Lx, Ly, Lz) =           2           0           0
 Calculating near-field  nuclear - nuclear repulsion energy...
 >>>> Time consumption: Enuc                          ,  Walltime:     0.00,  CPUtime:     0.00.
 Calculating T - matrix...
 >>>> Time consumption: T mat                         ,  Walltime:     0.00,  CPUtime:     0.00.
  ----- T - matrix calculated ----- 
 Smallest element cutoff threshold for layers =   9.9999999999999998E-017
 Cutoff in layers (Lx, Ly, Lz) =           2           0           0
 *** Lowdin orthogonalisation of the occupied starting guess MO coeff
 *** Orthogonalisation of the molecular orbitals ***
 
  ===== Entering Wannier SCF Loop =====
 >>>> Time consumption: Fock - nearfield J            ,  Walltime:     2.40,  CPUtime:     2.34.
 >>>> Time consumption: Fock - HFX                    ,  Walltime:     0.22,  CPUtime:     0.22.
 Orthogonalising potential added to the occupied orbitals only
 >>>> Time consumption: SCFLOOP orthpot               ,  Walltime:     0.00,  CPUtime:     0.00.
 
 Kinetic Energy                   2.8550680042533960     
 Electron-nucleus Interaction    -9.8574945680150474     
 Electron-electron Interaction    2.6061912646521432     
 Nucleus-nucleus Interaction      1.5499432438342460     
 
 HF energy E0                      -2.8462920552752626     
 
 
 ****************************************************************************
 * Iteration            1
 ****************************************************************************
 
 >>>> Time consumption: SCFLOOP - P0, S, F to full    ,  Walltime:     0.00,  CPUtime:     0.00.
 >>>> Time consumption: SCFLOOP - Diag F              ,  Walltime:     0.00,  CPUtime:     0.00.
 
 Occupied Eigenvalues:
  Epsilon 1 =  -0.89502202
 >>>> Time consumption: Fock - nearfield J            ,  Walltime:     2.58,  CPUtime:     2.45.
 >>>> Time consumption: Fock - HFX                    ,  Walltime:     0.22,  CPUtime:     0.22.
 Orthogonalising potential added to the occupied orbitals only
 >>>> Time consumption: SCFLOOP orthpot               ,  Walltime:     0.00,  CPUtime:     0.00.
 
 Kinetic Energy                   2.8550680042249144     
 Electron-nucleus Interaction    -9.8574945679869668     
 Electron-electron Interaction    2.6061912646447181     
 Nucleus-nucleus Interaction      1.5499432438342460     
 
 
 Contribution from orthpot        3.9129383901836909E-012
 
 TESTING: lambda/2 * Tr(P0)          3.9129383908144993E-012
 TESTING: difference                 6.3080841980907907E-022
 
 HF energy                       -2.8462920552830884        Change in EHF  -7.8257400559778034E-012
 Lagrangian                      -2.8462920552791755        Change in L    -2.8462920552791755     
 
 ****************************************************************************
 * Iteration            2
 ****************************************************************************
 
 >>>> Time consumption: SCFLOOP - P0, S, F to full    ,  Walltime:     0.00,  CPUtime:     0.00.
 >>>> Time consumption: SCFLOOP - Diag F              ,  Walltime:     0.00,  CPUtime:     0.00.
 
 Occupied Eigenvalues:
  Epsilon 1 =  -0.89502202
 >>>> Time consumption: Fock - nearfield J            ,  Walltime:     2.22,  CPUtime:     2.20.
 >>>> Time consumption: Fock - HFX                    ,  Walltime:     0.19,  CPUtime:     0.18.
 Orthogonalising potential added to the occupied orbitals only
 >>>> Time consumption: SCFLOOP orthpot               ,  Walltime:     0.00,  CPUtime:     0.00.
 
 Kinetic Energy                   2.8550680042533947     
 Electron-nucleus Interaction    -9.8574945680150439     
 Electron-electron Interaction    2.6061912646521415     
 Nucleus-nucleus Interaction      1.5499432438342460     
 
 
 Contribution from orthpot        1.1506031545660939E-021
 
 TESTING: lambda/2 * Tr(P0)          8.0566378265432433E-021
 TESTING: difference                 6.9060346719771500E-021
 
 HF energy                       -2.8462920552752617        Change in EHF   7.8266282343975035E-012
 Lagrangian                      -2.8462920552752617        Change in L     3.9137582064086018E-012
 
 Wannier SCF Converged
 Final PBC-HF energy  -2.8462920552752617     
 Final Lagrangian     -2.8462920552752617     
 Final orthpot contribution   1.1506031545660939E-021
 
  ===== Exiting Wannier SCF Loop =====

 ===============================================================================================
            TIMINGS:                   Wannier SCF
 ===============================================================================================
      Task                                   Walltime           Cputime        Times called
 -----------------------------------------------------------------------------------------------
 >>> maxgab                                      0.01              0.01           1
 >>> S mat                                       0.00              0.00           1
 >>> T mat                                       0.00              0.00           2
 >>> Enuc                                        0.00              0.00           1
 >>> Fock - nearfield J                          7.19              6.99           3
 >>> Fock - HFX                                  0.62              0.61           3
 >>> SCFLOOP orthpot                             0.00              0.00           3
 >>> SCFLOOP - P0, S, F to full                  0.00              0.00           2
 >>> SCFLOOP - Diag F                            0.00              0.00           2
 >>> SCFLOOP - Occ MO                            0.00              0.00           4
 ===============================================================================================
 ===============================================================================================


 ===============================================================================================
            TIMINGS:                   Wannier SCF
 ===============================================================================================
      Task                                   Walltime           Cputime        Times called
 -----------------------------------------------------------------------------------------------
 >>> maxgab                                      0.01              0.01           1
 >>> S mat                                       0.00              0.00           1
 >>> T mat                                       0.00              0.00           2
 >>> Enuc                                        0.00              0.00           1
 >>> Fock - nearfield J                          7.19              6.99           3
 >>> Fock - HFX                                  0.62              0.61           3
 >>> SCFLOOP orthpot                             0.00              0.00           3
 >>> SCFLOOP - P0, S, F to full                  0.00              0.00           2
 >>> SCFLOOP - Diag F                            0.00              0.00           2
 >>> SCFLOOP - Occ MO                            0.00              0.00           4
 ===============================================================================================
 ===============================================================================================

    Total no. of matmuls used:                    163321
    Total no. of Fock/KS matrix evaluations:           1
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
                      Memory statistics          
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
      Allocated memory (TOTAL):         0 byte Should be zero, otherwise a leakage is present
 
      Max allocated memory, TOTAL                        817.960 kB
      Max allocated memory, type(matrix)                  17.168 kB
      Max allocated memory, real(realk)                  780.600 kB
      Max allocated memory, complex(complexk)              0.304 kB
      Max allocated memory, integer                       37.364 kB
      Max allocated memory, logical                        2.324 kB
      Max allocated memory, character                      1.616 kB
      Max allocated memory, AOBATCH                        6.656 kB
      Max allocated memory, ODBATCH                        0.176 kB
      Max allocated memory, LSAOTENSOR                     0.608 kB
      Max allocated memory, SLSAOTENSOR                    0.184 kB
      Max allocated memory, ATOMTYPEITEM                  38.264 kB
      Max allocated memory, ATOMITEM                       1.320 kB
      Max allocated memory, LSMATRIX                       1.008 kB
      Max allocated memory, OverlapT                       4.544 kB
      Max allocated memory, integrand                    258.048 kB
      Max allocated memory, integralitem                 184.320 kB
      Max allocated memory, IntWork                       38.472 kB
      Max allocated memory, Overlap                      297.776 kB
      Max allocated memory, ODitem                         0.128 kB
      Max allocated memory, LStensor                       0.186 kB
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

    Allocated MPI memory a cross all slaves:          0 byte  - Should be zero - otherwise a leakage is present
    This is a non MPI calculation so naturally no memory is allocated on slaves!
    >>>  CPU Time used in LSDALTON is   7.97 seconds
    >>> wall Time used in LSDALTON is   8.19 seconds

    End simulation
